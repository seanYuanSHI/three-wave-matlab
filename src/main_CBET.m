% This is the main driver for calculating scattering 
% for cross-beam energy transfer (CBET). 
%
% Two lasers enter from the vacuum into the plasma. 
% The program first calculates the incidence
% angles in the plasma frame, and then calculates the
% refraction angles once the lasers enter the plasma.
% Notice that magnetized plasmas are birefringent.
%
% The first laser is regarded as a pump with given 
% frequency and propagation directions.
% The second laser is regarded as a seed, whose
% resonant frequency is determined by root finding
% for given propagation directions.
%
% In the slab geometry, the half space x<0 is vacuum.
% The plasma fills the x>0 half space, and has a 
% uniform flow along the z direction. The B0 field
% is also along the z direction.
%
% In the cylidrical geometry, incident and refracted
% waves are always in the plane containing z axis. 
% The plasma flow and B field are along z direction.
%
% In the full space geometry, the plasma fills the
% entire space, and no refraction occurs.
%
% Inputs:
%    wave1 contains w, b, theta, phi
%    wave2 contains b, theta, phi
%        w     : frequency in lab fram, in unit of Trad/s, scalar
%        theta : incidence polar angles in lab fram, in degree
%        phi   : incidence azimuthal angles in lab fram, in degree
%        b     : branch index, scalar, b=1 for highest-frequency branch
%    beta     : plasma flow velocity along z//B0, in unit of c
%    para     : structure containing input parameters
%    geometry : choose from 'slab', 'cylinder', 'full'
%    flag     : flag>0 show messages
%
% Output:
%    scatter: data structure containing the following, default values are zero 
%      w1p   : frequency of wave 1 in plasma frame, scalar
%      ck1p  : wave vector of wave 1 inside plasma, plasma frame, 3-by-1 
%
%      w2    : frequency of resonant wave 2 in lab frame, length nc
%      w2p   : frequency of resonant wave 2 in plasma frame, length nc
%      ck2p  : wave vector of wave 2 inside plasma, plasma frame, 3-by-nc
%
%      Mc    : abs(Mc) is growth rate normalized by unmagnetized Raman, length nc
%      b3    : branch index for the mediating wave, length nc
%
% Written by Yuan SHI, March 9, 2022

function scatter = main_CBET(wave1, wave2, beta, para, geometry, flag)
    %%%%%%%%%%%%%%%%%%%%%%
    % smallness parameter
    % absolute convergence criteria for 
    % root finding is set to dw = ee*min(para.wa,w3guess)
    ee = 0; % use intrinsic eps
    %ee=1e-3; % use customized dw
    
    % conversion factor from degree to rad
    d2r = pi/180;
    %%%%%%%%%%%%%%%%%%%%%%

    % default input
    if nargin<6
        flag=0;
    end
    if nargin<5
        geometry='slab';
    end
    if strcmp(geometry, 'slab')
        gflag = 0;
    elseif strcmp(geometry, 'cylinder')
        gflag = 1;
    else
        gflag = -1;
        geometry = 'full';
    end
    disp(['Geometry is ', geometry])

    % unpack wave 1
    w1 = abs(wave1.w); % ensure positive
    b1 = round(wave1.b); % ensure integer
    t1 = wave1.theta;
    p1 = wave1.phi;
    % incident pump unit vector ckx 
    ck1x = sin(t1*d2r)*cos(p1*d2r);
    % unpack wave 2
    b2 = round(wave2.b); % ensure integer
    t2 = wave2.theta;
    p2 = wave2.phi;
    % incident seed unit vector ckx 
    ck2x = sin(t2*d2r)*cos(p2*d2r);

    % check inputs
    if gflag==0 % slab geometry
        assert(ck1x>0,'wave 1 should propagate into +x')
        assert(ck2x>0,'wave 2 should propagate into +x')
    end
    assert(abs(beta)<1,'flow speed should be < c!')     

    % total number of waves
    nw = para.nw;
    % number of plasma wave branches, exclude 2EM waves
    nc = para.nw - 2;
    % initial branch index
    b0 = 2;

    if flag>0
        disp('Computing refracted pump')
    end
    % refracted pump in plasma frame
    [w1p, ck1p] = BoostRefract(w1,t1,p1,b1,beta,para,geometry);
    % store pump
    scatter.w1p = w1p;
    scatter.ck1p = ck1p;
    % initialize storage
    w2=zeros(1,nc);
    w2p=zeros(1,nc);
    ck2p=zeros(3,nc);
    Mc=zeros(1,nc);
    b3=zeros(1,nc);

    % function mapping w2 to all posible w3
    % w: lab frame seed frequency
    function  [w3s, wp, ckp] = W2W3(w)
        % refracted seed in plasma frame
        [wp,ckp]=BoostRefract(w,t2,p2,b2,beta,para,geometry);
        if ckp(1)*ck2x<0 % total reflection of seed
            w3s=[];
            disp('Seed total internal reflection')
            disp(['initial ckx/ck=',num2str(ck2x)])
            disp(['final ckx=',num2str(ckp(1))])
        else % seed can enter plasma
            % plasma wave vector in plasma frame
            ck3=ck1p-ckp;
            % convert to spherical coordinate
            [~,ele,ck]=cart2sph(ck3(1),ck3(2),ck3(3));
            % propagation angle of wave 3 in degree
            t3=90-ele/d2r;
            % frequency of wave 3, 0: no plot
            w3s=ck_2_w_warm(ck,t3,para,0);
        end
    end    

    % objective function for resonance matching
    % w: lab frame seed frequency
    % b: branch index of plasma wave
    function fd = fmatching(w, b)
        % frequency of wave 3
        [w3s,wp,~]=W2W3(w);
        if length(w3s)==0
            fd = -999;
        else
            % frequency difference
            fd=w1p-wp-w3s(b);
        end
    end    

    % check if total internal reflection occurs
    if ck1p(1)*ck1x<0 
        disp('Pump cannot enter plasma due to total internal reflection!')
        disp(['initial ckx/ck=',num2str(ck1x)])
        disp(['final ckx=',num2str(ck1p(1))])
    else
        % pump can enter, calculate resonant seed
        % initial guess of resonance frequencies
        [w3guess,~,~]=W2W3(w1);
        if length(w3guess)==0
            disp('Seed guess total reflection. Assume no interaction.')
        else
            % convergence criteria
            if ee>0 % use customized
                dw = ee*min(para.wa,w3guess(nw-1));
            else % use intrinsic
                dw = eps; % intrisic MATLAB precision
            end
            if flag>0
                display(['Convergence criteria is dw=',num2str(dw)])
                options = optimset('Display','iter','TolX',dw); % show iteration
            else
                options = optimset('TolX',dw);
            end
            
            for i=1:nc
                % b3 value
                ii = b0+i;
                if flag>0
                    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
                    display(['Computing mediation by plasma wave ',num2str(ii)])
                end
                
                % search for zero of objective function
                fun = @(w)fmatching(w,ii);
                % search for root, lab frame frequency for wave 2
                try
                    w0 = fzero(fun,w1-w3guess(ii),options);
                catch
                    warning('fzero fails to find root. Omit this resonance.')
                    w0 = 0;
                end

                % load values
                if w0>0
                    w2(i)=w0; % lab frame
                    [~,wp,ckp]=W2W3(w0); % plasma frame
                    w2p(i)=wp;
                    ck2p(:,i)=ckp;
 
                    if flag>0
                        disp('Compute coupling...')
                    end
                    % load wave data in plasma frame, 0: no display of wave data 
                    wave=wave_data_warm(b1,w1p,ck1p,b2,wp,ckp,para,1);
                    b3(i)=wave.b3; % should equal to ii
                    if b3(i)~=ii
                        display(['Actual b3=',num2str(b3(i))])
                    end

                    % coupling coefficient
                    [C,~,~]=Couple_warm(wave,para);
                    % growth rate
                    Mc(i)=Growth(wave,para,C);
                end
            end
        end
    end

    % load output
    scatter.w2 = w2;
    scatter.w2p = w2p;
    scatter.ck2p = ck2p;
    scatter.Mc = Mc;
    scatter.b3 = b3;
    
end
