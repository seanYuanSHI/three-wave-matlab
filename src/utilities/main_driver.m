% This script is the driver for main.m
% parameters:

B0=2;
b1=1;
b2=1;
%w1=3.5327e3;
w1=2*pi*854.109;   % 0.351 mu
%w1=299.792458*2*pi; % 1 um

theta1=54;
theta2=126;
phi2=180;

%ckmin=1e3;
%ckmax=4e3;
ckmin=[];
ckmax=[];

debug=1;

fpath = '../data/OMEGA/CBET/backscatter/';
ftail = '_N1T0.05_carbon.mat';
%ftail = '_b1-b1_w5366_backward_data.mat';
fname = [fpath,'B',num2str(B0),'_t',num2str(theta1),ftail];
disp(fname)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% para
para=Parameter(B0);

% ck1r
ck1r=wb_2_ck_warm(w1,b1,theta1,para,0);
% bounds in unit of ck1r
bounds.ckmin=ckmin;
bounds.ckmax=ckmax;

% khat 1
st1=sin(theta1/180*pi);
ct1=cos(theta1/180*pi);
khat1=[st1;0;ct1];

ck1=ck1r*khat1;

% khat2
[x,y,z]=sph2cart(phi2/180*pi,(90-theta2)/180*pi,1);
khat2=[x;y;z];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% key steps inside main 
%[w2,ck2r]=search_resonance_warm(w1,ck1,b2,khat2,para,flag,range)
%[w2,ck2r]=search_resonance_warm(w1,ck1,b2,khat2,para,debug,bounds)
%figure

scatter=main(b1,w1,ck1,b2,khat2,para,debug,bounds);

% save data
save(fname,'scatter','para','ck1r','w1','theta1','theta2','phi2')
