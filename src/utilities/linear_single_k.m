% This function computes all properties 
% of interest for a single linear wave
% whose k is specified.
%
% Inputs:
%    para  : data structure containing plasma parameters
%    k     : norm of wave vector, in Trad/s 
%    theta : angle of k from z, k in zx plane, in deg
%    b     : branch index
% Outputs:
%    w     : wave frequency, in Trad/s
%    e     : unit polarization vector in xyz coordinate
%    u     : wave energy coefficient, dimensionless
%
% Written by Yuan SHI, Nov 3, 2020

function [w,e,u]=linear_single(para,k,theta,b)

% angle in rad
t = theta/180*pi;
ct = cos(t);
st = sin(t);
% wave vector
kvec = k*[st;0;ct];

% compute dispersion at given k
omega = ck_2_w_warm(k,theta,para,0);
% select branch
w = omega(b);

% polarization vector
[e,phi,psi]=wck_2_e_warm(w,kvec,para,0);

% wave energy
H2 = Energy_warm(w,kvec,para); %H/2
u = e'*H2*e;

% report quantites 
%disp('wave frequencies are',omega)
%fprintf('polarization phi=%.4f, psi=%.4f\n',phi,psi)
