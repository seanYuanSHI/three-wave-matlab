% This is the driver function for ../plots/plot_map_table.m
cd .. % enter /src directory
addpath(genpath('./'))
clear

% path to data file
fpath='../data/OMEGA/';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%geometry='sidelighter';
geometry='backlighter';

% index for b3
indices=[1,2,3,4];
%indices=[1];

% all cases
N0list=[0.01,0.10,1.00];
B0list=[1,2,3,4];

% selected case
N0=0.01;
B0=1;


% flag=1: plot as function of N0
% flag=2: plot as function of B0
flag=2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N=length(indices);
for ib=1:N 
    index=indices(ib);
    if flag==1
        % filename base and tail
        fbase=sprintf('%s%s/map_N',fpath,geometry);
        ftail=sprintf('B%d.mat',B0);
        % fnamelist
        for i=1:length(N0list)
            fnamelist{i}=sprintf('%s%.2f%s',fbase,N0list(i),ftail);
        end
        plot_map_table(fnamelist,index,log10(N0list),'log10 N0(10^{20} cc)')

    else
        % filename base and tail
        fbase=sprintf('%s%s/map_N%.2fB',fpath,geometry,N0);
        ftail='.mat';
        % fnamelist
        for i=1:length(B0list)
            fnamelist{i}=sprintf('%s%d%s',fbase,B0list(i),ftail);
        end
        plot_map_table(fnamelist,index,B0list,'B0(MG)')
    end
end
