% This function print map for all caes in the table
% Input:
%    fname  : name of the .mat data file
%    nmax   : when specified, only print to nmax modes

function map_table_print(fname,nmax)
    % load data 
    load(fname)

    if nargin<2
        % print all down scattered modes
        nmax = para.nw-2;        
    end
 
    % display setup
    display(setup)
    % print para
    para_print(para)

    nb = length(blist);
    for ib = 1:nb
        % print blist
        fprintf('b1=%d, b2=%d\n',blist(ib,1),blist(ib,2))
        % print ck1r
        fprintf('ck1r=%8.4e\n',mapTable(ib).ck1r)
        % print map
        map_print(mapTable(ib),setup.w1,nmax)
    end

