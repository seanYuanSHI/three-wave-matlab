% This is a driver for main_CBET.m
%
% The driver reads a file that specifies the geometry of a 1D scan.
% The file should have five columns. The 1st column is psi angle
% that parameterizes the 1D scan, the 2nd and 3rd columns are 
% pump theta and phi angles, the 4th and 5th columns are the seed 
% theta and phi angles. Angles are specified in degrees.
% The columns should be delimited by ',', and commented by '#'.
%
% The driver produces an output file for three-wave interactions
% as documented in the file header. The columns are flow velocity,
% psi angle, resonant seed frequency, abs of three-wave coupling
% and index of the branch that mediate the interaction.
%
% Set species parameters in ../species.txt
% Background magnetic field B0//z in MG
% Plasma is in x>0 half space, x<0 is vacuum.
% 
% Last modified by Yuan SHI on March 16, 2023

function main_CBET_driver(B0, fname_in, fname_out, b1, b2)
% branch index
if nargin<4
    b1 = 1; % pump 
end
if nargin<5
    b2 = 1; % seed
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pump wave frequency in lab frame, Trad/s
w1 = 5366.5; % 0.351um

% min/max plasma flow along +z
betamin = 0.0e-3;
betamax = 0.0e-3; % in unit of c, >=0
% number of points to scan between 0 and beta
% betascan=linspace(betamin,betamax,nbeta), when nbeta=1, betascan=betamax 
nbeta = 1; 

% geometry, choose from 'slab', 'cylinder', 'full'
%gflag = 'cylinder';
gflag = 'full';
% flag = 0: no message, 1: show message
flag = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize plasma
para=Parameter(B0);
% number of species
ns=length(para.Ws);
% species
Ns = para.Ns;
Ms = para.Ms;
Zs = para.Zs;
Ts = para.Ts;

% search arrays for beta
betascan=linspace(betamin,betamax,nbeta);

% read geometry file for the scan
disp(['Reading input geometry file: ', fname_in])
fid = fopen(fname_in, 'r');
% read data in cell array format
geometry = textscan(fid, '%f %f %f %f %f','Delimiter',',','CommentStyle','#');
fclose(fid);

% open output file and write header, append
disp(['Writing output file header: ', fname_out])
fid = fopen(fname_out,'a+');
% write header
fprintf(fid,'###############\n');
fprintf(fid,'# Geometry is %s, file name: \n#   %s\n',gflag,fname_in);
fprintf(fid,'# w1=%.4e Trad/s, b1=%d, b2=%d, B0=%.2e MG\n',w1,b1,b2,B0); 
for i=1:ns
    fprintf(fid,'# species %d: Z=%d, M=%.2f, N=%.2e, T=%.2f\n',...
                            i,Zs(i),Ms(i),Ns(i),Ts(i));
end
fprintf(fid,'# beta (c), angle (deg), w2(Trad/s), M, b3\n');
fprintf(fid,'###############\n');
% format string for data
fstring = '%.4e, %.4f, %.8e, %.4e, %d\n';

% initial load of waves
wave1.w = w1;
wave1.b = b1;
wave2.b = b2;

% loop through all scan cases
psis = geometry{1}; % access cell array by {}
na = length(psis);
for ib=1:nbeta
    beta = betascan(ib)
    for ia=1:na
        display(['ib/nb=',num2str(ib/nbeta),', ia/na=',num2str(ia/na)])

        % compute angles
        %[t1,p1,t2,p2]=geometry_CBET(angle,across,gos) 
        % extracting angles and load waves
        wave1.theta=geometry{2}(ia);
        wave1.phi=geometry{3}(ia);
        wave2.theta=geometry{4}(ia);
        wave2.phi=geometry{5}(ia);

        % compute scattering, 1: show messages
        scatter=main_CBET(wave1,wave2,beta,para,gflag,flag);
        % unpack
        w2 = scatter.w2;
        Mc = scatter.Mc;
        b3 = scatter.b3;

        % save data
        for i=1:length(w2)
            fprintf(fid,fstring,beta,psis(ia),w2(i),abs(Mc(i)),b3(i));
        end
    end
end

% close file
fclose(fid);
