% This is a driver for main_CBET.m when T0 is rampping on/off
% in a Thomson-scattering type setup from a gas-jet plasma cylinder,
% for which the flow is negligible and density remains largely constant.
%
% The driver reads a file that specifies the geometry. 
% The file should have five columns. The 1st column a integer label 
% for the Thomson setup, the 2nd and 3rd columns are 
% pump theta and phi angles, the 4th and 5th columns are the seed 
% theta and phi angles. Angles are specified in degrees in B0 frame.
% where the background magnetic field B0//z in specified in MG
% The columns should be delimited by ',', and commented by '#'.
%
% For each Thomson setup, this program will write a spearate data file
% whose columns are as documented in the file header. Each row of the
% file is a different plasma temperature T0.
%
% Inputs:
%    B0        : background B field in MG
%    fname_in  : input geometry file name 
%    fstem_out : output data file name stem
%                files will be named <fstem_out><label>.txt
%    b1, b2    : branch index of pump and seed
% 
% Last modified by Yuan SHI on April 4, 2023

function main_Thomson_driver(B0, fname_in, fstem_out, b1, b2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pump wave frequency in lab frame, Trad/s
w1 = 5366.5; % 0.351um

% min/max plasma temperature in keV
Tmin = 0.05;
Tmax = 0.5; 
% Tscan = linspace(Tmin,Tmax,nT), when nT=1, Tscan=Tmax 
nT = 21; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% search arrays 
Tscan=linspace(Tmin,Tmax,nT);

% initialize plasma
para=Parameter(B0);
% number of species
ns=length(para.Ws);
% species
Ms = para.Ms;
Zs = para.Zs;
Ns = para.Ns;
Ts = para.Ts;
% ratios of temperatures
Tr = Ts/Ts(1);

% read geometry file for the scan
disp(['Reading input geometry file: ', fname_in])
fid = fopen(fname_in, 'r');
% read data in cell array format
geometry = textscan(fid, '%d %f %f %f %f','Delimiter',',','CommentStyle','#');
fclose(fid);

% number of geometry setups
ng = length(geometry{1}); 
% format string for output data
fstring = '%.4f, %.8e, %.4e, %d\n';
% loop through all setups
for ig=1:ng
    % file name for the setup
    fname = [fstem_out,num2str(geometry{1}(ig)),'.txt'];
    % extract angles fro geometry file
    t1 = geometry{2}(ig);
    p1 = geometry{3}(ig);
    t2 = geometry{4}(ig);
    p2 = geometry{5}(ig);

    % open output file and write header, append
    disp(['Writing output file header: ', fname])
    fid = fopen(fname,'a+');
    % write header
    fprintf(fid,'###############\n');
    fprintf(fid,'# w1=%.4e Trad/s, B0=%.2e MG\n',w1,B0); 
    fprintf(fid,'# theta1=%.2f deg, phi1=%.2f, b1=%d\n',t1,p1,b1); 
    fprintf(fid,'# theta2=%.2f deg, phi2=%.2f, b2=%d\n',t2,p2,b2); 
    for i=1:ns
       fprintf(fid,'# species %d: Z=%d, M=%.2f, N=%.2f, T/T0=%.2f\n',...
                               i,Zs(i),Ms(i),Ns(i),Tr(i));
    end
    fprintf(fid,'# T0 (keV), w2(Trad/s), M, b3\n');
    fprintf(fid,'###############\n');
    
    % initial load of waves
    wave1.w = w1;
    wave1.b = b1;
    wave1.theta=t1;
    wave1.phi=p1;
    
    wave2.theta=t2;
    wave2.phi=p2;
    wave2.b = b2;
    
    % loop through all scan cases
    for it=1:nT
       display(['it/nT=',num2str(it/nT)])
       T0 = Tscan(it); 
       % overwrite plasma Ts
       para=Parameter(B0, Ns, T0*Tr);
       
       % 0: no flow, cyliner geometry, 0: show no messages
       scatter=main_CBET(wave1,wave2,0,para,'cylinder',0);
       % unpack
       w2 = scatter.w2;
       Mc = scatter.Mc;
       b3 = scatter.b3;
    
       % save data
       for i=1:length(w2)
           fprintf(fid,fstring,T0,w2(i),abs(Mc(i)),b3(i));
       end
    end
    
    % close file
    fclose(fid);
end
