% This function compute and save scattering map
% for a table of b1/b2 branch index at given geometry
%
% Inputs:
%     blist  : table of branch index of the form
%              blist=[b1,b2;...] nl-by-2 matrix 
%     para   : struct contain plasma parameters
%     setup  : struct contain problem setup, 
%              specifying scalars w1,t1,t2,p2
%     fname  : name of data file

function map_table(blist,para,setup,fname)

% default file name
if nargin<4
    fname = './data.mat'
end

% check dimensions
assert(size(blist,2)==2)
nl = size(blist,1);

% compute and load mat
for i=1:nl
    % copy basic setup
    s = setup;
    % unpack branch index
    s.b1 = blist(i,1);
    s.b2 = blist(i,2);
    disp(s)
    % compute map
    map = main_map(s,para,'',-1);
    % load struct
    mapTable(i) = map;
end

% save data
save(fname,'mapTable','setup','para','blist')
