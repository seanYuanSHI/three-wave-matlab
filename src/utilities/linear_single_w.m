% This function computes all properties 
% of interest for a single linear wave
% whose w is specified.
%
% Inputs:
%    para  : data structure containing plasma parameters
%    w     : angular frequency of the wave, in Trad/s 
%    theta : angle of k from z, k in zx plane, in deg
%    b     : branch index
%    unit  : when unit='freq', specify w in Trad/s
%            when unit='length', specify w in vacuum wavelength in um
% Outputs:
%    wave  : data structure containing inputs and the following
%        ck      : norm of the wave vector, in Trad/s
%        e       : unit polarization vector in xyz coordinate
%        phi, psi: polarization angles in the wave frame, in rad
%        u       : wave energy coefficient, dimensionless
%        vg,vg_SI: 1D wave group velicity along k direction, in units of c and SI
%        E2a     : convert factor from total E vector to normalized a
%
% Last modified by Yuan SHI, Oct 28, 2021

function wave = linear_single_w(para,w,theta,b,unit)
if nargin<5
    % defaul unit
    unit = 'freq';
end
% load common constants
constants

% convert unit for w
if strcmp(unit,'length')
    disp('w specified in vacuum wavelength in um')
    % convert um to Trad/s
    w = c8*2e2*pi/w; 
else
    disp('w specified in Trad/s')
end

% norm of wave vector
disp('Solving for wave vector norm...')
ck = wb_2_ck_warm(w,b,theta,para,0);
% convert to SI unit
k = 1e4*ck/c8;

% construct wave vector
% angle in rad
t = theta/180*pi;
ct = cos(t);
st = sin(t);
% wave vector
kvec = ck*[st;0;ct];

% polarization vector
disp('Computing wave polarization...')
[e,phi,psi]=wck_2_e_warm(w,kvec,para,0,1); % unit=1: angles in rad

% wave energy
disp('Computing wave energy...')
H2 = Energy_warm(w,kvec,para); %H/2
u = e'*H2*e;

% conversion factors from E_SI to a=e*E*sqrt(u)/(me*w*c)
E2a = 1e-9*e19*sqrt(abs(u))/(m30*w*c8);

% wave group velocity
disp('Computing wave group velocity')
vg = Vgroup1D(b,ck,theta,para);
vg_SI = vg*c8*1e8; % in SI unit

% load output data structure
wave.w = w;
wave.e = e;
wave.u = u;
wave.b = b;
wave.ck = ck;
wave.k_SI = k;
wave.phi_rad = phi;
wave.psi_rad = psi;
wave.theta = theta;
wave.vg = vg;
wave.vg_SI = vg_SI;
wave.E2a = E2a;
