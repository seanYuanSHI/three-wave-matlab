% This script runs map_table.m for a number of cases
% Assuming two-species plasma.
cd .. % enter /src directory
addpath(genpath('./'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fpath = '../data/NIF/rounded_triangle/underdense/backscatter/'
%fpath = '../data/NIF/rounded_triangle/underdense/forwardscatter/'

%setup.w1 = 2*pi*299.8; % 2*pi*THz, 1um
setup.w1 = 2*pi*854; % 2*pi*THz, 0.351um
setup.t1 = 30;
% back scatter t2=180-t1, forward scatter t2=t1
% back scatter p2=180   , forward scatter p2=0
setup.t2 = 150;
setup.p2 = 180; 
%setup.t2 = 30;
%setup.p2 = 0; 

%B0list=[1,2,3]; % MG
B0list=[100]; % MG

%Nslist=[0.01,0.1,1]; % 1e20 cc 
Nslist=[1.336]; % 1e20 cc 

%blist=[1,1;1,2;2,1;2,2];
blist=[1,1;2,2];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nB = length(B0list);
nN = length(Nslist);

for iB=1:nB
    % unpack
    B0 = B0list(iB);
    for iN=1:nN
        % unpack
        Ns = Nslist(iN);
        % load para
        para = Parameter(B0,[Ns,Ns]);
        % filename
        fname=sprintf('%s/map_N%.2fB%d.mat',fpath,Ns,B0)
        % compute data and save to file
        map_table(blist,para,setup,fname)    
    end
end 
