% This  is the main program for calculating three-wave coupling
%
% Inputs:
%    w1    : frequency of wave 1, in unit of Trad/s, scalar
%    b1,b2 : branch index, scalar, b=1 for highest-frequency branch
%    ck1   : wave vector for pump, in unit of Trad/s, length 3-by-1
%    khat2 : unit wave vector for scattered wave, length 3-by-1
%    para  : structure containing input parameters
%    debug : debug=1 report messsage
%
%    bounds  : optional, when specified, search within range
%             bounds.ckmin : minimum ck, length n-by-1
%             bounds.ckmax : maximum ck, length n-by-1
%             search for roots in intervals (ckmin,ckmax]
%
% Output:
%    scatter: data structure containing the following 
%      w2    : frequency of scattered wave, in unit of Trad/s, length nw
%      ck2r  : wave vector of scattered wave, in unit of Trad/s, length nw
%      C     : coupling coefficient, in unit of (Trad/s)^2, length nw
%      u123  : products of wave energy coefficients, length nw
%      Sa    : summation abs C,in unit of (Trad/s)^2, length nw
%      Mc    : abs(Mc) is growth rate normalized by unmagnetized Raman, nx-ny-nw
%      b3    : branch index for the mediating wave, length nw
%
% Written by Yuan SHI, Feb 4, 2019

function scatter=main(b1,w1,ck1,b2,khat2,para,debug,bounds)

% debug =1 report all message
if nargin<7
    debug=0;
end
if nargin<8
    bounds.ckmin=[];
    bounds.ckmax=[];
end

% check inputs
assert(length(b1)==1);
assert(length(w1)==1);
assert(length(b2)==1);
assert(length(ck1)==3);
assert(length(khat2)==3);

% ensure normalization
khat2=khat2/sqrt(khat2'*khat2);

% search for all possible resonantly scattered wave
[w2,ck2r]=search_resonance_warm(w1,ck1,b2,khat2,para,debug,bounds);

nw=length(w2);
if nw==0
    disp('No resonance found!')
    % default ouputs
    C=[];
    u123=[];
    Sa=[];
    Mc=[];
    b3=[];
else
    % initialize outputs
    C=zeros(1,nw);
    u123=zeros(1,nw);
    Sa=zeros(1,nw);
    Mc=zeros(1,nw);
    b3=zeros(1,nw);
    
    % compute for each scattering mode
    for i=1:nw
        % load wave characteristics
        ck2=ck2r(i)*khat2;
        wave=wave_data_warm(b1,w1,ck1,b2,w2(i),ck2,para,debug);
        
        % coupling coefficient
        [C(i),u123(i),Sa(i)]=Couple_warm(wave,para);
        % growth rate
        Mc(i)=Growth(wave,para,C(i));
        % branch index
        b3(i)=wave.b3;
    end
end

% load output
scatter.w2=w2;
scatter.ck2r=ck2r;
scatter.C=C;
scatter.u123=u123;
scatter.Sa=Sa;
scatter.Mc=Mc;
scatter.b3=b3;

