% This script tighten the current axis with a margin
relax=0.1;

axis tight
% x lim
xlims=get(gca,'Xlim');
dx=relax*(xlims(2)-xlims(1));
xlim([xlims(1)-dx,xlims(2)+dx])
% ylim
ylims=get(gca,'Ylim');
dy=relax*(ylims(2)-ylims(1));
ylim([ylims(1)-dy,ylims(2)+dy])

