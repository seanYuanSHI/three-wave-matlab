% This function plot resonance frequencies as
% functions of the B field at fixed theta.
% Plasma parameters are loaded from ../../species.txt
%
% Inputs:
%    theta : angle of wave propagation, in deg, scalar
%    B0    : magnetic field strength in MG, length 1-by-nB
% Output:
%    wr    : resonances frequencies, in Trad/s
%            size (ns+1)-by-nB, where ns is the 
%            number of plasma species
%
% Written by Yuan SHI, Nov 25, 2020

function wr=plot_resonance_B0(theta,B0)
    % prepare data
    nB = length(B0);
    para = Parameter(B0(1));
    ns = length(para.Ws);
    wr = zeros(ns+1,nB);
    % load data
    for i=1:nB
        para = Parameter(B0(i));
        wr(:,i)=resonance(theta,para);
    end

    % plot figure
    plot(B0,wr,'LineWidth',3)
    % mark x axis
    hold on
    plot(B0,zeros(1,nB),'k--')
    % annotate figure
    xlabel('B0 (MG)')
    ylabel('wr (Trad/s)')
    set(gcf,'Color','w')
