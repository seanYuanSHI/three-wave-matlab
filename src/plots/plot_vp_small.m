% This script plot the phase speed for gapless modes
% as function of angles

B0=0.1;
nt=101;

para=Parameter(B0);

% angles
t=linspace(0,180,nt);

% initial calculation
in2=in2_small(0,para,0);
nn=length(in2);

% initialize output
vp=zeros(nn,nt);

% compute all angles
for i=1:nt
    vp(:,i)=sqrt(in2);
    in2=in2_small(t(i),para,0);
end

% plot figure
clf
plot(t,vp,'linewidth',2.5)
xlabel('\theta')
ylabel('v_p/c')

Ns=para.Ns;
Ts=para.Ts;
Ms=para.Ms;
Va=para.Va;
Cs=para.Cs;
title(['B0=',num2str(B0),'MG, n(cc)=',num2str(Ns'),...
    ', T(keV)=',num2str(Ts'),' M=',num2str(Ms'),...
    ', Va=',num2str(Va),', Cs=',num2str(Cs')])
set(gcf,'color','w')

