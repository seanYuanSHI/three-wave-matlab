% This script compare timing of Weierstrass v.s. Lebesque root finding
% when applied to polynomials whose roots are separate in scale
%
% Consider a third oprder polynomial with roots x1<x2< x3
% d12=x2-x1, d23=x3-x2. The program scan timing for ratio d23/d12
%

x1=0;
x2=0.01;
nx=12;

% absolute convergence criteria
ex=1e-12;
% max number of iteration
iter=100;

% record timing
tw=zeros(nx,1);
tl=zeros(nx,1);
t2=zeros(nx,1);

for i=1:nx
    x3=10^i;
    % construct function handel
    fun=@(x)(x-x1).*(x-x2).*(x-x3);
    
    % root finding by Weierstrass
    tic;
    [proots,~]=poly_roots_MC(fun,3,1);
    proots=poly_roots_Weierstrass(fun,proots,iter,ex,0);
    tw(i)=toc;
    
    % root finding by Lebesque
    tic;
    [roots,~]=zeros_continue(fun,x1-1,x3+1,ex,0,0,3,100,iter);
    tl(i)=toc;
    
    % root by Lebesque but use knowledge of scale speparation
    tic
    zeros_continue(fun,x1-1,x2+1,ex,0,0,3,100,iter);
    zeros_continue(fun,x3-1,x3+1,ex,0,0,3,100,iter);
    t2(i)=toc;
    
end

% plot timing
clf
plot(tw)
hold on
plot(tl)
plot(t2)
legend('Weierstrass','Lebesque','apriori Lebesque' ,'Location','northwest')
legend boxoff