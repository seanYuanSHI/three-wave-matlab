% This script plot backward parallel AAS when where are multiple ions
close all
clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust
flag=2; % compute data and plot
% flag=2; % read data and plot

B0=1e-5;
w1max=1e-6;
w1min=1e-8;

np=32;
t1=2;

% Alfven branch
% two species
% b1=5;
% b2=5;
% three species
b1=6;
b2=6;

% bounds of resonance search, in units of ck1r
ckmin=0.1;
ckmax=5;

% resonance branch
blist=[1,2]; 

% file path
%fstem='../data/';
fstem='../data/Corona/Artificial/A-S2/';
% font size
fs=16;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load setup
setup.b1=b1; % Alfven branch
setup.b2=b2; % Alfven branch

setup.t1=t1; % near parallel pump
setup.t2=180-t1; % near backscattering
setup.p2=180; % parallel daughter

setup.ckmin=ckmin;
setup.ckmax=ckmax;

% plasma parameter
para=Parameter(B0);
wp=para.wp; % plasma frequency
Ms=para.Ms; % mass of species

% abs electron gyro frequency
Ws=para.Ws;
We=max(abs(Ws));

% other parameters for labeling purpose only
Zs=para.Zs;
Ts=para.Ts;
Ns=para.Ns;

fname=strcat(fstem,'data.mat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot dispersion relation
omega=ck_2_w_warm(linspace(0,2*w1max,100),t1,para,1);
Va=para.Va;
Cs=para.Cs;
Vp=max([Va;Cs]);
ylim([0 2*w1max*Vp])
xlabel('ck (Trad/s)')
ylabel('\omega (Trad/s)')
set(gcf,'color','w')
set(gca,'Fontsize',fs)

if flag==1 % compute
    % compute and plot for each frequency
    logw1max=log10(w1max);
    logw1min=log10(w1min);
    logwlist=linspace(logw1min,logw1max,np);
    w1=10.^logwlist;
    
    % allocate array
    % Sound speed
    Cs=para.Cs;
    nc=length(Cs);
    
    w2=zeros(2*nc+2,np);
    b3=zeros(2*nc+2,np);
    C=zeros(2*nc+2,np); 
    Gw1=zeros(2*nc+2,np); 

    for i=1:np
        disp(['i/np=',num2str(i/np)])
        % compute scattering
        w1tmp=w1(i);
        w1tmp2=w1tmp^2;
        setup.w1=w1tmp;
        map=main_map(setup,para,fstem,-1);
        
        % unload data
        w2map=squeeze(map.w2);
        b3map=squeeze(map.b3);
        Cmap=squeeze(map.C);
        
        nw=length(w2map);
        if nw>0 % resonance found
            % load data
            w2(1:nw,i)=w2map;
            b3(1:nw,i)=b3map;
            C(1:nw,i)=abs(Cmap);
            
            % compute Gw1
            % growth rate=w1*Gw1*(B1/B0)
            for iw=1:nw
                w2tmp=w2(iw,i);
                if w2tmp~=0
                    w3tmp=w1tmp-w2tmp;
                    Gw1(iw,i)=C(iw,i)*We/w1tmp2/sqrt(w2tmp*w3tmp);
                end
            end
        end
    end    
    % save data
    save(fname,'w1','w2','b3','C','Gw1')
else
    % read data
    load(fname)    
end
    

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%%
% title str
str=['B0=',num2str(B0),' MG, mass=',num2str(Ms'),...
    ', charge=',num2str(Zs'),', n(cc)=',num2str(Ns'),...
    ', T (keV)=',num2str(Ts')];

% plot all branches
nb=length(blist);
%f1=w1/2/pi*1e12;
f1=w1;
for ib=1:nb
    if sum(b3(ib,:))~=0
        % plot frequency
        figure
        semilogx(f1,w2(ib,:)/w1,'.-','Linewidth',1)
        grid on
        xlabel('\omega_1 (Trad/s)','Fontsize',fs)
        %xlabel('f_1 (Hz)','Fontsize',fs)
        ylabel('\omega_2/\omega_1','Fontsize',fs)
        set(gca,'Fontsize',fs)
        title(str,'Fontsize',10)
        set(gcf,'color','w')
        
        % plot coupling
        figure
        semilogx(f1,C(ib,:),'.-','Linewidth',1)
        grid on
        xlabel('\omega_1 (Trad/s)','Fontsize',fs)
        %xlabel('f_1 (Hz)','Fontsize',fs)
        ylabel('C (Trad/s)^2','Fontsize',fs)
        set(gca,'Fontsize',fs)
        title(str,'Fontsize',10)
        set(gcf,'color','w')
        
        % plot growth rate
        figure
        semilogx(f1,Gw1(ib,:),'.-','Linewidth',1)
        grid on
        xlabel('\omega_1 (Trad/s)','Fontsize',fs)
        %xlabel('f_1 (Hz)','Fontsize',fs)
        ylabel('\gamma_0/\omega_1','Fontsize',fs)
        set(gca,'Fontsize',fs)
        title(str,'Fontsize',10)
        set(gcf,'color','w')
        
        % plot branch
        figure
        semilogx(f1,b3(ib,:),'.-','Linewidth',1)
        grid on
        xlabel('\omega_1 (Trad/s)','Fontsize',fs)
        %xlabel('f_1 (Hz)','Fontsize',fs)
        ylabel('b3','Fontsize',fs)
        set(gca,'Fontsize',fs)
        title(str,'Fontsize',10)
        set(gcf,'color','w')
    end
end
