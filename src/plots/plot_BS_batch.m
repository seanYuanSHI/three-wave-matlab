% This script compute multiple backscattering data
% comment parameters in plot_BS.m to avoid overwritting

fbase='../data/w1-B81/';

for b1=1:2
    for b2=1:2
        fstem=[fbase,num2str(b1),'-',num2str(b2),'/'];
        disp(['Computing ',fstem,' started on ',datestr(now)])
        plot_BS
        disp([fstem,' finished on ',datestr(now),char(10),char(10)])
    end
end

        
        