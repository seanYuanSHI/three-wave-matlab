% This script plot asymptotic dispersion for ck against exact dispersion

B0=1;
theta=30;
nk=1000;

para=Parameter(B0);
ckmax=1e3*para.wa;
ck=linspace(0,ckmax,nk);
clf

% plot exact
omega=ck_2_w_warm(ck,theta,para,0);
%wsmall=ck_2_w_small(ck,theta,para,0);
[wc2,vps,ngl,wsmall]=ck_2_w_small(ck,theta,para,0);

% labels for figures
Ns=para.Ns;
Ts=para.Ts;
Ms=para.Ms;
Va=para.Va;
Cs=para.Cs;


% plot low and high frequency modes separately
nw=length(omega(:,1));
nh=nw-ngl;

if nh<nw
    subplot(2,1,1)
end
plot(ck,omega(1:nh,:),'.')
hold on
plot(ck,wsmall(1:nh,:))
xlabel('ck (Trad/s):  1000*wa')
ylabel('\omega (Trad/s)')
ylim([omega(nh,1)/2 omega(1,nk)*1.1])
title(['B0=',num2str(B0),'MG, n(cc)=',num2str(Ns'),...
    ', M=',num2str(Ms')])
    
if nh<nw
    subplot(2,1,2)
    plot(ck,omega(nh+1:nw,:),'.')
    hold on
    plot(ck,wsmall(nh+1:nw,:))
    xlabel('ck (Trad/s):  1000*wa')
    ylabel('\omega (Trad/s)')
    ylim([0 wsmall(nh+1,nk)])
    title(['T(keV)=',num2str(Ts'),...
        ', Va=',num2str(Va),', Cs=',num2str(Cs)])
end

set(gcf,'color','w')

