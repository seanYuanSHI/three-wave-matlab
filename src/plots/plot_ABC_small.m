% This script plot dispersion coefficients A,B,C
% and compare with low w asymptotics

B0=1;
theta=10; % angle of propagation
nref=2.3; % refractive index
nw=100;   % number of plot points

para=Parameter(B0);
wmax=para.wa;  % max w
disp(['w max=',num2str(wmax)])

% For fixed n=ck/w, plot as function of w
w=linspace(0,wmax,nw);
ck=nref*w;

% allocate array
% exact
Ae=zeros(1,nw);
Be=zeros(1,nw);
Ce=zeros(1,nw);

% compute exact
for i=1:nw
    [Ae(i),Be(i),Ce(i),~]=wck_2_ABCF_warm(w(i),ck(i),theta,para,0);
end

% compute approximate
wps2=para.wps2;
Ws=para.Ws;
us2=para.us2;

% number of species
ns=length(Ws);

% characateristic sums
I0=1;
I1=0;
I2=0;
n2=nref^2;
ct=cos(theta/180*pi);
ct2=ct^2;
st2=1-ct2;
for i=1:ns
    ker=wps2(i)/(1-us2(i)*n2*ct2);
    I2=I2+ker;
    I1=I1+ker/Ws(i);
    I0=I0+ker/Ws(i)^2;
end

% asymptotic values for small w
Asw2=-I2*ct2;

Ca=para.Ca;
iCa2=1/Ca^2;

Bsw2=(I0*I2-I1^2)*st2-2*iCa2*I2;
Csw2=((I0*I2-I1^2)*st2-iCa2*I2)*iCa2/ct2;

% plot figure
w2=w.^2;
clf
subplot(3,1,1)
plot(w,Ae.*w2,'r','linewidth',2.5)
hold on
plot(w,Asw2*ones(1,nw),'m--','linewidth',3)
xlabel('w')
ylabel('A*w^2')

subplot(3,1,2)
plot(w,Be.*w2,'b','linewidth',2.5)
hold on
plot(w,Bsw2*ones(1,nw),'c--','linewidth',3)
xlabel('w')
ylabel('B*w^2')

subplot(3,1,3)
plot(w,Ce.*w2,'k','linewidth',2.5)
hold on
plot(w,Csw2*ones(1,nw),'--','Color',[1,1,1]/2,'linewidth',3)
xlabel('w')
ylabel('C*w^2')

set(gcf,'color','w')

