% This function plot resonance frequencies as
% functions of the wave propagation angle.
%
% Inputs:
%    para  : structure containing input parameters
%    theta : angle of wave propagation, in deg, size 1-by-nt
% Output:
%    wr    : resonances frequencies, in Trad/s
%            size (ns+1)-by-nt, where ns is the 
%            number of plasma species
%
% Written by Yuan SHI, Nov 25, 2020

function wr = plot_resonance_theta(para,theta)
    % if theta unspecified, use default array
    if nargin<2 
        % x axis of the plot
        nt = 100;
        theta=linspace(0,90,nt);
    else
        nt = length(theta);
    end

    % prepare data
    ns = length(para.Ws);
    wr = zeros(ns+1,nt);
    for i=1:nt
        wr(:,i)=resonance(theta(i),para);
    end

    % plot figure
    plot(theta,wr ,'LineWidth',3)
    % mark x axis
    hold on
    plot(theta,zeros(1,nt),'k--')
    % annotate figure
    xlabel('theta (deg)')
    ylabel('wr (Trad/s)')
    set(gcf,'Color','w')
