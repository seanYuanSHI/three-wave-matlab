% This script plot cold against analytical approximations for 
% upper-hybrid and lower-hybrid scattering for O-wave pump/daughter
% perp to B0 in two species plasma
%
% Exact perp is slow to converge, use near perp instead

clf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust
flag=1; % compute data and plot
% flag=2; % read data and plot

w1=600;
B0=5;
fstem='../data/';
dformt=0; % data form
np=32;

b=2; % b=1 X wave 
     % b=2 O wave
         

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load setup
setup.b1=b; 
setup.b2=b; 
setup.w1=w1;
setup.t1=89;

p2=linspace(2.5,179.5,np);
setup.t2=91*ones(1,np);
setup.p2=p2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
para=Parameter(B0);
wp=para.wp; % plasma frequency
Ms=para.Ms; % mass of species
ns=length(Ms);
assert(ns==2,'This program only checks magnetized e-i plasma!')

% other parameters for labeling purpose only
Zs=para.Zs;
Ts=para.Ts;
Ns=para.Ns;

% plot dispersion relation
% omega=ck_2_w_warm(linspace(0,2*w1,100),0,para,1);
% figure

% compute exact data
if flag==1
    disp('computing map...')
    map=main_map(setup,para,fstem,dformt);
else
    disp('reading data...')
    [map,setup]=read_data(fstem,dformt);
end

% squeeze extra dimensions
w2=squeeze(map.w2);
M=squeeze(map.M);
w2=w2(:,1:2);
M=M(:,1:2);

% plasma wave frequency
w3=w1*ones(np,2)-w2;

% compute UH and LH frequencies in cold plasma
%     w^4-B*w^2+C=0
% B=We^2+Wi^2+wp^2
% C=We^2*Wi^2+Wpe2*Wi^2+Wpi2*We^2
Ws=para.Ws;
wps2=para.wps2;
We2=Ws(1)^2;wpe2=wps2(1);
Wi2=Ws(2)^2;wpi2=wps2(2);
B=We2+Wi2+wp^2;
C=We2*Wi2+wpi2*We2+wpe2*Wi2;
F=sqrt(B^2-4*C);
wUH2=(B+F)/2;wUH=sqrt(wUH2);
wLH2=(B-F)/2;wLH=sqrt(wLH2);

% geometric factor
alpha=p2'/180*pi;
sa=sin(alpha/2);

% compute approximate data
MUH=sa.*sqrt(wp./w3(:,1));

aWe=max(abs(Ws));
MLH=sa*wp*sqrt(wp*wLH)/wUH/aWe;


% plot frequency
subplot(1,2,1)
plot(p2',w3(:,1),'r','Linewidth',2)
hold on
plot(p2',w3(:,2),'b','Linewidth',2)
xlabel('\alpha')
ylabel('\omega_3 (Trad/s)')
title(['\omega_1=',num2str(w1),' Trad/s, mass=',num2str(Ms'),', charge=',num2str(Zs')])

% plot normalized growth rates
subplot(1,2,2)
plot(p2',M(:,1),'r','Linewidth',2)
hold on
plot(p2',MUH,'m--','Linewidth',3)
title(['n(cc)=',num2str(Ns'),', T (keV)=',num2str(Ts')])

plot(p2',M(:,2),'b','Linewidth',2)
hold on
plot(p2',MLH,'c--','Linewidth',3)

xlabel('\alpha')
ylabel('M')

l=legend('UH exact','UH low-T O-wave','LH exact','LH low-T O-wave & wp~We');
set(l,'Location','southeast')
legend boxoff
set(gcf,'color','w')
