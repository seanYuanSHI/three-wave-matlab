% This script plot a single case 
%close all % figure
%clear     % data

cd .. % enter /src directory
addpath(genpath('./'))
% file path for saving data, default filename is data.mat
fstem='../data/NIF/N64/N64_T1_B80_t0_w5366_b2-4_backward/'; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust

% flag = 1: compute data and plot
% flag = 2: read data and plot
% flag = 3: update plots assuming all data already in workspace
flag=2; 

% blist : list of branches to plot
%blist=[1,2,3];
blist = 2;

% plasma parameters
B0 = 80; % in MG

% pump parameters
f1=854; % in THz, 0.351um
%f1=299.8; % in THz, 1um
w1=2*pi*f1;  % in Trad/s
%w1 = 9733; % in Trad/s
t1 = 0;  % in deg
b1 = 1;   % branch index

% probe parameters
t2 = 180; % in deg
p2 = 180; % in deg 
b2 = 4;   % branch index

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% linear wave data
if flag~=3
    % initialize para
    disp('Initializing plasma...')
    para=Parameter(B0);
    disp(para)

    % compute and plot linear dispersion
    disp('Computing wave dispersion relation at pump angle...')
    ckmax = 2*w1;
    ck = linspace(-ckmax,ckmax,1000);
    omega=ck_2_w_warm(ck,t1,para,0);
end

% computing three-wave data
if flag==1 % compute data
    % load parameters
    setup.w1 = w1;
    setup.t1 = t1;
    setup.b1 = b1;
    
    setup.t2 = t2;
    setup.p2 = p2;
    setup.b2 = b2;

    disp('Computing scattering map...')
    disp(setup)
    map=main_map(setup,para,fstem,0); % 0 for matlab format
elseif flag==2 % load existing data
    disp('Reading data...')
    [map,setup]=read_data(fstem,0); % 0 for matlab format
else
    disp('Data already in workspace')
end


% plot figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Plotting figure...')
figure

% plot dispersion
plot(ck,real(omega),'.-')
hold on

% mark light cone and axis
line([0,ckmax],[0,ckmax],'Color',[1 1 1]/2)
line([0,-ckmax],[0,ckmax],'Color',[1 1 1]/2)
line([-ckmax,ckmax],[0,0],'Color',[1 1 1]/2)
line([0,0],[0,ckmax],'Color',[1 1 1]/2)
ylim([0 ckmax])
% axis labels
xlabel('ck (Trad/s)')
ylabel('w (Trad/s)')
set(gcf,'color','w')
box off

% plot arrows if colinear 
% near forward scattering
if abs(t1-t2)<1e-3 && abs(p2)<1e-3 
   % sign of probe
   s2 = 1; % same direction as pump
   plotarrow = true;
% backward scattering
elseif abs(t1+t2-180)<1e-3 && abs(p2-180)<1e-3
    % sign of probe
    s2 = -1; % opposite direction as pump
    plotarrow = true;  
else
    plotarrow = false;
end

% plot arrows in current axis
if plotarrow
    % pump arrows
    ck1r=map.ck1r;
    p1=[ck1r w1];

    % probe arrow
    ck2r=squeeze(map.ck2r);
    w2=squeeze(map.w2);
    ck2rb=ck2r(blist);
    w2b=w2(blist);
    p2=[s2*ck2rb w2b];

    % media arrow
    p3=[ck1r-s2*ck2rb w1-w2b];

    % plot auxilliary arrows
    quiver(p2(1),p2(2),p3(1),p3(2),0,'Linestyle','--',...
        'Color',[1 1 1]/2,'Linewidth',2)
    quiver(p3(1),p3(2),p2(1),p2(2),0,'Linestyle','--',...
        'Color',[1 1 1]/2,'Linewidth',2)
    % plot main arrows
    quiver(0,0,p1(1),p1(2),0,'Color','k','Linewidth',2)
    quiver(0,0,p2(1),p2(2),0,'Color','k','Linewidth',2)
    quiver(0,0,p3(1),p3(2),0,'Color','k','Linewidth',2.5,'MaxHeadSize',0.5)
end

