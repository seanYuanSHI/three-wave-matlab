% This script plot figure 1 of the paper
close all
clear

% parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% global parameters
 B0=2.5;         % MG
 % mi/me=5;      % mass ratio
 % Te=Ti=3.2 KeV   % temperature
 % ne=ni=1e18cc; % density
 % pe=pi=3;      % p-index
 para=Parameter(B0);

 % wave vector parameters 
 nk=1000;             % number of plot points >1   
 ktheta=30;           % polar angle of k in deg
 kphi=45;            % azimuthal angle of k in deg
 
 % polarization transformation parameters %%%%%%%%
 %    trans: polarization transformation parameters, 3D array nb*np*4
 %        nb: number of dispersion branches
 %        ns: maxim number of segments among all branches
 %        trans(ib,ip,1:3)=(wi,wf,t1,t2)
 %           wi : begin-of-segment index
 %           wf : end-of-segment index
 %           t1 : type 1 transformation (phi,psi)->((-1)^t1*phi,psi+t1*180)
 %           t2:  type 2 transformations (phi,psi)->(phi+t2*180,psi)
 ns=3;
 nw=para.nw;
 trans=zeros(nw,ns,4);
 
 % specific for B0=2.5, theta=30, nk=1000;
 %     trans(1,1,:)=[1,nk,0,-2];
 %     trans(2,1,:)=[1,nk,-1,2];
 %     trans(2,2,:)=[ceil(nk*0.38)-7,nk,1,3];
 %     trans(3,1,:)=[1,nk,0,-1];
 %     trans(4,1,:)=[1,ceil(nk*0.16)+4,1,1];
 %     trans(5,1,:)=[ceil(nk*0.42)+8,ceil(nk*0.54)+7,1,1];
 %     trans(5,2,:)=[ceil(nk*0.66)+2,nk,1,1];
 %     trans(5,3,:)=[1,ceil(nk*0.02)-3,1,1];
 %     trans(6,3,:)=[1,nk,0,1];

% preparing plots %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
wp=para.wp;

% geometry
t=ktheta/180*pi;
st=sin(t);
ct=cos(t);

p=kphi/180*pi;
sp=sin(p);
cp=cos(p);

% report We/wpe
wps2=para.wps2;
wpe=sqrt(wps2(1));
Ws=para.Ws;
We=Ws(1);
disp(['We/wpe=',num2str(We/wpe)])
% report Va/Cs
Va=para.Va;
disp(['Va=',num2str(Va)])
Cs=para.Cs;
disp(['Cs=',num2str(Cs')])



% Plot dispersion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
kmax=2.5*wp;
ck0=linspace(0,kmax,nk+1);
ck=ck0(2:nk+1);
omega=ck_2_w_warm(ck,ktheta,para,1);
axis([0 kmax 0 kmax])
set(gcf,'Color','w')
box off

% plot polarization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

khat=[st*cp;st*sp;ct];
ckvec=zeros(3,nk);
for i=1:nk
    ckvec(:,i)=ck(i).*khat;
end

% compute/plot for each mode
% unit polarization vector
ehat=zeros(3,nk,nw);
for ib=1:nw
    w=omega(ib,:);
    [ehat(:,:,ib),phi,psi]=wck_2_e_warm(w,ckvec,para,0);
    
    % apply transformations
    for ip=1:ns
        % index before break point
        wi=trans(ib,ip,1); 
        wf=trans(ib,ip,2); 
        if wi>0 % is not null transformation
            
            % T1 transformation (phi,psi)->(-phi,psi+t2*180)
            t1=round(trans(ib,ip,3)); % make sure its integer
            phi(wi:wf)=(-1)^t1*phi(wi:wf);
            psi(wi:wf)=psi(wi:wf)+t1*180;
            
            % T1 transformation (phi,psi)->(phi+t1*180,psi)
            t2=round(trans(ib,ip,4)); % make sure its integer
            phi(wi:wf)=phi(wi:wf)+t2*180;
            
        end
    end
    
    % plot
    subplot(1,2,1)
    plot(phi,w,'-','linewidth',2.5)
    hold on
    grid on
    xlabel('phi')
    ylabel('w')
    ax=gca;
    ax.XTick=[-270 -180 -90 0 90 180 270];
    axis([-300 300 0 kmax])
    box off
    
    subplot(1,2,2)
    plot(psi,w,'-.','linewidth',2.5)
    hold on
    grid on
    xlabel('psi')
    ylabel('w')
    ax=gca;
    ax.XTick=[-270 -180 -90 0 90 180 270];
    axis([-300 300 0 kmax])
    box off
end

set(gcf,'Color','w')

% plot energy coefficient %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

% compute/plot for each mode
u=zeros(1,nk);
for ib=1:nw
    w=omega(ib,:);
    
    % compute for each wave vector
    for ik=1:nk
        % wave energy operator
        H2=Energy_warm(w(ik),ckvec(:,ik),para);
        % unit polarization vector
        e=ehat(:,ik,ib);
        % wave energy coefficient
        u(ik)=e'*H2*e;
    end
    
    % plot
    semilogx(real(u),w,'-','linewidth',2.5)
    hold on
    grid on
    xlabel('u')
    ylabel('w')
end

set(gcf,'Color','w')
