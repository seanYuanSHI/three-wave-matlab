% This script plot backward parallel AAS against analytic approximation
% in two species plasma
% scale B, n, w ck to make numbers closer to O(1)
close all
clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust
flag=1; % compute data and plot
% flag=2; % read data and plot

B0=1e-6;
w1max=2*pi*1e-13;
w1min=2*pi*1e-15;
fstem='../data/';
np=16;
t1=2;

% bounds of resonance search, in units of ck1r
ckmin=0.2;
ckmax=2;

ir=1; % resonance branch

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load setup
setup.b1=5; % Alfven branch
setup.b2=5; % Alfven branch
setup.t1=t1; % near parallel pump
setup.t2=180-t1; % near backscattering
setup.p2=180; % parallel daughter

setup.ckmin=ckmin;
setup.ckmax=ckmax;

% plasma parameter
para=Parameter(B0);
wp=para.wp; % plasma frequency
Ms=para.Ms; % mass of species
ns=length(Ms);
assert(ns==2,'This program only checks two-species plasma!')

% ratio of ion mass over electron mass
m1=Ms(1);
m2=Ms(2);
[mi,ind]=max([m1/m2,m2/m1]);

% ion gyro frequency
Ws=para.Ws;
Wi=Ws(ind);

% other parameters for labeling purpose only
Zs=para.Zs;
Ts=para.Ts;
Ns=para.Ns;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot dispersion relation
%omega=ck_2_w_warm(linspace(0,2*w1max,100),0,para,1);
%figure
fname=strcat(fstem,'data.mat');

if flag==1 % compute
    % Alfven speed
    Va=para.Va;
    Ca=para.Ca;
    Ca2=Ca^2;
    % Sound speed
    Cs=para.Cs;
    nc=length(Cs);
    assert(nc==1,'one sound wave needed for AAS scattering!')
    % prefactor for analytical result
    pre=Ca2/Va/Cs/4/mi/Wi;
    
    
    % compute and plot for each frequency
    logw1max=log10(w1max);
    logw1min=log10(w1min);
    logwlist=linspace(logw1min,logw1max,np);
    wlist=10.^logwlist;
    
    w2list=zeros(1,np);
    Cexact=zeros(1,np);
    Capprox=zeros(1,np);
    for i=1:np
        disp(['i/np=',num2str(i/np)])
        % compute scattering
        w1=wlist(i);
        %iw12=1/w1^2;
        %iw13=1/w1^3;
        setup.w1=w1;
        map=main_map(setup,para,fstem,-1);
        
        % unload data
        w2map=map.w2;
        Cmap=map.C;
        
        nw=length(w2map);
        if nw>0 % resonance found
            % select AAS scattering
            w2=w2map(ir);
            w2list(i)=w2/w1;
            %Cexact(i)=Cmap(1)*iw13*Wi;
            Cexact(i)=Cmap(ir);
            
            % compute analytic approximation
            w3=w1-w2;
            Capprox(i)=pre*w2*w3*w1;
        end
    end
    
    % save data
    save(fname,'wlist','Cexact','Capprox')
else
    % read data
    load(fname)    
end
    

% plot frequency
clf

subplot(1,2,1)
semilogx(wlist,w2list,'.-','Linewidth',1)
xlabel('\omega_1 (Trad/s)')
ylabel('\omega_2/\omega_1')
title(['B0=',num2str(B0),' MG, mass=',num2str(Ms'),', charge=',num2str(Zs')])
%ylim([ckmin ckmax])

% plot growth rate
subplot(1,2,2)
% iw3=4*mi*Wi./wlist.^3;
% semilogx(wlist,Cexact.*iw3,'r','Linewidth',2)
% hold on
% semilogx(wlist,Capprox.*iw3,'m--','Linewidth',3)
% ylabel('4M_i\Omega_i\Gamma/\omega_1^3')
%ylabel('\Gamma')
semilogx(wlist,abs(Capprox./Cexact),'.-','Linewidth',1)
grid on
xlabel('\omega_1 (Trad/s)')
ylabel('\Gamma/\Gamma_{//}')
title(['n(cc)=',num2str(Ns'),', T (keV)=',num2str(Ts')])
%ylim([0 2])

%l=legend('AAS exact','AAS approx');
%l=legend('AAS approx/exact');
%set(l,'Location','northwest')
%legend boxoff
set(gcf,'color','w')
