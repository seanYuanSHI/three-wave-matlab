% This script plot fig 4 of the paper
close all
clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust
flag=2; % compute data and plot
% flag=2; % read data and plot

B0=1e-6;

w1max=2*pi*1e-13;
w1min=2*pi*1e-15;

t1=2; % near parallel

nf=3; % number of points for w 
np=4; % number of points for t

% bounds of resonance search, in units of ck1r
ckmin=0.2;
ckmax=100;

fstem='../data/';
fname=strcat(fstem,'data_wt.mat');

% branches to plot
%blist=[1,2,3];
blist=1;
% number of contour levels
nct=32;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load setup
setup.b1=5; % Alfven branch
setup.b2=5; % Alfven branch

setup.t1=t1; % near parallel pump

% backward scattering
t2=linspace(92,178,np);
setup.t2=t2;
setup.p2=180*ones(1,np);

setup.ckmin=ckmin;
setup.ckmax=ckmax;

% plasma parameter
para=Parameter(B0);
wp=para.wp; % plasma frequency
Ms=para.Ms; % mass of species
ns=length(Ms);
assert(ns==2,'This program only plot two-species plasma!')

% ratio of ion mass over electron mass
m1=Ms(1);
m2=Ms(2);
[mi,ind]=max([m1/m2,m2/m1]);

% ion gyro frequency
Ws=para.Ws;
Wi=Ws(ind);

% other parameters for labeling purpose only
Zs=para.Zs;
Ts=para.Ts;
Ns=para.Ns;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Alfven speed
Va=para.Va;
Ca=para.Ca;
Ca2=Ca^2;
% Sound speed
Cs=para.Cs;
nc=length(Cs);
assert(nc==1,'one sound wave needed for AAS scattering!')

% compute and plot dispersion relation
omega=ck_2_w_warm(linspace(0,w1max/max(Ca,Cs),100),0,para,1);

if flag==1 % compute
    % prefactor for analytical result
    pre=Ca2/Va/Cs/4/mi/Wi;    
    
    % compute and plot for each frequency
    logw1max=log10(w1max);
    logw1min=log10(w1min);
    logwlist=linspace(logw1min,logw1max,nf);
    w1=10.^logwlist;
    
    % store possible F,A,S branch in two-species plasma
    w2=zeros(nf,np,3);
    b3=zeros(nf,np,3);
    Cn=zeros(nf,np,3);
    % loop through pump frequency
    for iw=1:nf
        disp(['i/nf=',num2str(iw/nf)])
        % compute scattering
        w1tmp=w1(iw);
        %iw12=1/w1^2;
        %iw13=1/w1^3;
        setup.w1=w1tmp;
        map=main_map(setup,para,fstem,-1);
        
        % unload data
        w2map=squeeze(map.w2);
        b3map=squeeze(map.b3);
        Cmap=squeeze(map.C);
        
        nw=size(w2map,2);
        if nw>0 % resonance found
            for ib=1:nw
                % frequency
                w2tmp=w2map(:,ib);
                w2(iw,:,ib)=w2tmp'/w1tmp;
                
                % branch index
                b3tmp=b3map(:,ib);
                b3(iw,:,ib)=b3tmp';
                
                % compute analytic approximation
                w3tmp=w1tmp-w2tmp;
                Capprox=pre*w1tmp.*w2tmp.*w3tmp;
                % normalized coupling coefficient
                Ctmp=Cmap(:,ib)./Capprox;
                Cn(iw,:,ib)=abs(Ctmp');            
            end
        end
    end    
    % save data
    save(fname,'t2','w1','w2','Cn','b3')
else
    % read data
    load(fname)    
end
    

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% title string
str=['B0=',num2str(B0),' MG, mass=',num2str(Ms'),...
    ', charge=',num2str(Zs'),', n(cc)=',num2str(Ns'),...
    ', T (keV)=',num2str(Ts')];

[T2,F1]=meshgrid(t2,log10(w1/2/pi));

% plot all branches
nb=length(blist);
for ib=1:nb
    % plot frequency
    figure
    [~,h]=contourf(T2,F1,w2(:,:,ib),nct);
    set(h,'LineColor','none')
    c=colorbar;
    c.Label.String = '\omega_2/\omega_1';
    xlabel('\theta_2')
    ylabel('log10(f_1/THz)')
    title(str)
    set(gcf,'color','w')
    ylim([-14 -13])
    
    % plot growth rate
    figure
    [~,h]=contourf(T2,F1,Cn(:,:,ib),nct);
    set(h,'LineColor','none')
    c=colorbar;
    c.Label.String = '\Gamma/\Gamma_{//}';
    xlabel('\theta_2')
    ylabel('log10(f_1/THz)')
    title(str)
    set(gcf,'color','w')
    ylim([-14 -13])
    
    % plot branch index
%     figure
%     surf(T2,F1,b3(:,:,ib));
%     xlabel('\theta_2')
%     ylabel('log10(f_1/THz)')
%     zlabel('b3')
%     title(str)
%     set(gcf,'color','w')
end
