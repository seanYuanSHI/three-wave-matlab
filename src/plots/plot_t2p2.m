% This script plot figure 2 of the paper
close all
clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust

flag=1; % compute data and plot
% flag=2; % read data and plot

B0=2.5;
w1=75;
t1=30;

b1=2;
b2=3;

% number of plot points
np=8;
%np=32;

fstem='../data/';
dformat=0;

% blist : list of branches to plot
%blist=[1,2,3];
blist=1;

% plot map
%    flag  : flag=1 plot w1-w2
%            flag=2 plot ck2r/ck1r
%            flag=3 plot C/w1^2
%            flag=4 plot M
%            flag=5 plot b3
%            flag can be an array to plot multiple maps
pflag=[1,4,5];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
setup.b1=b1;
setup.b2=b2;
setup.w1=w1;
setup.t1=t1;

% only need to plot east hemisphere due to symmetry
p2=linspace(0,180,np);
% plot both north and south hemispheres
t2=linspace(0.5,179.5,np);
[P2,T2]=meshgrid(p2,t2);

setup.p2=P2;
setup.t2=T2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
para=Parameter(B0);

% compute data
if flag==1
    disp('computing map...')
    map=main_map(setup,para,fstem,dformat);
else
    disp('reading data...')
    [map,setup]=read_data(fstem,dformat);
end

% plot figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot_main_map(map,setup,flag,blist)
plot_main_map(map,setup,pflag,blist)

% plot forward scattering
setup0=setup;
setup0.p2=0;
setup0.t2=t1;
map0=main_map(setup0,para,fstem,-1);

% plot dispersion
figure
omega=ck_2_w_warm(linspace(-1.2*w1,1.2*w1,1000),t1,para,1);
% compute arrows
ck1r0=map0.ck1r;
p1=[ck1r0 w1];
ck2r0=squeeze(map0.ck2r);
w20=squeeze(map0.w2);
ck2rb=ck2r0(blist);
w2b=w20(blist);
p2=[ck2rb w2b];
p3=[ck1r0-ck2rb w1-w2b];
% plot auxilliary arrows
quiver(p2(1),p2(2),p3(1),p3(2),0,'Linestyle','--',...
    'Color',[1 1 1]/2,'Linewidth',2)
quiver(p3(1),p3(2),p2(1),p2(2),0,'Linestyle','--',...
    'Color',[1 1 1]/2,'Linewidth',2)
% plot arrows
quiver(0,0,p1(1),p1(2),0,'Color','k','Linewidth',2)
quiver(0,0,p2(1),p2(2),0,'Color','k','Linewidth',2)
quiver(0,0,p3(1),p3(2),0,'Color','k','Linewidth',2.5,'MaxHeadSize',0.5)
% axis
line([0 0], [0 2*w1],'Color','k')
xlabel('ck (Trad/s)')
ylabel('\omega (Trad/s)')
set(gcf,'Color','w')
box off




