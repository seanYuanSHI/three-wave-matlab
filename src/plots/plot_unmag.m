% This script plot unmagnetized against analytic approximation
% in two species plasma
close all
clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust
flag=1; % compute data and plot
% flag=2; % read data and plot

w1=600;
%w1=299.792*2*pi;
%w1=w1*1e-5;
t1=90; % the angle does not matter in unmagnetized plasma
fstem='../data/';
dform=0; % data form 0: Matlab 
%np=32;
np=8;

% bounds in unit of ck1r
ckmin=0.5;
ckmax=1.0;

%blist=[1,2,3,4];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load setup
setup.b1=1;
setup.b2=1;
setup.w1=w1;
setup.t1=t1;

bounds.ckmin=ckmin;
bounds.ckmax=ckmax;

% y-y aligned polarization
t2=t1+linspace(5,179.5,np);
setup.t2=t2;
setup.p2=0*ones(1,np);
alpha=(t2-t1)';

% x-y rotating polarization
% p2=linspace(2.5,179.5,np);
% setup.t2=90*ones(1,np);
% setup.p2=p2;
% alpha=p2';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
para=Parameter(0);
wp=para.wp; % plasma frequency
Ms=para.Ms; % mass of species
ns=length(Ms);
assert(ns==2,'This program only checks two-species unmagnetized plasma!')

% ratio of ion mass over electron mass
m1=Ms(1);
m2=Ms(2);
mi=max(m1/m2,m2/m1);

% other parameters for labeling purpose only
Zs=para.Zs;
Ts=para.Ts;
Ns=para.Ns;

% plot dispersion relation
% omega=ck_2_w_warm(linspace(0,2*w1,100),0,para,1);
% figure

% compute exact data
if flag==1
    disp('computing map...')
    map=main_map(setup,para,fstem,dform);
else
    disp('reading data...')
    [map,setup]=read_data(fstem,dform);
end

% compute approximate data for Stocks scattering
w2=squeeze(map.w2);
M=squeeze(map.M);
w2=w2(:,1:2);
M=M(:,1:2);

% geometric factor
sa=sin(alpha/360*pi);

% plasma wave frequency
w3=w1*ones(np,2)-w2;
% plot frequency
subplot(1,2,1)
plot(alpha,w3(:,1),'r','Linewidth',2)
hold on
plot(alpha,w3(:,2),'b','Linewidth',2)
xlabel('\alpha')
ylabel('\omega_3 (Trad/s)')
title(['\omega_1=',num2str(w1),' Trad/s, mass=',num2str(Ms'),', charge=',num2str(Zs')])

subplot(1,2,2)
% compute and plot Raman
MR=sa.*sqrt(wp./w3(:,1))*(1-1/mi);
plot(alpha,M(:,1),'r','Linewidth',2)
hold on
plot(alpha,MR,'m--','Linewidth',3)
title(['n(cc)=',num2str(Ns'),', T (keV)=',num2str(Ts')])

% compute and plot Brillouin
uindex=para.uindex;
nt=length(uindex);
if nt>0
    MB=sa.*sqrt(wp./w3(:,2)/mi);
    plot(alpha,M(:,2),'b','Linewidth',2)
    hold on
    plot(alpha,MB,'c--','Linewidth',3)
    l=legend('Raman exact','approx Raman low T','Brillouin exact','approx Brillouin low T');
else
    l=legend('Raman exact','approx Raman low T');
end

xlabel('\alpha')
ylabel('M')


set(l,'Location','southeast')
legend boxoff
set(gcf,'color','w')
