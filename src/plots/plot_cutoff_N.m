% This function plot the cutoff frequency as 
% a function of density for given B0.
% The species info is specified in species.txt file
% Inputs:
%    B0     : magnetic field in MG, scalar
%    Narray : vector of density values in 10^20 cc, 1-by-nN
% output:
%    wc     : cutoff frequencies, (ns+2)-by-nN
% 
% Written by Yuan SHI, Dec 3, 2020

function wc = plot_cutoff_N(B0,Narray)
    % dimension
    nN = length(Narray);

    % initialize parameters
    para = Parameter(B0);
    % number of species
    Ws = para.Ws;
    ns = length(Ws);

    % create standardized para
    para = Parameter(B0,ones(1,ns));
    wps2 = para.wps2;

    % initialize array
    if B0==0
        % unmagnetized
        wc = zeros(3,nN);
    else
        % magnetized
        wc=zeros(ns+2,nN);
    end

    % load values
    for i=1:nN
        % scale density
        wc(:,i)=cutoff(Narray(i)*wps2,Ws);
    end

    % plot figure
    plot(Narray,wc)
    xlabel('N (10^{20} cc)')
    ylabel('wc (Trad/s)')
    title(['B0=',num2str(B0),' MG'])
    set(gcf,'Color','w')
    AxisLessTight;    
    
