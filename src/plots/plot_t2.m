% This script plot fig 3 of the paper
close all
%clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust
flag=1; % compute data and plot
% flag=2; % read data and plot

f1=854.109; % THz <=> 0.351 um
%f1=299.792; % THz <=> 1um
B0=3; % MG
%B0=2; % MG
t1=0; % pump angle
%t1=80;

% branch index
b1=1;
b2=1;

% bounds for resonance search, in unit of ck1r
ckmin=0.5;
ckmax=1;

% branches to plot
blist=[1,2,3,4,5,6];

%fstem='../data/';
dform=0;
%np=128;
%np=32;
np=8;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load setup
setup.b1=b1;
setup.b2=b2;
setup.t1=t1;

w1=2*pi*f1;
setup.w1=w1;

%t2=linspace(87,93,np);
t2=linspace(2.5,179.5,np);
setup.t2=t2;
setup.p2=0*ones(1,np);

setup.ckmin=ckmin;
setup.ckmax=ckmax;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
para=Parameter(B0);

Ms=para.Ms; % mass of species
Zs=para.Zs;
Ts=para.Ts;
Ns=para.Ns;

str=['\omega_1=',num2str(w1),' Trad/s, B0=',num2str(B0),' MG',', t1=',num2str(t1),...
    ', b1=',num2str(b1),', b2=',num2str(b2),char(10),...
    'mass=',num2str(Ms'),', charge=',num2str(Zs'),...
    ', n(cc)=',num2str(Ns'),', T (keV)=',num2str(Ts')];


% compute exact data
%        map.w2   : frequency of scattered wave, in unit of Trad/s, nx-ny-nw
%        map.ck2r : wavevector of scattered wave, in unit of Trad/s, nx-ny-nw
%        map.ck1r : wavevector of pump, in unit of Trad/s, scalar
%        map.C    : coupling coefficient, in unit of (Trad/s)^2, nx-ny-nw
%        map.M    : growth rate, in unit of unmagnetized Raman, nx-ny-nw
%        map.b3   : branch index for the mediating wave, nx-ny-nw
if flag==1
    disp('computing map...')
    map=main_map(setup,para,fstem,dform);
else
    disp('reading data...')
    [map,setup]=read_data(fstem,dform);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot dispersion relation
% omega=ck_2_w_warm(linspace(0,2*w1,100),t1,para,2);
% xlabel('ck (Trad/s)')
% ylabel('\omega (Trad/s)')
% set(gcf,'color','w')

% plot frequency
figure
w2=squeeze(map.w2);
w3=w1-w2;
semilogy(t2,w3(:,blist),'.-','Linewidth',2)
title(str)
xlabel('\theta_2')
ylabel('\omega_3 (Trad/s)')
set(gcf,'color','w')
saveas(gcf,[fstem,'w3.png'])

% plot growth rats
figure
M=squeeze(map.M);
plot(t2,M(:,blist),'.-','Linewidth',2)
title(str)
xlabel('\theta_2')
ylabel('M')
set(gcf,'color','w')
saveas(gcf,[fstem,'M.png'])

% plot wave energy coefficients
figure
u123=abs(squeeze(map.u123));
semilogy(t2,u123(:,blist),'.-','Linewidth',2)
title(str)
xlabel('\theta_2')
ylabel('u1*u2*u3')
set(gcf,'color','w')
saveas(gcf,[fstem,'u123.png'])

% plot sum of abs C
figure
Sa=abs(squeeze(map.Sa));
semilogy(t2,Sa(:,blist),'.-','Linewidth',2)
title(str)
xlabel('\theta_2')
ylabel('Sa')
set(gcf,'color','w')
saveas(gcf,[fstem,'Sa.png'])

% plot branch indes
figure
b3=squeeze(map.b3);
plot(t2,b3(:,blist),'.-','Linewidth',2)
title(str)
xlabel('\theta_2')
ylabel('b3')
set(gcf,'color','w')
saveas(gcf,[fstem,'b3.png'])

% plot frequency-colored growth rate
figure
nb=length(blist);
for ib=1:nb    
    Mb=log10(M(:,blist(ib)))';
    z = zeros(size(t2));
    col=real(log10(w3(:,blist(ib))))'; % This is the color
    surface([t2;t2],[Mb;Mb],[z;z],[col;col],...
        'facecol','no',...
        'edgecol','interp',...
        'linew',4);
end
xlabel('\theta_2')
ylabel('log_{10}(M)')
set(gcf,'color','w')
c = colorbar;
c.Label.String = 'log_{10}(\omega_3/Trad/s)';
colormap jet
caxis([-2.5 3.5])
%xlim([0 180])
ylim([-5.5 0])
saveas(gcf,[fstem,'Mw3.png'])

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % swap rr and rl data
% fstem='../data/fig3/B3/';
% % fapp='/';
% % ind=63; % index before crossing 90 deg
% fapp='/t89-91/';
% ind=16;
% 
% load([fstem,'1-1',fapp,'data.mat'])
% w211=squeeze(map.w2);
% M11=squeeze(map.M);
% 
% 
% load([fstem,'1-2',fapp,'data.mat'])
% w212=squeeze(map.w2);
% M12=squeeze(map.M);
% 
% w1=setup.w1;
% w311=w1-w211;
% w312=w1-w212;
% 
% t2=setup.t2;
% np=length(t2);
% w3rr=[w311(1:ind,:);w312(ind+1:np,:)];
% w3rl=[w312(1:ind,:);w311(ind+1:np,:)];
% Mrr=[M11(1:ind,:);M12(ind+1:np,:)];
% Mrl=[M12(1:ind,:);M11(ind+1:np,:)];
% 
% save([fstem,'data_rl.mat'],'t2','Mrl','w3rl')
% save([fstem,'data_rr.mat'],'t2','Mrr','w3rr')
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % merge higher resolution rl data
% fstem='../data/fig3/B3/';
% % low resolution
% load([fstem,'data_rl_low.mat'])
% t2_low=t2;
% Mrl_low=Mrl;
% w3rl_low=w3rl;
% 
% % high resolution data
% load([fstem,'data_rl_high.mat'])
% t2_high=t2;
% Mrl_high=Mrl;
% w3rl_high=w3rl;
% 
% % merge and sort
% t2_merge=[t2_low,t2_high];
% [t2,ind]=sort(t2_merge,'ascend');
% Mrl_merge=[Mrl_low;Mrl_high];
% Mrl=Mrl_merge(ind,:);
% w3rl_merge=[w3rl_low;w3rl_high];
% w3rl=w3rl_merge(ind,:);
% 
% % save data
% save([fstem,'data_rl.mat'],'t2','Mrl','w3rl')
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % merge higher resolution rr data
% fstem='../data/fig3/B3/';
% % low resolution
% load([fstem,'data_rr_low.mat'])
% t2_low=t2;
% Mrr_low=Mrr;
% w3rr_low=w3rr;
% 
% % high resolution data
% load([fstem,'data_rr_high.mat'])
% t2_high=t2;
% Mrr_high=Mrr;
% w3rr_high=w3rr;
% 
% % merge and sort
% t2_merge=[t2_low,t2_high];
% [t2,ind]=sort(t2_merge,'ascend');
% Mrr_merge=[Mrr_low;Mrr_high];
% Mrr=Mrr_merge(ind,:);
% w3rr_merge=[w3rr_low;w3rr_high];
% w3rr=w3rr_merge(ind,:);
% 
% % save data
% save([fstem,'data_rr.mat'],'t2','Mrr','w3rr')
% 
