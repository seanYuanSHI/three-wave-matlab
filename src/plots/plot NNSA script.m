t1=30;
f1=284.0112;
w1=2*pi*f1;

% file folder name stem
fbase='../data/t1_';

% range of Mt and M
mm=0.5;

% folder of data files
if w1>0
    disp('Plot w1 on positive branch')
    ts='\Delta^+\omega';
    fp='s1p_s2p/';
    %fn='s1p_s2n/';
else
    disp('Plot w1 on negative branch')
    ts='\Delta^-\omega';
    fp='s1n_s2p/';
    fn='s1n_s2n/';
end
w1p=abs(w1);

% file name
fpstem=strcat(fbase,num2str(t1),'/',fp);
%fnstem=strcat(fbase,num2str(t1),'/',fn);

% Read data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read existing file ../data/w2.nc
% [lonp,latp,WRp]=read_w2_data(0,fpstem); % no plot here
% [lonn,latn,WRn]=read_w2_data(0,fnstem); % no plot here
[lon,lat,WRp]=read_w2_data(0,fpstem); % no plot here

% frequency shifts
WRp=WRp-w1p;
% WRn=WRn-w1p;

% read existing file ../data/M.nc
% [M,Mt]=read_M_data(0); % no plot here
Mp=read_M_data(0,fpstem); % no plot here
%Mn=read_M_data(0,fnstem); % no plot here

% Plot figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sw=size(WRp);
%nr=sw(3);
nr=1;
j=2;
%for j=1:nr
    % plot w2p
    % subplot(nr,4,(j-1)*4+1)
    %subplot(nr,1,(j-1)*3+1)
    figure
    cmin=min(min(WRp(:,:,j)));
    cmax=max(max(WRp(:,:,j)));
    Hammer_plot(lon,lat,WRp(:,:,j),cmin,cmax,[ts,' (Trad)'])
    % Hammer_plot(lonp,latp,WRp(:,:,j),cmin,cmax)
    % c.Label.String=['dwp',num2str(j)];
    %title(ts);
    xlabel('\phi_2')
    ylabel('\theta_2')
    
%     % plot w2n
%     subplot(nr,4,(j-1)*4+2)
%     cmin=min(min(WRn(:,:,j)));
%     cmax=max(max(WRn(:,:,j)));
%     Hammer_plot(lonn,latn,WRn(:,:,j),cmin,cmax)
%     c.Label.String=['dwn',num2str(j)];
    
    % plot Mp
    % subplot(nr,4,(j-1)*4+3)
    %subplot(nr,3,(j-1)*3+2)
    figure
    Hammer_plot(lon,lat,Mp(:,:,j),0,mm,'M^+')
    % Hammer_plot(lonp,latp,Mp(:,:,j),0,mm)
    % c.Label.String=['Mp',num2str(j)];
    %title('M^+');
    xlabel('\phi_2')
    ylabel('\theta_2')
    
    % plot Mn
    % subplot(nr,4,(j-1)*4+4)
%     subplot(nr,3,(j-1)*3+3)
%     Hammer_plot(lon,lat,Mn(:,:,j),0,mm,'M^-')
    % Hammer_plot(lonn,latn,Mn(:,:,j),0,mm)
    % c.Label.String=['Mn',num2str(j)];
    %title('M^-');
%     xlabel('\phi_2')
%     ylabel('\theta_2')
%end

%supertitle(['\theta_1=',num2str(t1),'^{\circ}'])

