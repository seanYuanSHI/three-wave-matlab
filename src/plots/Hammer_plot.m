% This function plot spherical data in hammer projection
% Inputs:
%      lon  : meshgrid of longitudes, in rad
%      lat  : meshgrid of latitude, in rad
%      Z    : meshgrid of data
%      cmin : color minimum data
%      cmax : color maximum data
%      cstring : colorbar label

function Hammer_plot(lon,lat,Z,cmin,cmax,cstring)

% check input
%assert(cmin<=cmax)
clim=sort([cmin,cmax],'ascend');
cmin=clim(1);
cmax=clim(2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ifrb=1; % if use red-blue color scheme
nc=64; % number of color levels

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% construct color map
if ifrb==1    
    % color map redblue
    rb=redblue(nc);
    
    % color scale 2*max abs
    cc=2*max(abs(cmin),abs(cmax));
    
    % color marker
    if cmin>0
        cl=nc/2;
    else
        cl=nc*(1/2+cmin/cc);
    end    
    
    if cmax<0
        cu=nc/2;
    else
        cu=nc*(1/2+cmax/cc);
    end  
    
    % color index
    il=floor(cl)+1;
    iu=floor(cu);
    
    % colormap
    colormap(gca,rb(il:iu,:));
end

% transform to hammer projection
[Hlon,Hlat]=sph2hammer(lon,lat);
contourf(Hlon,Hlat,Z,nc,'LineColor','none')
axesm ('hammer', 'Frame', 'off', 'Grid', 'on');

ch=colorbar;
set(get(ch,'title'),'string',cstring);
caxis([cmin cmax])

set(gcf,'Color','w')

ax = gca;
% yruler = ax.YRuler;
% yruler.Axle.Visible = 'off';
% xruler = ax.XRuler;
% xruler.Axle.Visible = 'off';
ax.FontSize=13;



