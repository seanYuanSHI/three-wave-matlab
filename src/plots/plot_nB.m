% This script scan n-B parameter space for fixed geometry
close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust
flag=1; % compute data and plot
% flag=2; % read data and plot

%f1=299.792; % THz <=> 1um
f1=56.5645; % TH <=> 5.3 um
%f1=28.2823; % TH <=> 10.6 um
t1=50; % pump angle
t2=130;% scatter polar
p2=180; % scatter azimuthal

% branch index
b1=1;
b2=1;

% bounds for resonance search, in unit of ck1r
ckmin=0.5;
ckmax=1.0;

% n and B range
nlog10min=-3; % n=10^18 cc ->-2
nlog10max=-1; % n=10^20 cc ->0

Blog10min=-2; % B=0.1 MG ->-1
Blog10max=1; % B=10 MG  ->1

% number of species
ns=2;
% number of modes
nw=4;

% branches to plot
%blist=[1,2,3,4];

fstem='../data/';

nn=32;
nb=32;
% nn=5;
% nb=5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load setup
setup.b1=b1;
setup.b2=b2;

w1=2*pi*f1;
setup.w1=w1;

setup.t1=t1;
setup.t2=t2;
setup.p2=p2;

setup.ckmin=ckmin;
setup.ckmax=ckmax;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize search array
Blog=linspace(Blog10min,Blog10max,nb);
B=10.^Blog;

nlog=linspace(nlog10min,nlog10max,nn);
N=10.^nlog;

% data
Mdata=zeros(nb,nn,nw);
w2data=zeros(nb,nn,nw);
b3data=zeros(nb,nn,nw);

fname=strcat(fstem,'data_nB.mat');
if flag==1
    disp('computing map...')
    % scan B and n
    for in=1:nn
        disp(['in/nn=', num2str(in/nn')])
        % density
        Ns=N(in)*ones(ns,1);
        for ib=1:nb
            disp(['ib/nb=', num2str(ib/nb')])
            para=Parameter(B(ib),Ns);    
            
            % compute exact data
            %        map.w2   : frequency of scattered wave, in unit of Trad/s, nx-ny-nw
            %        map.ck2r : wavevector of scattered wave, in unit of Trad/s, nx-ny-nw
            %        map.ck1r : wavevector of pump, in unit of Trad/s, scalar
            %        map.C    : coupling coefficient, in unit of (Trad/s)^2, nx-ny-nw
            %        map.M    : growth rate, in unit of unmagnetized Raman, nx-ny-nw
            %        map.b3   : branch index for the mediating wave, nx-ny-nw
            %map=main_map(setup,para,fstem,flag)
            map=main_map(setup,para,fstem,-1);
            
            w2=squeeze(map.w2);
            b3=squeeze(map.b3);
            M=squeeze(map.M);
            nm=length(M);
            
            if nm>0
                % load data
                Mdata(ib,in,1:nm)=M(1:nm);
                w2data(ib,in,1:nm)=w2(1:nm);
                b3data(ib,in,1:nm)=b3(1:nm);
            end
        end
    end
    % save data
    save(fname,'Mdata','w2data','b3data')               
else
    % read data
    load(fname)  
end

% info for message
Ms=para.Ms; 
Zs=para.Zs;
Ts=para.Ts;

str=['\omega_1=',num2str(w1),...
    ', t1=',num2str(t1),', t2=',num2str(t2),', p2=',num2str(p2),...
    ', b1=',num2str(b1),', b2=',num2str(b2),char(10),...
    'mass=',num2str(Ms'),', charge=',num2str(Zs'),...
    ', T (keV)=',num2str(Ts')];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot contour
[Nmesh,Bmesh]=meshgrid(nlog,Blog);

% contour We^2=wpe^2 <=> W2*B^2=wp2*n <=> log(B)=(log(n)+log(wp2/W2))/2
% wps2=318260.7346 *Zs^2(ns/10^20cc)(me/ms)*(Trad)^2
wp=sqrt(318261);
% Ws=17.5882 Zs(B/1MG)(me/ms)*(Trad)
W=17.5882;
% critical line
log10Bc=nlog/2+log10(wp/W);

for ib=1:nw
    figure
    [~,h]=contourf(Nmesh,Bmesh,w1-w2data(:,:,ib));
    hold on
    plot(nlog,log10Bc,'--w','Linewidth',2)
    set(h,'LineColor','none')
    c=colorbar;
    c.Label.String = '\omega_3 (Trad/s)';
    xlabel('log10(n/10^{20} cc)')
    ylabel('log10(B/MG)')
    title(str)
    axis([nlog10min nlog10max Blog10min Blog10max])
    set(gcf,'color','w')
    saveas(gcf,[fstem,'w3_',num2str(ib),'.png'])
    
    figure
    [~,h]=contourf(Nmesh,Bmesh,Mdata(:,:,ib));
    hold on
    plot(nlog,log10Bc,'--w','Linewidth',2)
    set(h,'LineColor','none')
    c=colorbar;
    c.Label.String = 'M';
    xlabel('log10(n/10^{20} cc)')
    ylabel('log10(B/MG)')
    title(str)
    axis([nlog10min nlog10max Blog10min Blog10max])
    set(gcf,'color','w')
    set(gcf,'Position',[500 500 800 400])
    saveas(gcf,[fstem,'M_',num2str(ib),'.png'])
    
    figure
    surf(Nmesh,Bmesh,b3data(:,:,ib));
    hold on
    plot(nlog,log10Bc,'--k','Linewidth',2)
    title('b3');
    xlabel('log10(n/10^{20} cc)')
    ylabel('log10(B/MG)')
    axis([nlog10min nlog10max Blog10min Blog10max])
    set(gcf,'color','w')
    saveas(gcf,[fstem,'b3_',num2str(ib),'.png'])
end


