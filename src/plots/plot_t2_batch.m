% This script run multiple plot_fig3.m
% Remember to comment parameters to avoid over writting

% resolve near 90 and 180
str1='/t89-91/';
str2='/t170-180/';
fbase='../data/fig3/B';
%Bs=[3,1];
%for i=1:2
    %B0=Bs(i);
    B0=0.3;
    for j=1:2    
        b1=1;
        b2=j;
        np=32;
        
        
        fstem=[fbase,num2str(B0),'/1-',num2str(j),str1];
        t2=linspace(89,91,np);
        disp([fstem,' started on ',datestr(now)])
        plot_fig3
        disp([fstem,' finished on ',datestr(now),char(10),char(10)])

        
        fstem=[fbase,num2str(B0),'/1-',num2str(j),str2];
        t2=linspace(170,180,np);
        disp([fstem,' started on ',datestr(now)])
        plot_fig3
        disp([fstem,' finished on ',datestr(now),char(10),char(10)])
    end
%end
