% This script plot backscattering
close all
%clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters to adjust
flag=1; % compute data and plot
% flag=2; % read data and plot

%f1=854.109; % THz <=> 0.351 um
f1=299.792; % THz <=> 1um
B0=81; % MG
%B0=2; % MG

% branch index
%b1=1;
%b2=1;

% bounds for resonance search, in unit of ck1r
ckmin=0.5;
ckmax=1;

% branches to plot
blist=[1,2,3,4];

%fstem='../data/';
np=64;
%np=16;

% angle index to plot dispersion
ip=ceil(np/2);
% branch index to plot arrows
ib=3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load setup
setup.b1=b1;
setup.b2=b2;

w1=2*pi*f1;
setup.w1=w1;

t=linspace(2,88,np);
setup.p2=180;

setup.ckmin=ckmin;
setup.ckmax=ckmax;

fname=[fstem,'data_BS.mat'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
para=Parameter(B0);
nw=para.nw; % number of branches

Ms=para.Ms; % mass of species
Zs=para.Zs;
Ts=para.Ts;
Ns=para.Ns;

str=['\omega_1=',num2str(w1),' Trad/s, B0=',num2str(B0),' MG',...
    ', b1=',num2str(b1),', b2=',num2str(b2),char(10),...
    'mass=',num2str(Ms'),', charge=',num2str(Zs'),...
    ', n(cc)=',num2str(Ns'),', T (keV)=',num2str(Ts')];


% compute exact data
%        map.w2   : frequency of scattered wave, in unit of Trad/s, nx-ny-nw
%        map.ck2r : wavevector of scattered wave, in unit of Trad/s, nx-ny-nw
%        map.ck1r : wavevector of pump, in unit of Trad/s, scalar
%        map.C    : coupling coefficient, in unit of (Trad/s)^2, nx-ny-nw
%        map.u123 : products of wave enrgy coefficients, nx-ny-nw
%        map.Sa   : summation abs C,in unit of (Trad/s)^2, nx-ny-nw
%        map.M    : growth rate, in unit of unmagnetized Raman, nx-ny-nw
%        map.b3   : branch index for the mediating wave, nx-ny-nw
if flag==1
    % allocate arrays
    ck1r=zeros(1,np);
    ck2r=zeros(nw,np);
    w2=zeros(nw,np);
    M=zeros(nw,np);
    b3=zeros(nw,np);
    u123=zeros(nw,np);
    Sa=zeros(nw,np);
    
    disp('computing map...')
    % compute all angles
    for i=1:np
        disp(['i/np=',num2str(i/np)])
        t1=t(i);
        t2=180-t1;
        setup.t1=t1;
        setup.t2=t2;
        map=main_map(setup,para,fstem,-1);
        
        ck2rtmp=squeeze(map.ck2r);
        w2tmp=squeeze(map.w2);
        Mtmp=squeeze(map.M);
        b3tmp=squeeze(map.b3);
        u123tmp=squeeze(map.u123);
        Satmp=squeeze(map.Sa);
        
        ck1r(i)=map.ck1r;
        nb=length(w2tmp);
        if nb>0
            ck2r(1:nb,i)=ck2rtmp;
            w2(1:nb,i)=w2tmp;
            M(1:nb,i)=Mtmp;
            b3(1:nb,i)=b3tmp;
            u123(1:nb,i)=u123tmp;
            Sa(1:nb,i)=Satmp;
        end
    end    
    % save data
    save(fname,'t','w1','w2','M','b3','u123','Sa','ck1r','ck2r')       
else
    % read data
    load(fname)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot frequency
figure
plot(t,w2(blist,:)/w1,'.-','Linewidth',2)
title(str)
xlabel('\theta')
ylabel('\omega_2/\omega_1')
set(gcf,'color','w')
saveas(gcf,[fstem,'w2.png'])

% plot growth rats
figure
plot(t,M(blist,:),'.-','Linewidth',2)
title(str)
xlabel('\theta')
ylabel('M')
set(gcf,'color','w')
saveas(gcf,[fstem,'M.png'])

% plot wave energy coefficients
figure
semilogy(t,u123(blist,:),'.-','Linewidth',2)
title(str)
xlabel('\theta')
ylabel('u1*u2*u3')
set(gcf,'color','w')
saveas(gcf,[fstem,'u123.png'])

% plot sum of abs Sa
figure
semilogy(t,Sa(blist,:),'.-','Linewidth',2)
title(str)
xlabel('\theta')
ylabel('Sa')
set(gcf,'color','w')
saveas(gcf,[fstem,'Sa.png'])

% plot branch indes
figure
plot(t,b3(blist,:),'.-','Linewidth',2)
title(str)
xlabel('\theta')
ylabel('b3')
set(gcf,'color','w')
saveas(gcf,[fstem,'b3.png'])


% plot dispersion relation
figure
ck=linspace(-2*w1,2*w1,100);
omega=ck_2_w_warm(ck,t(ip),para,0);
pi2=2*pi;
plot(ck/pi2,omega(3:6,:)/pi2,'Linewidth',2)
hold on
plot(ck/pi2,omega(1:2,:)/pi2,'k','Linewidth',2)
xlabel('c/\lambda (THz)')
ylabel('f (THz)')
set(gcf,'color','w')

% compute arrows
p1=[ck1r(ip) w1]/pi2;
ck2rb=ck2r(ib,ip);
w2b=w2(ib,ip);
p2=[-ck2rb w2b]/pi2;
p3=[ck1r(ip)+ck2rb w1-w2b]/pi2;
% plot auxilliary arrows
quiver(p2(1),p2(2),p3(1),p3(2),0,'Linestyle','--',...
    'Color',[1 1 1]/2,'Linewidth',2)
quiver(p3(1),p3(2),p2(1),p2(2),0,'Linestyle','--',...
    'Color',[1 1 1]/2,'Linewidth',2)
% plot arrows
quiver(0,0,p1(1),p1(2),0,'Color','k','Linewidth',2)
quiver(0,0,p2(1),p2(2),0,'Color','k','Linewidth',2)
quiver(0,0,p3(1),p3(2),0,'Color','k','Linewidth',2)
% axis
line([0 0], [0 w1/pi],'Color','k')
%xlabel('ck (Trad/s)')
%ylabel('\omega (Trad/s)')
set(gcf,'Color','w')
box off
saveas(gcf,[fstem,'wck.png'])
