% This script plot dispersion relation
%close all
%clear
cd .. % enter /src directory
addpath(genpath('./'))

% parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% physical parameters
B0=0.5;         % MG
Ns=1.0;         % 10^20 cc

% list of angles
tlist=[0,45,90];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters for plotting
% file path
fpath='../data';
fstem = sprintf('%s/disp_eH_N%.2fT0.5B%.2f',fpath,Ns,B0);     

% wave vector parameters 
kmax = 2000; % Trad/s
nk = 100;

% large figure size
%posL=[50,50,1000,600];
posL=[0,0,10,6];
% small figure size
%posS=[50,50,400,240];
posS=[0,0,4,2];

% plot zoomed figure
kzoomC = 200;  % near curoff
kzoomL = 1000; % gapless modes

wbandC = 100; % zoom near cutoff in [wp-wbandC, cp+wbandC]
%wbandL = 10; % zoom near gapless modes in [0,wbandL]

% Plot dispersion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
para=Parameter(B0,[Ns,Ns]);
ck=linspace(0,kmax,nk);

wp=para.wp;
wbandL = max(abs(para.Ws));

figure
nt=length(tlist);
for it=1:nt
    % unpack
    theta=tlist(it)
    % file name
    fname = sprintf('%s_t%d',fstem,theta);   
    fnameO = sprintf('%s_O.png',fname); 
    fnameC = sprintf('%s_C.png',fname); 
    fnameL = sprintf('%s_L.png',fname); 

    % compute and plot dispersion
    omega=ck_2_w_warm(ck,theta,para,1);

    % plot large figure
    %set(gcf,'Position',posL)
    set(gcf,'PaperUnits','inches','PaperPosition',posL)
    % save figure
    print('-djpeg', fnameO, '-r100')
    %saveas(gcf,fnameO)
    %exportgraphics(gcf,fnameO)

    % zoom near cutoff    
    %set(gcf,'Position',posS)
    set(gcf,'PaperUnits','inches','PaperPosition',posS)
    xlim([0,kzoomC])
    ylim([wp-wbandC,wp+wbandC])
    % save figure
    print('-djpeg', fnameC, '-r100')
    %saveas(gcf,fnameC)
    %exportgraphics(gcf,fnameC)

    % zoom for gapless modes    
    %set(gcf,'Position',posS)
    set(gcf,'PaperUnits','inches','PaperPosition',posS)
    xlim([0,kzoomL])
    ylim([0,wbandL])
    % save figure
    print('-djpeg', fnameL, '-r100')
    %saveas(gcf,fnameL)
    %exportgraphics(gcf,fnameL)
end
