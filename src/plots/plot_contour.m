% This script test contour plot

x=linspace(0,1,10);
y=linspace(-2,-1,5);

z=zeros(5,10);
[X,Y]=meshgrid(x,y);

for i=1:10
    for j=1:5
        z(j,i)=x(i)*y(j);
    end
end

[~,h]=contourf(X,Y,z);
set(h,'LineColor','none')
c=colorbar;
c.Label.String = 'xy';