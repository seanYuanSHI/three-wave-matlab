% This script plot main_map.m figures 
% assuming either 1D trace: plot as function of angle <ck1,ck2>
%              or 2D map  : plot as spherical projection
%                           for hammer plot -180<p<180
%                                              0<t<90
%
% Inputs:
%    map   : three-wave scattering data structure 
%    setup : data structure containing experimental setup info
%    flag  : flag=1 plot w1-w2
%            flag=2 plot ck2r/ck1r
%            flag=3 plot C/w1^2
%            flag=4 plot M
%            flag=5 plot b3
%            flag can be an array to plot multiple maps
%    blist : list of branches to plot
%
% Written by Yuan SHI, Feb 5, 2019

function plot_main_map(map,setup,flag,blist)

% setup para
w1=setup.w1;
t1=setup.t1;
b1=setup.b1;
b2=setup.b2;

t2=setup.t2;
p2=setup.p2;

% plot title
tstr=['\omega_1=',num2str(w1),' Trad/s, \theta_1=',num2str(t1),...
    ', (b1,b2)=(',num2str(b1), ',',num2str(b2),')'];

% dimensions
nm=length(flag);
w2=map.w2;
nx=size(w2,1);
ny=size(w2,2);
nw=size(w2,3);
na=nx*ny;

if nargin<4
    % default plot all branches
    blist=1:nw;
    nb=nw;
else
    nb=length(blist);
    if nb==0
        % default plot all branches
        blist=1:nw;
        nb=nw;
    end
end

% plot each map
for i=1:nm
    % unload data
    if flag(i)==1 % plot w1-w2
        data=map.w2-w1;
        ystr='\Delta\omega (Trad/s)';
    elseif flag(i)==2 % plot ck2r/ck1r
        ck1r=map.ck1r;
        data=map.ck2r/ck1r;
        ystr='k_2/k_1';
    elseif flag(i)==3 % plot C/w1^2
        data=map.C/w1^2;
        ystr='C/w_1^2';
    elseif flag(i)==4 % plot M
        data=map.M;
        ystr='M';
    else % plot b3
        data=map.b3;
        ystr='b3';
    end
    
    if (nx==1 || ny==1) && (na~=1) % plot 1D trace
        % remove singleton dimension
        data=squeeze(data);        
        
        % construct khat1
        [x,y,z]=sph2cart(0,(90-t1)/180*pi,1);
        khat1=[x;y;z];        
        % compute angle
        angle=zeros(na,1);
        for j=1:na
            % construct khat1
            [x,y,z]=sph2cart(p2(j)/180*pi,(90-t2(j))/180*pi,1);
            khat2=[x;y;z];
            ca=khat1'*khat2;
            angle(j)=acos(ca)/pi*180;
        end
        
        % plot for each mode
        figure
        for j=1:nb
            jj=blist(j);
            plot(angle,data(:,jj),'.-','linewidth',2.5)
            hold on
        end
        xlabel('\alpha')
        ylabel(ystr)
        title(tstr)
        set(gcf,'Color','w')
    end
    if nx>1 && ny>1
        % convert to longitude and lattitude
        lon=p2/180*pi;
        lat=(90-t2)/180*pi;        
        
        % plot each mode
        for j=1:nb
            figure     
            jj=blist(j);
            cmin=min(min(data(:,:,jj)));
            cmax=max(max(data(:,:,jj)));
            if cmin==cmax
                cmin=0.95*cmin;
                cmax=1.05*cmax;
            end
            
            Hammer_plot(lon,lat,data(:,:,jj),cmin,cmax,ystr)
            title(tstr)
        end
    end
end    
    