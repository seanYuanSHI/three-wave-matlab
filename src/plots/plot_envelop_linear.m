% This function plots transverse components of a EM wave envelop.
%
% When the wave is not an eigenmode, it will undergoes Faraday 
% rotation and ellipticity oscillation. This function 
% plots the slowly varying Ey and Ez envelopes,
% assuming the wave propagates in the x direction.
%
% The electric field of the wave can be written as
% E = E0*Re[A1*e1*exp(i*k1*x-i*w*t)+A2*e2(exp(ik2*x-iw*t)]
%   = E0*Re[exp(i*k*x-i*w*t)*(A1*e1*exp(i*pi*dn*L)+A2*e2*exp(-i*pi*dn*L))]
%   = E0*Re[exp(i*k*x-i*w*t)*Ehat]
% where k=(k1+k2)/2, dn=(ck1-ck2)/w, L=x/lambda0,  lambda0=2*pi*c/w
% Ehat is the Fourier envelop, which is spatially dependent, and
% presumably varies slowly compared to the fast wave phase exp(i*k*x-i*w*t).
% This function plots abs of Ehat components Ey and Ez
%
% Inputs:
%    w     : wave frequency, in Trad/S
%    theta : angle between wave vector and B0 in deg
%    para  : struct contains plasma parameters
%    Einit : initial E field [Ey;Ez], complex 2-by-1
%            when unspecified, assume linear polariztion
%            along y direction, namely, Ey=1, Ez=0.
%            The program will normalize, s.t. Einit'*Einit=1
%
% Written by Yuan SHI Nov 26, 2020

function plot_envelop_linear(w,theta,para,Einit)
    %%%%%%%%%%%%%%%%%%%%%
    % number of plot points
    nx = 1e3;
    % number of periods to plot
    np = 2;
    % number of representaitve phases per period
    nr = 8;
    %%%%%%%%%%%%%%%%%%%%

    % initial E field vector
    if nargin<4
        % defaul linear polarization along y
        Einit=[1;0];
    else
        % ensure normalization
        E0=sqrt(Einit'*Einit);
        Einit=Einit/E0;
    end

    % geometric factors
    % B0 frame (x,y,z)
    % B0 along z, wave vector k in xz plane
    t=theta/180*pi;
    st=sin(t);
    ct=cos(t);
    khat=[st;0;ct];
    % wave frame (x',y',z')
    % wave vector along x', B0 in x'z' plane, y' is y
    R = [st,0,ct;0,1,0;-ct,0,st];

    % wave vectors for the two EM modes
    ckr1 = wb_2_ck_warm(w,1,theta,para,0);
    ckr2 = wb_2_ck_warm(w,2,theta,para,0);
    % check solutions 1 exist
    if length(ckr1)==0
        disp('Mode 1: no wave with such w!')
        m1=0;
    else
        m1=1;
        % unit polarization vector in B0 frame
        [exyz1,~,~] = wck_2_e_warm(w,ckr1*khat,para,0);
        % rotate to wave frame 
        e1 = R*exyz1;
    end
    % check solutions 2 exist
    if length(ckr2)==0
        disp('Mode 2: no wave with such w!')
        m2=0;
    else
        m2=1;
        % unit polarization vector in B0 frame
        [exyz2,~,~] = wck_2_e_warm(w,ckr2*khat,para,0);
        % rotate to wave frame 
        e2 = R*exyz2;
    end

   
    % wave propagation
    if m1==1 && m2==1 
        % both modes exist, propagate as superposition
        % sum of refractive index
        sn = (ckr1+ckr2)/w;
        % difference in refractive index
        dn = (ckr1-ckr2)/w;
        % Spatial oscillation period in unit of vacuum wavelength
        L0=1/abs(dn);
     
        % coefficients for combining eigenmodes  
        % E=A1*e1+A2*e2
        ee = [e1(2),e2(2);e1(3),e2(3)];
        % ee*A = Einit
        A = linsolve(ee,Einit);
        % Fourier vector envelop of the E field
        Ehat = @(L)A(1)*e1*exp(1i*pi*dn*L)+A(2)*e2*exp(-1i*pi*dn*L);

        % report full Einit, including longitudinal component
        constants % load script
        l0=200*pi*c8/w; % lambda0 in um
        str='Wave propagates as superposition of eigenmodes';
        fprintf('%s\nPeriod x/lambda_0=%4.2f, x=%4.2f um\nInitial E=\n',str,L0,L0*l0)
        disp(Ehat(0))
        disp('Plotting figure')
     
        % prepare array for x axis, L=x/lambda0
        Lmax = np*L0; % plot np periods
        Lx = linspace(0,Lmax,nx);
        % complex envelope vector
        Ec = Ehat(Lx);
        % amplitude and phase
        [tx,Ex]=cart2pol(real(Ec(1,:)),imag(Ec(1,:)));     
        [ty,Ey]=cart2pol(real(Ec(2,:)),imag(Ec(2,:)));     
        [tz,Ez]=cart2pol(real(Ec(3,:)),imag(Ec(3,:)));     

        % plot figure
        clf
        % plot amplitudes
        subplot(2,3,[2,3])
        plot(Lx,Ex,'g')
        hold on
        plot(Lx,Ey,'r','LineWidth',3)
        plot(Lx,Ez,'b','Linewidth',2)
        plot(Lx,Ey-Ez,'k','LineWidth',1)
        plot(Lx,zeros(1,nx),'k:')
        % mark figure
        xlabel('x/lambda_0')
        ylabel('Amplitude (normalized)')
        legend('Ex','E_y','E_z','E_y-E_z','Location','eastoutside')
        %xlim([0,Lmax])
        AxisLessTight;

        % plot phase
        subplot(2,3,[5,6])
        plot(Lx,tx*180/pi,'g')
        hold on
        plot(Lx,ty*180/pi,'r','LineWidth',3)
        plot(Lx,tz*180/pi,'b','Linewidth',2)
        plot(Lx,(ty-tz)*180/pi,'k','LineWidth',1)
        plot(Lx,90*(-4:4)'*ones(1,nx),'k:')
        % mark figure
        xlabel('x/lambda_0')
        ylabel('Phase (deg)')
        legend('Ex','E_y','E_z','Ey-Ez','Location','eastoutside')
        %xlim([0,Lmax])
        AxisLessTight;
      
        % plot electric field trace at representative phases
        subplot(2,3,[1,4])
        % separation of representative phases
        Lp = L0/nr;
        % separation for one fast wave phase
        dL = 1/abs(sn);
        % plot each trace
        for i=0:np*nr
            % x axis
            xp = i*Lp;
            Lxp = linspace(xp+dL,xp-dL,nx);        
     
            % complex envelope vector
            Ecp = Ehat(Lxp);
            % fase wave phase
            phase = exp(1i*pi*sn*Lxp);
            % real electric field components, wave envelope * wave phase
            Eyr = real(Ecp(2,:).*phase);
            Ezr = real(Ecp(3,:).*phase);
            % color code each trace
            w = mod(i/nr,1); % weighting factor
            c = [0,1,1]*w + [1,0,1]*(1-w); % linear interpolate between r and b
            % adjust linewidth to mark each period
            if w==0
                l=3;
            else
                l=1;
            end 
            % plot figure
            plot3(Eyr,Ezr,Lxp,'Color',c,'LineWidth',l)
            %plot3(Lxp,Eyr,Ezr,'Color',c,'LineWidth',l)
            hold on
        end
        % mark figure
        xlabel('Ey')
        ylabel('Ez')
        zlabel('x/lambda_0')
        xlim([-1.5,1.5])
        ylim([-1.5,1.5])
        zlim([-dL,Lmax+dL])
        %xlim([-dL,Lmax+dL])
        %ylim([-1.5,1.5])
        %zlim([-1.5,1.5])
        grid on       
 
        set(gcf,'Color','w')
      

    elseif m1==0 && m2==1 
        % wave is only propagating in engenmode 2
        % report ful Einit, including longitudinal component
        fprintf('Wave propagates in mode 2\nUnit vector E=\n')
        disp(e2)
        disp('No figure is plotted')
    else
        disp('Unknown situation!')
    end

