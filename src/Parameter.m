% This function read list of species 
% and generate characteristic frequencies
% The default filename is 'species.txt'
%
% Ths program exclude species with zero density
%
% Input:
%    B0   : background magnetic field in unit of MG
%    Ns   : density of species in unit of 1e20 cc
%           whenever specified, overwirte input file
%    Ts   : temperature of species in unit of keV
%           whenever specified, overwirte input file
% Outputs:
%    para    : structure containing paramters
%      para.B0   : magnetic field
%      para.wps2 : species plasma frequency^2, column vector
%      para.Ws   : species gyro frequency, column vector
%      para.Zs   : species charge in unit of e
%      para.Ms   : species mass in unit of me
%      para.Ns   : species density in unit of 1e20 cc
%      para.Ts   : species temperature in unit of keV
%      para.us2  : species u_thermal^2/c^2, column vector, us2=ps*Ts/ms
%      para.ps   : species polytropic index
%                  p=1: isothermal
%                  p=3: adiabatic for mono-atomic gas
%
%      para.wp   : total plasma frequency
%      para.nn   : nn=1 if non-neutral
%                  nn=0 if neutral
%      para.nB   : nB=1 weakly magnetized
%      para.nw   : number of dispersion branches
%
%      para.Cs   : unmagnetized sound wave speed/c
%      para.Va   : Alfven speed/c
%      para.Ca   : Ca=Va/sqrt(1+Va^2)
%
%      para.wc   : cutoff frequencies
%      para.wa   : upper validity bound of low w asymptotics
%      para.ea   : smallness parameter for wa
%
%      para.lD   : Debye length in 1 um
%
% Frequencies are in unit of 2*pi*THz=Trad/s
%
% Written by Yuan SHI, Nov 10, 2017
% Last modified by Yuan Shi on June 12, 2022

function para=Parameter(B0, Ns, Ts)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plasma frequency^2: 
%    wps2=318260.7346 *Zs^2(ns/10^20cc)(me/ms)*(Trad)^2
wp0=318261;

% gyro frequency: 
%    Ws=17.5882 Zs(B/1MG)(me/ms)*(Trad)
W0=17.5882;

% thermal speed: 
%    us^2/c^2=1.956951198e-3 ps*(me/ms)*(Ts/1KeV)
u0=1.95695e-3;

% relative smallness parameter for asymptotics
ea=0.1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load input species file
filename='../species.txt';
delimiter=' ';
headerlines=1;
sp=importdata(filename,delimiter,headerlines);

% load species data
sd=sp.data;
[ns0,nc]=size(sd);
assert(nc==5,'File must be of the format: Z, M, N, T, p')
Z=sd(:,1); % charge in unit of e
M=sd(:,2); % mass in unit of me
N=sd(:,3); % density in unit of 10^20 cc
T=sd(:,4); % temperature in unit of KeV
p=sd(:,5); % polytropic index

% only positive density are valid
for i=1:ns0
    N(i)=max(N(i),0);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check there is plasma
c0=sum(abs(N));
assert(c0~=0,'No plasma! Density of some species should be nonzero!')

% Check scale separation
cs=wp0*c0/B0^2/W0^2;
nB=0;
if cs>1e4
    disp('Comparatively weak magnetic field may cause slow convergence.')
    disp('Consider using asymptotic dispersion relations instead!')
    disp('Assume unmag EM wave e//y in wave frame.')
    nB=1;
end

% Check quasi neutrality
cc=Z'*N;
nn=0;
if abs(cc)/c0>1e-6
    disp('The plasma is not quasi-neutral at 1 ppm level !')
    nn=1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate index list of non-zero density species
index=zeros(ns0,1);
ns=0;
for j=1:ns0
    if N(j)>0
        ns=ns+1;
        index(ns)=j;
    end
end
index=index(1:ns);
% check additional inputs
if nargin>1
    assert(length(Ns)==ns,...
        ['density overwrite must match original size! ns=',num2str(ns)])
end
if nargin>2
    assert(length(Ts)==ns,...
        ['temperature overwrite must match original size! ns=',num2str(ns)])
end

% Initialize outputs
wps2=zeros(ns,1);
Ws=zeros(ns,1);
us2=zeros(ns,1);
for j=1:ns
    jj=index(j);
    % overwrite density if specified
    if nargin>1
        N(jj)=Ns(j);
    end
    % overwrite temperature if specified
    if nargin>2
        T(jj)=Ts(j); 
    end
    
    % compute species specific plasma parameters
    wps2(j)=wp0*Z(jj)^2*N(jj)/M(jj);
    
    Ws(j)=W0*Z(jj)*B0/M(jj);
        
    us2(j)=u0*p(jj)*T(jj)/M(jj);
    if us2(j)>0.1
        warning(['Temperature relativistic for ',num2str(j),...
            '-th species in input list!'])
    end
end

% Compute total plasma frequency
wp2=sum(wps2);
wp=sqrt(wp2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mark plasma species with non-zero thermal speed
nt=0;
uindex=zeros(ns,1);
for j=1:ns
    if us2(j)~=0
        nt=nt+1;
        uindex(nt)=j;
    end
end
uindex=uindex(1:nt);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute derived parameters
% sound speed
Cs=sound(wps2,us2,uindex,0);
nc=length(Cs);

% Alfven speed
Va=Alfven(wps2,Ws);
Ca=Va/sqrt(1+Va^2);

% cutoff frequencies
wc=cutoff(wps2,Ws,0);

% wa and nw
if B0==0 % unmagnetized
    wa=ea*wp; % wp is the only characteristic frequency
    nw=nc+3;  % sounds + Langmuir + 2 EM
else
    wa=ea*min([wp;abs(Ws)]); % wp and Ws 
    nw=nc+3+ns;  % sounds + Langmuir + 2 EM + cyclotrons
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load inputs
para.B0=B0;
para.wps2=wps2;
para.Ws=Ws;
para.us2=us2;
para.Zs=Z(index);
para.Ms=M(index);
para.Ns=N(index);
para.ps=p(index);
para.Ts=T(index);
para.uindex=uindex;

para.wp=wp;
para.nn=nn;
para.nB=nB;
para.nw=nw;

para.Cs=Cs;
para.Va=Va;
para.Ca=Ca;

para.wc=wc;
para.wa=wa;
para.ea=ea;

% Debye length
[~,lD] = Debye(para);
para.lD = lD;

