% This program calculate three-wave scattering for multiple khat2
%
% Inputs:
%    setup : data structure containing experimental setup info
%      setup.w1    : frequency of pump wave, scalar, in unit of Trad/s
%      setup.b1,b2 : branch index, scalar, b=1 for highest-frequency branch
%      setup.t1    : polar angle of pump wave in degree, scalar
%                      azimuthal angle p1=0, due to cylindrical symmetry
%      setup.t2    : polar angle of scattered wave in degree, size nx-by-ny
%      setup.p2    : azimuthal angle of scattered wave in degree, size nx-by-ny
%      
%      ckmin/ckmax  : optional, in units of ck1r,
%                     when specified, search within range
%                     ckmin : minimum ck, length n-by-1
%                     ckmax : maximum ck, length n-by-1
%                     search for roots in intervals (ckmin,ckmax]

%    para  : structure containing plasma parameters
%    fstem : string, file path to write data
%    flag  : flag=0 save data in matlab format
%            flag=1 write netcdf format
%            flag=-1 do not save data
%
% Output:
%    map   : three-wave scattering data structure 
%        map.w2   : frequency of scattered wave, in unit of Trad/s, nx-ny-nw
%        map.ck2r : wavevector of scattered wave, in unit of Trad/s, nx-ny-nw
%        map.ck1r : wavevector of pump, in unit of Trad/s, scalar
%        map.C    : cmplex coupling coefficient, in unit of (Trad/s)^2, nx-ny-nw
%        map.u123 : products of wave enrgy coefficients, nx-ny-nw
%        map.Sa   : summation abs C,in unit of (Trad/s)^2, nx-ny-nw
%        map.Mc   : abs(Mc) is growth rate normalized by unmagnetized Raman, nx-ny-nw
%        map.b3   : branch index for the mediating wave, nx-ny-nw
%
% Written by Yuan SHI, Feb 4, 2019
% Last modified by Yuan SHI, Nov 3, 2020

function map=main_map(setup,para,fstem,flag)

debug=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% unload setup
w1=setup.w1;
b1=setup.b1;
b2=setup.b2;
t1=setup.t1;
t2=setup.t2;
p2=setup.p2;

% bounds for search
try
    ckmin=setup.ckmin;
    ckmax=setup.ckmax;
catch
    ckmin=[];
    ckmax=[];
end    

% check inputs
assert(length(w1)==1);
assert(length(b1)==1);
assert(length(b2)==1);
assert(length(t1)==1);

nx=size(t2,1);
ny=size(t2,2);
assert(size(p2,1)==nx);
assert(size(p2,2)==ny);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% determine ck1r and ck1
disp('determine ck1r and ck1...')
ck1r=wb_2_ck_warm(w1,b1,t1,para,0);
nk1=length(ck1r);

% bounds in unit of ck1r
%bounds.ckmin=ck1r*ckmin;
%bounds.ckmax=ck1r*ckmax;
bounds.ckmin=ckmin;
bounds.ckmax=ckmax;

% preload output
map.ck1r=ck1r;
map.w2=[];
map.ck2r=[];
map.C=[];
map.u123=[];
map.Sa=[];
map.Mc=[];
map.b3=[];

if nk1==0
    disp('No such pump wave exist!')
else    
    % prepare mapping
    disp(['Mapping warm started on ',datestr(now)])
    ii=0; % progress counter
    nn=nx*ny; % total number of map points
    prog=0.1; % fraction of completion
    
    % construct khat1
    theta1=t1/180*pi;
    st1=sin(theta1);
    ct1=cos(theta1);
    khat1=[st1;0;ct1];
    
    % pump wave vector
    ck1=ck1r*khat1;
    
    % initialize outputs
    nw=para.nw; % maximum number of modes
    w2map=zeros(nx,ny,nw);
    ck2rmap=zeros(nx,ny,nw);
    Cmap=zeros(nx,ny,nw);
    u123map=zeros(nx,ny,nw);
    Samap=zeros(nx,ny,nw);
    Mmap=zeros(nx,ny,nw);
    b3map=zeros(nx,ny,nw);
    
    % load values
    disp('Computing for each probe angle...')
    % Matlab is column major
    for j=1:ny
        for i=1:nx
            if debug==1
                fprintf('ix/nx=%d/%d, iy/ny=%d/%d\n',i,nx,j,ny)
            end
            % location on the map
            theta2=t2(i,j);
            phi2=p2(i,j);
            
            % construct khat2
            [x,y,z]=sph2cart(phi2/180*pi,(90-theta2)/180*pi,1);
            khat2=[x;y;z];
            
            % compute scattering
            % scatter=main(b1,w1,ck1,b2,khat2,para,debug,bounds)
            scatter=main(b1,w1,ck1,b2,khat2,para,debug,bounds);
            
            % load values
            w2=scatter.w2;
            nc=length(w2);
            w2map(i,j,1:nc)=w2;
            
            ck2rmap(i,j,1:nc)=scatter.ck2r;
            Cmap(i,j,1:nc)=scatter.C;
            u123map(i,j,1:nc)=scatter.u123;
            Samap(i,j,1:nc)=scatter.Sa;
            Mmap(i,j,1:nc)=scatter.Mc;
            b3map(i,j,1:nc)=scatter.b3;
            
            % report progress
            ii=ii+1;
            frac=ii/nn;
            if frac>=prog
                disp([num2str(frac*100),' % finished'])
                prog=frac+0.1;
            end
        end
    end
    
    % post load data structure
    map.w2=w2map;
    map.ck2r=ck2rmap;
    map.C=Cmap;
    map.u123=u123map;
    map.Sa=Samap;
    map.Mc=Mmap;
    map.b3=b3map;
    
    % write data to file
    if flag~=-1
        disp('Writing data to file...')
        write_data(map,setup,para,fstem,flag)
        disp(['Mapping finished on ',datestr(now)])
    end
end    
