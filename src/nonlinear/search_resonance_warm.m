% Given 1st wave and khat, this  program searches for w2, ck2 
% such that three-wave resonance conditions are satisfied:
%    fd(w2,ck2)=0             : 2nd wave on shell
%    fd(w1-w2,ck1-ck2)=0      : 3rd wave on shell
% where fd is the dispersion polynomial.
%
% The trivial root at ck2=0 is excluded whenever it occurs
% because this case corresponds to no three-wave interaction.
%
% Inputs:
%    w1     : frequency of wave 1 in 2*pi*THz, scalar
%    b2     : branch index of wave 2
%             b=1 : highest frequency branch
%    ck1    : wave vector of pump, 3-by-1
%    khat2  : unit wave vector of seed, 3-by-1
%    para   : structure containing input parameters
%    flag   : flag=1, plot figures, using zeros_continue.m
%
%    bounds  : optional, when specified, search within range
%             specified in unit of ck1r
%             bounds.ckmin : minimum ck, length n-by-1
%             bounds.ckmax : maximum ck, length n-by-1
%             search for roots in intervals (ckmin,ckmax]
%
% Output:
%    w2    : frequencies in 2*pi*THz, length nw
%            nw is number of roots found
%    ck2r  : length of wave vector >0, length nw
%
% Written by Yuan SHI, Feb 3, 2019

function [w2,ck2r]=search_resonance_warm(w1,ck1,b2,khat2,para,flag,bounds)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters
eN=2; % convergence criteria for asymptotic exponent
err=1e-12;  % convergence criteria for ck2r in unit ck1r
           % err too large cause large root error
           % err too small takes long computation time

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check input
assert(length(ck1)==3,'ck1 should be 3-by-1')
assert(length(khat2)==3,'khat2 should be 3-by-1')

if nargin<7
    bounds.ckmin=[];
    bounds.ckmax=[];
end
ckmin=bounds.ckmin;
ckmax=bounds.ckmax;
nr=length(ckmin);
assert(nr==length(ckmax))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% preparation

% khat1
[~,lat,ck1r]=cart2sph(ck1(1),ck1(2),ck1(3));
theta1=90-lat/pi*180; % polar angle

% khat2
[~,lat,ck2r]=cart2sph(khat2(1),khat2(2),khat2(3));
khat2=khat2/ck2r; % make sure khat2 is normalized
theta2=90-lat/pi*180; % polar angle

% input message
msg=[' w1=',num2str(w1),...
     ', b2=',num2str(b2),' ck1r=',num2str(ck1r),...
     ', theta1=',num2str(theta1),', theta2=',num2str(theta2)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Search ck2r 
% normalization
nw=para.nw;
nw2=2*nw;
ck1n=ck1r^nw2;

% construct search handel 
%fun=@(ck2r)fsearch_warm(ck2r,khat2,b2,w1,ck1,para,0);
fun=@(ck2r)fsearch_warm(ck2r*ck1r,khat2,b2,w1,ck1,para,0)/ck1n;

% determine search interval
if nr==0 % if range not given by input, search maximum range
    ckmin=0;    
    % identify upper bound where roots can exist
    %disp('finding interval bounds...')
    %[ckmax,~,~]=asympt_interval_refine(fun,ck1r,eN,0);
    [ckmax,~,~]=asympt_interval_refine(fun,1,eN,0); % normalized
end

% find roots on [0,kmax]
ek=err;
%ek=err*ck1r; % convergence criteria for root finding
%ek=max(ek,err);
%disp('finding roots...')
%[roots,t]=zeros_continue(fun,a,b,ex,flag,rejec,rate,nx,iter)
% -1 : reject left boundary root
% 2  : reject both boundaries root
%disp('zero_continue...')
[ktmp,~]=zeros_continue(fun,ckmin,ckmax,ek,flag,2); % normalized
nk=length(ktmp);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% outputs
%disp('outputing...')
if nk>0    
    %ck2r=ktmp;
    ck2r=ck1r*ktmp;
    % Compute w2 
    omega2=ck_2_w_warm(ck2r,theta2,para,0);
    w2=omega2(b2,:);
%    favg=mean(abs(fun(ck2r))); % numerical zero
else
    disp(['No root found!',msg]) 
    ck2r=[];
    w2=[];
%    favg=0;
end

% % check number of roots
% f0=fun(0);
% if abs(f0)>favg && f0*fmax>0 && mod(nk-t,2)==1 % even roots
%     warning(['Should have even number of nontrivial roots! t=',...
%         num2str(t)',msg])
% end
% if abs(f0)>favg &&f0*fmax<0 && mod(nk-t,2)==0 % odd roots
%     warning(['Should have odd number of nontrivial roots!t=',...
%         num2str(t)',msg])
% end
