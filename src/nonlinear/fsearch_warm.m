% This function is used to search 3-wave resonance
% This function search ck2r on given branch s.t. 3rd wave is on shell    
%
% Input:
%    ck2r  : norm of ck2, length nk2 
%    khat2 : unit vector along ck2, 3-by-1
%    b2    : branch index of seed. b2=1: highest frequency branch
%    w1    : frequency of the pump wave in 2*pi*THz, scalar
%    ck1   : wave vector of pump, 3-by-1
%    para  : structure containing input parameters
%    flag  : flag=1, plot figure
%
% Output:
%    fd    : value of dispersion polynomial for the 3rd wave
%
% Written by Yuan SHI, April 8, 2018

function fd=fsearch_warm(ck2r,khat2,b2,w1,ck1,para,flag)

% check inputs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
assert(length(w1)==1,'w1 should be scalar')
assert(length(ck1)==3,'ck1 should be 3-by-1')
assert(length(khat2)==3,'khat2 should be 3-by-1')

% wave vector
nk2=length(ck2r);
ck2=khat2*ck2r; % size 3-by-nk2


% compute w2 for given ck2r %%%%%%%%%%%%%%%%
% polar coordinate
[~,lat,~]=cart2sph(khat2(1),khat2(2),khat2(3));
theta2=90-lat/pi*180; % polar angle

% wave frequency
omega2=ck_2_w_warm(ck2r,theta2,para,0);
w2=omega2(b2,:); % row vector length nk2


% compute 3rd wave %%%%%%%%%%%%%%%%%%%%%%%%
w3=w1-w2; % length nk2
ck3=ck1*ones(1,nk2)-ck2; % size 3-by-nk2

% polar coordinate
[~,lat,ck3r]=cart2sph(ck3(1,:),ck3(2,:),ck3(3,:));
theta3=90-lat/pi*180; % polar angle, length nk2

% construct search fd %%%%%%%%%%%%%%%%%%%%%
% initialize
fd=zeros(1,nk2);
% load values
for i=1:nk2
    fd(i)=fck2w_warm(w3(i),ck3r(i),theta3(i),para,0);
end

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag==1
    plot(ck2r,fd,'.-')
    grid on
end