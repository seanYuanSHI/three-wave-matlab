% This function pack wave frequency and vector into data structure
%
% All waves have positive frequencies
% The pump wave always has the highest frequency
%
% Inputs:
%    b1    : branch index of first wave
%    w1    : frequency of 1st wave, positive
%    ck1   : wave vector of pump, 3-by-1
%    b2    : branch index of second wave
%    w2    : frequency of 2nd wave, positive
%    ck2   : wave vector of seed, 3-by-1
%    para  : structure containing input parameters
%    flag  : flag=1 report wave data
%
% Output:
%    wave  : structure containing wave w,ck,e,b for three waves
%       wave.wi : wave frequency, 1-by-1 real scalar
%       wave.cki: wave vector of i-th wave, 3-by-1 real vector
%       wave.ei : polarization vector of i-th wave, 3-by-1 complex vector
%       wave.bi : branch index of i-th wave, scalar
%
% Written by Yuan SHI on Feb 4, 2019

function wave=wave_data_warm(b1,w1,ck1,b2,w2,ck2,para,flag)

if nargin<6
    flag=0; % default no report
end

% confirm inputs
w1=abs(w1);
w2=abs(w2);
% ensure ck is read
ck1=real(ck1);
ck2=real(ck2);

% compute polarization of wave 1 and 2
[ehat1,~,~]=wck_2_e_warm(w1,ck1,para,0);
[ehat2,~,~]=wck_2_e_warm(w2,ck2,para,0);

% compute branch index of wave 1 and 2
%b1=wck_2_b_warm(w1,ck1,para);
%b2=wck_2_b_warm(w2,ck2,para);

% load wave 1 and 2
if w1>w2 % 1 is pump
    % load pump
    wave.w1=w1;
    wave.ck1=ck1;
    wave.b1=b1;
    wave.e1=ehat1;
    
    % load scatter
    wave.w2=w2;
    wave.ck2=ck2;
    wave.b2=b2;
    wave.e2=ehat2;
    
    ck3=ck1-ck2;
    
else % 2 is pump
    % load pump
    wave.w1=w2;
    wave.ck1=ck2;
    wave.b1=b2;
    wave.e1=ehat2;
    
    % load scatter
    wave.w2=w1;
    wave.ck2=ck1;
    wave.b2=b1;
    wave.e2=ehat1;
    
    ck3=ck2-ck1;
    
end

% load 3rd wave, pump wave always has higher frequency 
w3=abs(w1-w2);

wave.w3=w3;
wave.ck3=ck3;
wave.b3=wck_2_b_warm(w3,ck3,para);
[ehat3,~,~]=wck_2_e_warm(w3,ck3,para,0);
wave.e3=ehat3;

% report wave data %%%%%%%%%%%%%%%%%%%%%%%%%
if flag==1
    disp(['w1=',num2str(wave.w1),', b1=',num2str(wave.b1),...
        ', ck1=',num2str(wave.ck1'),', e1=',num2str(wave.e1')])
    disp(['w2=',num2str(wave.w2),', b2=',num2str(wave.b2),...
        ', ck2=',num2str(wave.ck2'),', e2=',num2str(wave.e2')])
    disp(['w3=',num2str(wave.w3),', b3=',num2str(wave.b3),...
        ', ck3=',num2str(wave.ck3'),', e3=',num2str(wave.e3')])
end
    

