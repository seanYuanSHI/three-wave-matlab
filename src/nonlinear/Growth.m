% This function compute the normalized growth rate 
% using coupling coefficient with kinematic factors
%
% Inputs:
%    wave  : structure containing wave w,ck,e for three waves
%       wave.wi : wave frequency, 1-by-1 real scalar
%       wave.cki: wave vector of i-th wave, 3-by-1 real vector
%       wave.ei : polarization vector of i-th wave, 3-by-1 complex vector
%    para  : structure containing input parameters
%    C     : coupling coefficient, complex
%
% Outputs:
%    Mc    : abs(Mc) is the normalized growth rate
%
% Written by Yuan SHI on Nov 12, 2017
% Last modified by Yuan SHI on Nov 3, 2020

function Mc=Growth(wave,para,C)

if nargin<3
    % load coupling coefficient
    C=Couple_warm(wave,para);
end

% compute plasma frequency
wp=para.wp;

% compute normalized growth rate
w1=wave.w1;assert(w1>0);
w2=wave.w2;assert(w2>0);
w3=wave.w3;assert(w2>0);

%M=2*abs(C)/sqrt(w1*w2*w3*wp);
Mc=2*C/sqrt(w1*w2*w3*wp);
