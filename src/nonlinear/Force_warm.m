% This function compute a list of forcing operator for given w and ck 
% The magnetic field is in the z-direction
% The operator is represented by 3-by-3 matrix in (x,y,z) coordinate 
%
% Input:
%    w     : frequency of the wave in 2*pi*THz
%    ck    : wave vector in unit of 2*pi*THz, 3-by-1 real vector
%    para  : structure containing input parameters
% Output:
%    Fs(1:3,1:3,s): forcing operator for species s 
%
% Written by Yuan SHI, Nov 11, 2017
% Modified to include thermal effects by Yuan SHI on Mar 29, 2018

function Fs=Force_warm(w,ck,para)

% check length
assert(length(ck)==3,'vector ck should be 3-by-1')

% convert ck to sperical coordinate
[phi,ele,r]=cart2sph(ck(1),ck(2),ck(3));

% polar angle theta=pi/2-ele;
st=cos(ele);
ct=sin(ele);
st2=st^2;
ct2=ct^2;

% azimuthal angle
sp=sin(phi);
cp=cos(phi);
% rotation (x,y,z)=(x',y',z')*R
%       (x',y',z')=(x,y,z)*iR
%               ck=(x',y',z')*(kperp;0;kpara)
R=[cp,sp,0;-sp,cp,0;0,0,1];  % rotate
iR=[cp,-sp,0;sp,cp,0;0,0,1]; % inverse

% square of frequency
w2=w^2;
% square of wave vector
ck2=r^2;
% square of refractive index
n2=ck2/w2;

% load plasma parameters
Ws=para.Ws;
us2=para.us2;

% Initialize output
ns=length(Ws);
Fs=zeros(3,3,ns);

% load matrix
for j=1:ns
    % magnetic factors
    b=Ws(j)/w;
    b2=b^2;
    r2=1/(1-b2);
    ibr2=1i*b*r2;
    
    % thermal factors
    rho2=us2(j)*n2/(1-us2(j)*n2*r2*(1-b2*ct2));
    rrs=r2*rho2*st2;
    rsc=rho2*st*ct;
    
    % load matrix in (kperp,0,kpara) coordinate
    f=[ r2*(1+rrs),   ibr2*(1+rrs),   r2*rsc;
      -ibr2*(1+rrs),  r2*(1+b2*rrs), -ibr2*rsc;
        r2*rsc,         ibr2*rsc,    1+rho2*ct2];
    
    % rotate into (x,y,z) frame
    Fs(:,:,j)=iR*f*R;
end