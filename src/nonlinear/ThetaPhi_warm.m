% This function compute total EM and fluid scattering 
%
% Inputs:
%    wave  : structure containing wave w,ck,e for three waves
%       wave.wi : wave frequency, 1-by-1 real scalar
%       wave.cki: wave vector of i-th wave, 3-by-1 real vector
%       wave.ei : polarization vector of i-th wave, 3-by-1 complex vector
%    para  : structure containing input parameters
%
% Outputs:
%    TP(ns) : list of scattering strengths Theta+Phi
%             ns is index of species
%
% Merge Theta and Phi by Yuan SHI on Feb 3, 2019

function TP=ThetaPhi_warm(wave,para)

% unpack frequencies
w1=wave.w1; assert(w1>0);
w2=wave.w2; assert(w2>0);
w3=wave.w3; assert(w3>0);

iw1=1/w1;
iw2b=-1/w2;
iw3b=-1/w3;

% unpack wave vectors
ck1=wave.ck1;
ck2=wave.ck2;
ck3=wave.ck3;

ck2b=-ck2;
ck3b=-ck3;

% unpack polarizations
e1=wave.e1;
e2=wave.e2;
e3=wave.e3;

e2b=conj(e2);
e3b=conj(e3);

% forcing operators
Fs1=Force_warm(w1,ck1,para);
Fs2=Force_warm(w2,ck2,para);
Fs3=Force_warm(w3,ck3,para);

% thermal parameters
us2=para.us2; % thermal speed us^2/c^2
ps=para.ps;   % polytropic index

% species dependent common thermal factor
uw=-us2*iw1*iw2b*iw3b;

% compute for each species
ns=length(us2);
TP=zeros(ns,1);

for is=1:ns
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % forcing operators
    F1=Fs1(:,:,is);
    F2=Fs2(:,:,is);
    F3=Fs3(:,:,is);
    
    % F*e vectors
    f1=F1*e1;
    f2=F2*e2;
    f3=F3*e3;
    
    f2b=conj(f2);
    f3b=conj(f3);
    
    % a.' is transpose of a, without conjugation
    % EM scattering strength %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %ST123=(ck1'*f2b)*(e1.'*f3b)/w2b;
    k1f2=ck1.'*f2b;
    e1f3=e1.'*f3b;
    ST123=k1f2*e1f3*iw2b;    
    %ST132=(ck1'*f3b)*(e1.'*f2b)/w3b;
    k1f3=ck1.'*f3b;
    e1f2=e1.'*f2b;
    ST132=k1f3*e1f2*iw3b;
    
    %ST231=(ck2b'*f3b)*(e2b.'*f1)/w3b;
    k2f3=ck2b.'*f3b;
    e2f1=e2b.'*f1;
    ST231=k2f3*e2f1*iw3b;
    %ST213=(ck2b'*f1)*(e2b.'*f3b)/w1;
    k2f1=ck2b.'*f1;
    e2f3=e2b.'*f3b;
    ST213=k2f1*e2f3*iw1;
    
    %ST312(is)=(ck3b'*f1)*(e3b.'*f2b)/w1;
    k3f1=ck3b.'*f1;
    e3f2=e3b.'*f2b;
    ST312=k3f1*e3f2*iw1;    
    %ST321(is)=(ck3b'*f2b)*(e3b.'*f1)/w2b;
    k3f2=ck3b.'*f2b;
    e3f1=e3b.'*f1;
    ST321=k3f2*e3f1*iw2b;
    
    % total EM scattering
    T=ST123+ST132+ST231+ST213+ST312+ST321;
    
    % Fluid scattering strength %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % k1f1=ck1.'*f1
    k1f1=ck1.'*f1;
    % k2f2=ck2b.'*f2b
    k2f2=ck2b.'*f2b;
    % k3f3=ck3b.'*f3b
    k3f3=ck3b.'*f3b;
    
    % PH0=(ps(is)-2)*(ck1.'*f1)*(ck2b.'*f2b)*(ck3b.'*f3b)
    PH0=(ps(is)-2)*k1f1*k2f2*k3f3;
    
    % PH1=(ck1.'*f1)*(ck1.'*f2b)*(ck1.'*f3b)
    PH1=k1f1*k1f2*k1f3;
    
    % PH2=(ck2b.'*f1)*(ck2b.'*f2b)*(ck2b.'*f3b)
    PH2=k2f1*k2f2*k2f3;
    
    % PH3=(ck3b.'*f1)*(ck3b.'*f2b)*(ck3b.'*f3b)
    PH3=k3f1*k3f2*k3f3;
    
    % total fluid scattering
    P=PH0+PH1+PH2+PH3;
    
    % EM + Fluid Scattering %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    TP(is)=T+uw(is)*P;
    
end    

