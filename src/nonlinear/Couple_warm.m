% This function compute coupling coefficient 
%
% Inputs:
%    wave  : structure containing wave w,ck,e for three waves
%       wave.wi : wave frequency, 1-by-1 real scalar
%       wave.cki: wave vector of i-th wave, 3-by-1 real vector
%       wave.ei : polarization vector of i-th wave, 3-by-1 complex vector
%    para  : structure containing input parameters
%
% Outputs:
%    C     : coupling coefficient, complex
%    u123  : products of wave energy coefficients
%    Sa    : summation of abs values
%
% Written by Yuan SHI on Nov 12, 2017
% Modified to include thermal effects by Yuan SHI on Mar 29, 2018
% Last modified by Yuan SHI on Nov 3, 2020

function [C,u123,Sa]=Couple_warm(wave,para)

% smallness parameter for checking input
ee=1e-6;

% Check normalization
e1=wave.e1;
e12=e1'*e1;
if abs(e12-1)>ee
    warning(['Normalized e1^2=',num2str(e12)])
end

e2=wave.e2;
e22=e2'*e2;
if abs(e22-1)>ee
    warning(['Normalized e2^2=',num2str(e22)])
end

e3=wave.e3;
e32=e3'*e3;
if abs(e32-1)>ee
    warning(['Normalized e3^2=',num2str(e32)])
end

% compute wave energy coefficients
w1=wave.w1;assert(w1>0);
ck1=wave.ck1;
H=Energy_warm(w1,ck1,para);
u1=e1'*H*e1;
if abs(u1-real(u1))>ee*abs(u1)
    warning(['Complex u1=',num2str(u1)]) 
end

w2=wave.w2;assert(w1>0);
ck2=wave.ck2;
H=Energy_warm(w2,ck2,para);
u2=e2'*H*e2;
if abs(u2-real(u2))>ee*abs(u2)
    warning(['Complex u2=',num2str(u2)]) 
end

w3=wave.w3;assert(w1>0);
ck3=wave.ck3;
H=Energy_warm(w3,ck3,para);
u3=e3'*H*e3;
if abs(u3-real(u3))>ee*abs(u3)
    warning(['Complex u3=',num2str(u3)]) 
end

%u123=4*sqrt(u1*u2*u3);
u123=u1*u2*u3;
% ensure real
u123=abs(u123);

% load parameters
wps2=para.wps2;
Zs=para.Zs;
Ms=para.Ms;
ns=length(Ms);

% load scattering strength
TP=ThetaPhi_warm(wave,para);

% compute coupling coefficient
C=0;
Sa=0;
for j=1:ns
    dC=Zs(j)*wps2(j)*TP(j)/Ms(j);
    C=C+dC;
    Sa=Sa+abs(dC);
end
C=C/4/sqrt(u123); % complex coupling
