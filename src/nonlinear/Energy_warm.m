% This function compute the wave energy operator
% Inputs:
%    w     : frequency of the wave in 2*pi*THz, scalar
%    ck    : wave vector in unit of 2*pi*THz, 3-by-1 real vector
%    para  : structure containing input parameters
% Output:
%    H2  : wave energy operator H/2, 3-by-3 matrix
%
% Written by Yuan SHI, Nov 11, 2017
% Modified to include thermal effects by Yuan SHI on Mar 29, 2018

function H2=Energy_warm(w,ck,para)

% check dimensions
assert(length(w)==1,'w should be scalar')
assert(size(ck,1)==3,'ck should be 3-by-1')
assert(size(ck,2)==1,'ck should be 3-by-1')

% inverse frequncy squared
iw2=1/w^2;

% list of species plasma parameters
wps2=para.wps2;
us2=para.us2;
ns=length(wps2);

% forcing operators
Fs=Force_warm(w,ck,para);

% construct energy operator
H2=eye(3); % identity matrix
% sum over species
for j=1:ns
    % explicitly cold terms
    f1=Fs(:,:,j);
    f2=f1*f1;
    
    % explicitly warm terms
    fk=f1*ck; % 3-by-1
    fkkf=fk*fk'; % 3-by-3 outer product, fk' is Hermitian transpose
    ft=us2(j)*iw2*fkkf;
    
    % total contribution by this species
    p=wps2(j)*iw2/2;
    H2=H2-p*(f1-f2-ft);
end

