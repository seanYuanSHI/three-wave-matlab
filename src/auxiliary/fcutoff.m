% This function is used in cutoff.m for root finding
%     fcutoff(w)=\sum_s wps2/(w-Ws)-w
% Input:
%    w     : frequency in unit of 2*pi*THz
%    wps2  : list of plasma frequency squared, length ns-by-1
%    Ws    : list of gyro frequencies, length ns-by-1
% Output:
%    fc    : value of fcutoff
%
% Written by Yuan SHI, Nov 13, 2017

function fc=fcutoff(w,wps2,Ws)

% initialize output
ns=length(Ws);

% pole removing polynomial
tau=ones(size(w));
for i=1:ns
    pole=w-Ws(i);
    tau=tau.*pole;
end

% sum branches
fc=w.*tau;
for i=1:ns
    ipole=ones(size(w));
    for j=1:ns
        if j~=i
            ipole=ipole.*(w-Ws(j));
        end
    end
    fc=fc-wps2(i)*ipole;
end