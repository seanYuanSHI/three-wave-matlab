% This function compute Alfven speed Va, where
%     1/Va^2=sun_s m_s*n_s/B0^2
%
% Inputs:
%    wps2  : list of plasma frequency squared, length ns-by-1
%    Ws    : list of gyro frequencies, length ns-by-1
% Output:
%    Va    : Alfven speed normalized by c
%
% Written by Yuan SHI, Mar 27, 2018

function Va=Alfven(wps2,Ws)

mag=Ws(1);
if mag==0 % unmagnetized
    Va=0;
else % magnetized
    %wps2=para.wps2;
    %Ws=para.Ws;
    iWs2=1./(Ws.^2);
    % inverse Alfven speed is additive in species
    iVa2=wps2'*iWs2;
    % Alfven speed VA^2/c^2, normalized by c^2
    Va=1/sqrt(iVa2);
end
