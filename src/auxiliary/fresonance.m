% This function is used in resonance.m for root finding
% by constructing the polynomial equation f(x)=0
%
% Denoting 
%     x=w^2/wp2, as=wps2*sin(theta)^2/wps, bs=Ws^2/wp2
% the polynomial is
%    f(x) = \prod_s(x-bs)*(x-ct2-x*\sum_s as/(x-bs)),
% where ct2=cos(theta)^2. For a plasma of ns species, thie polynomial
% is of degree ns+1 with leading coefficient 1.
%
% Input:
%    x     : frequency normalized by wp, scalar
%    theta : azimuthal angle in degree
%    para  : structure containing input parameters
% Output:
%    f     : value of polynomial, scalar
%
% Written by Yuan SHI, Nov 25, 2020

function f=fresonance(x,theta,para)

% conver angle
t=theta/180*pi;
st2=sin(t)^2;
ct2=1-st2;

% plasma frequency
wp=para.wp;
wp2=wp^2;
% coefficients of the polynomial
as = para.wps2/wp2*st2;
bs = para.Ws.^2/wp2;

% initialize output
d=1;
ns=length(para.Ws);
ds=ones(ns,1);
% compute each term
for i=1:ns
    % common denominator
    d = d*(x-bs(i));
    % magnetic terms
    for j=1:ns
        if j==i
            ds(i)=ds(i)*as(i);
        else
            ds(i)=ds(i)*(x-bs(j));
        end
    end
end

% sum all terms
f = d*(x-ct2)-x*sum(ds);
%fd=f/d;
