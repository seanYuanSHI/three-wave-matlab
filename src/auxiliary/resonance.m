% This program finds resonance frequencies in cold fluid plasma
% Using Stix dispersion fucntions, the resonances are given by A=0
% In cold-fluid plasma, 
%     A = S*sin(theta)^2+P*cos(theta)^2
% Multiplying w^2 on both sides, we need to solve
%     0 = w^2-wp2*cos(theta)^2-sum_s wps2*sin(theta)^2*w^2/(w^2-Ws^2)
%
% A numerically robust way of solving this equation is to convert it to 
% a polynomial equation by removing all singularities. Denoting 
%     x=w^2/wp2, as=wps2*sin(theta)^2/wps, bs=Ws^2/wp2
% the polynomial equation can be written as
%    0 = f(x) = \prod_s(x-bs)*(x-ct2-x*\sum_s as/(x-bs)),
% where ct2=cos(theta)^2. For a plasma of Ns species, thie polynomial
% is of degree Ns+1 with leading coefficient 1. There are Ns+1 positive roots.
%
% The polynomial is constructed by fresonance.m
% This is a wrapper function that uses Weierstrass iteration to solve f(x)=0
%
% Input:
%    theta : azimuthal angle in degree
%    para  : structure containing input parameters
% Output:
%    wr    : list of positive resonances, 1-by-(ns+1)
%            sorted in ascending order
%
% Written by Yuan SHI, Nov 25, 2020

function wr=resonance(theta,para)
    %%%%%%%%%%%%%%%%%%%%%%
    % Parameters for Weierstrass iteration
    % maximum number of iterations
    imax=1e3;
    % absolute smallness parameter
    eabs=1e-9;
    % relative smallness parameter
    erel=1e-6;
    %%%%%%%%%%%%%%%%%%%%%%%

    % initialize function handel for root finding
    fun=@(x)fresonance(x,theta,para);

    % initial guess use roots at theta=0
    x = [1;(para.Ws/para.wp).^2];

    % use Weierstrass iteration to refine roots
    x = poly_roots_Weierstrass(fun,x,imax,eabs,erel);

    % normalized roots
    wr = para.wp*sqrt(x);
    % order positive frequencies
    wr=sort(abs(wr));

    
