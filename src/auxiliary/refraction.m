% This program calculates refracted wave vector
% when a vacuum EM wave incident on a magnetized plasma.
%
% The plasma is uniformly magnetized with B0 along +z.
% There is no plasma flow in any direction, so w is unchanged.
% The plasma fills the x>0 half space, 
% while the x<0 half space is vacuum.
% 
% Conservation of momentum parallel to the x=0 interface require
% ky and kz unchanged. The dispersion relation then constrains kx.
%
% Inputs: 
%    cki  : incident wave vector, 3-by-1, in Trad/s
%           requires k component>0
%    bt   : wave branch index for transmitted beam
%           b=1 for highest frequency branch
%    para : structure containing plasma parameters
%    flag : if flag=1 display progress
%           if flag=-1, use cold magnetized dispersion
% Output:
%    ckt   : transmitted wave vector, 3-by-1, in Trad/s
%
% Written by Yuan SHI, Mar 9, 2022

function ckt = refraction(cki, bt, para, flag)

    % default input
    if nargin<4
        flag=0; % use warm dispersion, no display
    end
    % check cki
    assert(length(cki)==3,'cki should be 3-by-1!')
    assert(cki(1)>=0,'cki should have kx>=0!')
    % ensure bt is integer
    bt = round(bt);
    % conversion factor from degree to rad
    d2r = pi/180;

    %%%%%%%%%%%%
    % estimate using cold magnetized plasma
    % frequency of vacuum EM wave
    w = norm(cki);
    % Stix's dielectric functions
    [S,D,P]=Stix(w,para);
    R=S+D;
    L=S-D;

    % incidence angle in wave frame
    ci = cki(3)/w;
    ci2 = ci^2;
    % Coefficients of the quardratic equation for n2, n=ck/w
    % S*n2^2-B*n2+C=0
    B=R*L+P*S+(S-P)*ci2;
    C=P*R*L+(R*L-P*S)*ci2;

    % determinant of quadratic equation for n2
    F2=B^2-4*S*C;
    F=sqrt(F2);

    % when n is odd, ck is smaller, so n2 is the smaller root
    % when n is even, ck is larger, so n2 is the larger root
    n2 = (B+(-1)^bt*F)/(2*S);

    % Initial guess using cold plasma
    ct = ci/sqrt(n2);
    tt = acos(ct)/d2r; % in degree
    if flag>0
        display(['Initial guess using cold plasma theta=',num2str(tt)])
    end

    %%%%%%%%%%%% 
    % refined result using warm dispersion 
    % define objective function for root funding
    % theta in degree
    function fd = frefraction(theta)
        % wave frequency, 0: no plot
        ckr = wb_2_ck_warm(w,bt,theta,para,0);
        % difference from ckz
        fd = ckr*cos(theta*d2r)-cki(3);
    end

    if flag>=0      
        % search for zero of objective function
        fun = @(t)frefraction(t);
        % options
        if flag>0
            options = optimset('Display','iter'); % show iteration
        else
            options = optimset();
        end
     
        % search for root
        try
            tt = fzero(fun,tt,options);
        catch
            warning('fzero fail to find root. Use cold refraction.')
        end
    end
    if flag>0
        display(['Final root using warm plasma theta=',num2str(tt)])
    end

    %%%%%%%%%%%%
    % compute ckr in warm magnetized plasma
    ckr = wb_2_ck_warm(w,bt,tt,para,0);
    % compute ckx component of ckt
    % default is total internal reflection
    ckx = -cki(1);
    if length(ckr)>0
        ckx2 = (ckr*sin(tt*d2r))^2-cki(2)^2;
        if ckx2>0 
            ckx = sqrt(ckx2);
        end
    end
    % load output, ensure real        
    % y and z components are the same as incident
    ckt = real([ckx;cki(2);cki(3)]);
    
end
