% This function pad or truncate a list
%
% Input:
%    in   : input row vector of length ni
%    no   : length of output vector
%    p    : values to pad if in is not long enough
%    s    : direction, if s=2  truncate or pad tail
%                         s=1  truncate tail or pad head
%                         s=-2 truncate head or pad tail
%                         s=-1 truncate or pad head
% Output: 
%    out  : input row vector of length no
% 
% Written by Yuan SHI, April 2, 2018

function out=pad_trunc(in,no,p,s)

% check padding methods
assert(s==2 || s==1 || s==-1 || s==-2,'Options are s=-2,-1,1,2')

% check input is a row vector
assert(size(in,1)==1,'Row vector needed!')
% size of row vector
ni=size(in,2);

% initialize output
out=p*ones(1,no);

% index for in
if s<0 % trunc head
    il=max(1,ni-no+1);
    ir=ni;
else   % trunc tail
    il=1;
    ir=min(ni,no);
end

% index for out
if abs(s)==2 % pad tail
    ol=1;
    or=min(ni,no);
else         % pad head
    ol=max(no-ni+1,1);
    or=no;
end
    
out(ol:or)=in(il:ir);
