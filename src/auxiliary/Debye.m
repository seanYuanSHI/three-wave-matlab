% This function compute the Debye frequency  and length
%
% The Debye frequency is defined as wD=c*kD 
% where kD=2*pi/\lambda_D is the debye wave vector
% 1/\lambda_D^2=\sum_s es^2*ns/\epsilon_0/Ts
%
% Inputs:
%    para : data structure that contains the following
%       Ns  : density of species in unit of 10^20 cc, length ns
%       Zs  : charge of species in unit e, length ns
%       Ts  : temperature of species in unit of keV, length ns
% Outputs:
%    ckD  : Debye frequency in unit of 10^12 rad/s
%    lD   : Debye length, in unit of 1 um
%
% written by Yuan Shi, Feb 7, 2019
% Modified by Yuan Shi, Feb 19, 2021

function [ckD,lD]=Debye(para)

% unload para
Ns=para.Ns;
Zs=para.Zs;
Ts=para.Ts;

% load constants
constants

% (c*kD)^2=ckD216*(10^16 rad/s)^2
% when n0=10^20 cc, T=1 keV 

% sum over species
ckD2=0;
ns=length(Ns);
for i=1:ns
    ckD2=ckD2+ckD216*Zs(i)^2*Ns(i)/Ts(i);
end

ckD=1e4*sqrt(ckD2); % in 10^12 rad/s

% lD=2*pi*c/c*kD
% speed of light c=c8*10^8 m/s
lD=1e2*c8/ckD; % in um

