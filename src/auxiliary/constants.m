% This script store physical constants and calculate baswic parameters
% consistent with EPOCH
% cflag=1: display data
cflag=0; % default no display

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% electron charge in 10^(-19) C
%e19=1.60217662;
e19=1.602176565;

% electron mass in 10^(-30) kg
%m30=0.910938356;
m30=0.910938291;

% vacuum permittivity in 10^(-12) F/m
ep12=8.854187817;

% Boltzmann constant in 10^(-23) m^2kg/s^2/K
kB23=1.38064852;

% Speed of light in 10^8 m/s
c8=2.99792458;

% atomic mass in 10^(-30) kg
au30=1660.539040;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% electron plasma frequency^2 in unit of [10^(12) rad/s]^2
% when ne0=10^20 cc
wpe2=1e6*e19^2/ep12/m30;

% electron gyro frequency in unit of 10^(12) rad/s
% when B0=1 MG
We=1e1*e19/m30;

% electron thermal speed u^2/c^2 
% when T=1 keV and polytropic index=1
ue2=1e-2*e19/m30/c8^2;

% electron Debye frequency c^2/lambda_D^2 in (10^16 rad/2)^2
% when n0=10^20 cc, T=1 keV 
ckD216=e19*c8^2/ep12;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if cflag==1
    disp(['wpe2=',num2str(wpe2),' (Trad/s)^2, wpe=',...
        num2str(sqrt(wpe2)),' Trad/s'])
    disp(['|We|=',num2str(We),' Trad/s'])
    disp(['(ue/c)^2=',num2str(ue2)])
    disp(['(ckD)^2=',num2str(ckD216),' (10^16 rad/s)^2'])
end
