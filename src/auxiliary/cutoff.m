% This program find the list of cutoff frequencies
% Using Stix dispersion fucntions, the cutoffs are given by 0=C=PRL
% Cutoff is unaffected by thermal effects.
%
% The branch P=0 gives w^2=wp^2, where wp is the plasma frequency. The
% positive cutoff frequency is thus w=wp
%
% The branches R=0 and L=0 gives other cutoffs. Since R(w)=L(-w), 
% we can find all positive cutoffs by searching for zeros of L only.
% Solving L=0 is equivalent to solve w=\sum_s wps2/(w-Ws)
%     Notice the RHS has singularities near w=Ws, near which a solution
%     can always be found.
%
%     In addition to a solution near each sigularity, there is an 
%     additional solution near w~0
%     
%     Therefore, there are ns+1 solutions
%
% Input:
%    wps2  : list of plasma frequency squared, length ns-by-1
%    Ws    : list of gyro frequencies, length ns-by-1
%    flag  : flag=1 plot diagostic figure
% Output:
%    wc    : list of positive cutoffs, (ns+2)-by-1
%            sorted in ascending order
%
% Written by Yuan SHI, Nov 13, 2017
% Last modified by Yuan Shi, Feb 21, 2019

function wc=cutoff(wps2,Ws,flag)

% default no plot
if nargin<3
    flag=0;
end

% smallness parameter for convergence
%ee=1e-9;

% plasma frequency
wp=sqrt(sum(wps2));
% magnetic field 
mag=Ws(1);

if mag==0 % unmagnetized
    wc=wp*ones(3,1); % 2 EM + Langmuir, sound wave has zero cutoff
else
    % normalization by mean Ws
    %w0=mean(abs(Ws));
    w0=wp;
    % root multiplicity
    ns=length(Ws);
    wn=w0^(ns+1);
    
    % function handel search for roots
    fun=@(w)fcutoff(w*w0,wps2,Ws)/wn;
    
    % convergence criteria
    %ew=ee*min(abs(Ws))/w0;
    
    % use Monte Carlo to estimate roots    
    [wtmp,~]=poly_roots_MC(fun,ns+1,1);
    wtmp=real(wtmp);
    
    % refine polynomial roots
    %x=poly_roots_Weierstrass(fun,x0,imax,eabs,erel)
    %wtmp=poly_roots_Weierstrass(fun,wtmp,1e3,ew,ee);
    wtmp=poly_roots_Weierstrass(fun,wtmp);
    
    % order positive frequencies
    wc=sort([w0*abs(wtmp);wp],'descend');
end

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag==1 && mag~=0
    clf        
    wmax=2*max(wtmp);
    x=linspace(-wmax,wmax,1000);
    f=fun(x);
    plot(x,f,'.-')
    hold on
    grid on
    % mark boundaries
    fmax=max(abs(f));
    for i=1:ns
        line([Ws(i)/w0 Ws(i)/w0],[-fmax,fmax],'Color','k')
    end    
    % resolve each root
    for i=1:ns+1
        x=linspace(0.95*wtmp(i),1.05*wtmp(i),1000);
        plot(x,fun(x),'.-')
    end
    % mark roots
    plot(wtmp,zeros(ns+1,1),'ok','MarkerFaceColor','k')
end

