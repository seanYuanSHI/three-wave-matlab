% This program calculate the wave 4-vector of vacuum EM wave
% in the reference frame boosted along the +z direction. 
%
% Assume the wave vector is of the form
%  k^mu=w*(1,sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta))
%
% Inputs: 
%    w0         : unboosted wave frequency
%    theta, phi : unboosted angles, in degree
%    beta       : boost velocity, in unit of c
% Output:
%    w     : boosted wave frequency, in same unit as w0
%    ck    : boosted wave vector, 3-by-1, in same unit as w0
%
% Written by Yuan SHI, Mar 9, 2022

function [w, ck] = zBoost(w0, theta, phi, beta)

    % check range
    assert(abs(beta)<1,'velocity > c!')
    % Lorentz factor
    gamma = 1/sqrt(1-beta^2);

    % convert angle
    d2r = pi/180;
    t = theta*d2r;
    st = sin(t);
    ct = cos(t);

    p = phi*d2r;
    sp = sin(p);
    cp = cos(p);

    % boosted frequency
    w = w0*gamma*(1-beta*ct);

    % boosted wave vector, in unit of w0
    kx = st*cp;
    ky = st*sp;
    kz = gamma*(ct-beta);
    % pack into vector
    ck = w0*[kx;ky;kz];
