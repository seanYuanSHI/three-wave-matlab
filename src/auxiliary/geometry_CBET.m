% This function convert scan angle to 
% theta-phi angles for main_CBET_driver.m
%
% Inputs:
%    angle : scan angle, in degree
%    across: angle between the two crossing beams, in degree
%    gos   : geometry of scan
%            gos=1: pump and seed on xz plan
%                   positive angle tilt k towards +z
%            gos=2: pump and seed on opposite side of a cone around x axis
%                   positive angle rotate right-handed w.r.t +x
% Outputs:
%    t1,p1 : angles for wave1
%    t2,p2 : angles for wave2
%
% Written by Yuan SHI on Mar 10, 2022

function [t1,p1,t2,p2]=geometry_CBET(angle, across, gos)
    % degree to rad
    d2r = pi/180;

    if gos==1 % tilt
        p1=0;
        p2=0;
        t1=(90+across/2)-angle; % blue shifted by v>0 flow
        t2=(90-across/2)-angle; % red shifted by v>0 flow
    else % rotate
        % sin, cos
        ca = cos(across/2*d2r);
        sa = sin(across/2*d2r);
        cp = cos(angle*d2r);
        sp = sin(angle*d2r);
     
        % convert k1hat to polar
        [azi,ele,~]=cart2sph(ca,sa*sp,-sa*cp);
        p1=azi/d2r;
        t1=90-ele/d2r;
     
        % convert k2hat to polar
        [azi,ele,~]=cart2sph(ca,-sa*sp,sa*cp);
        p2=azi/d2r;
        t2=90-ele/d2r;
    end
