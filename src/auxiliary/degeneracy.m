% This function is used to facilitate convergence of
% "ck_2_w_warm.m" in the unmagnetized case by 
% removing degeneracy of the two EM waves
% Inputs:
%      nw : number of dispersion branches
%      B0 : magnetic field
% Output:
%      deg: transformation matrix w2->deg*w2
%           such that w2(1)->w2(1)+dw, w2(2)->w2(2)-dw
%           where dw=w2(1)-w2(2), assuming w is 'descend' ordered
%
% Written by Yuan SHI, Jan 5, 2019

function deg=degeneracy(nw,B0)

% check input
assert(nw>1,'At least there should be 2 EM waves!')

% default is unit matrix
deg=eye(nw);

% remove degeneracy for unmagnetize case
if B0==0
    deg(1,1)=1.5;
    deg(1,2)=-0.5;
    deg(2,1)=-0.5;
    deg(2,2)=1.5;
end