% This function compute unmagnetized sound speed
%
% The unmagnetized longitudinal waves satisfies
%    w2=sun_s wps2/(1-us2*n2)*tau
% where tau is pole-removing polynomial
% Sound speed correspond to refractive index n2 s.t. w2=0
%
% Inputs:
%    wps2  : list of plasma frequency squared, length ns-by-1
%    us2   : list of thermal speeds^2/c^2, length ns-by-1
%    uindex: index of thermal species, length nt-by-1, nt<=ns
%    flag  : flag=1 plot diagnostic figure
% Output:
%    Cs    : list of sound speed normalized by c
%
% Written by Yuan SHI, Mar 26, 2018

%function Cs=sound(para,flag)
function Cs=sound(wps2,us2,uindex,flag)

% default no plot
if nargin<4
    flag=0;
end

% smallness parameter for search boundary
ee=1e-12;

% index of thermal species
%uindex=para.uindex;
% number of thermal species
nt=length(uindex);

% thermal speeds
%us2=para.us2;
% total number of species
ns=length(us2);
% number of sound waves
nc=nt+sign(ns-nt)-1;


if nc<1 % no sound wave
    Cs=[]; % no sound speed
else % exist sound wave  
    % inverse sound speed
    iCs2=zeros(nc,1);
        
    % 1/n2 poles
    in2=zeros(nt,1);
    for i=1:nt
        ind=uindex(i);
        in2(i)=1/us2(ind);
    end  
    in2=sort(in2,'ascend');
    % resolution in n2
    %dn2=min(in2);
    dn2=in2(1);
    dn2=dn2*ee;    
    
    % fundtion handel for search roots of sound speed
    %fun=@(n2)fsound(n2,para,0);    
    fun=@(n2)fsound(n2,wps2,us2,0);   
    
    % search sound wave    
    for j=1:nc
        if j<nt % search mid points        
            [n2,eflag]=zero_bisec(fun,in2(j)+dn2,in2(j+1)-dn2);
            assert(eflag~=2)
            iCs2(j)=n2;        
        else % search towards infty
            [n2,eflag]=zero_infty_bisec(fun,in2(nt)+dn2,1);
            assert(eflag~=2)
            iCs2(j)=n2;
        end
    end
 
    % sound speed normalized by c
    Cs=1./sqrt(iCs2);
end

% plot figure %%%%%%%%%%%%%%%%%%%%%%
if flag==1 && nc>0
    clf
    n2=linspace(0,2*max(iCs2),100);
    % plot big scale
    plot(n2,fun(n2),'.-')
    hold on
    % resolve each root
    for i=1:nc
        n2=linspace(iCs2(i)-dn2,iCs2(i)+dn2,100);
        plot(n2,fun(n2),'.-')
    end
    grid on
    plot(iCs2,zeros(nc,1),'ok','MarkerFaceColor','k')
end
   
