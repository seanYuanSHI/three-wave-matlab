% This function is used for root finding for sound.m
%
% The unmagnetized longitudinal waves satisfies
%    w2=sun_s wps2/(1-us2*n2)*tau
% where tau is pole-removing polynomial
% Sound speed correspond to refractive index n2 s.t. w2=0
%
% Inputs: 
%    n2    : refractive index^2=ck2/w2
%    wps2  : list of plasma frequency squared, length ns-by-1
%    us2   : list of thermal speeds^2/c^2, length ns-by-1
%    flag  : flag=1, plot figure
% Output:
%    w2    : frequency w^2
%
% Written by Yuan SHI, Mar 26, 2018

function w2=fsound(n2,wps2,us2,flag)

% initialize output
w2=zeros(size(n2));
tau=ones(size(n2));

% sum over thermal species
%wps2=para.wps2;
%us2=para.us2;
ns=length(wps2); % total number of species 


for i=1:ns
    pole=1-us2(i)*n2;
    tau=tau.*pole;
    w2=w2+wps2(i)./pole;
end
w2=w2.*tau;

% plot figure
if flag==1
    clf
    plot(n2,w2,'.-')
    grid on
end
