% This function compute the wave vector of a vacuum 
% EM wave after it enters into a magnetized plasma.
% 
% In slab geometry, plasma filles the x>0 half space.
% In cylinder geometry, the incident and refracted
% waves are in a plane containing the z axis. 
% In full geometry, plasma filles the whole space.
% The plasma has a uniform flow along the z//B0 direction.
%
% In the vacuum and lab frame, the wave vector is 
%  k^mu=w*(1,sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta))
%
% Inputs: 
%    w0         : unboosted wave frequency, in Trad/s
%    theta, phi : unboosted angles, in degree
%    bt         : wave branch index for transmitted beam
%                 b=1 for highest frequency branch
%    beta       : boost velocity, in unit of c
%    para       : structure containing plasma parameters
%    geometry   : choose from 'slab', 'cylinder', 'full'
% Output:
%    w     : wave frequency in plasma frame, in Trad/s
%    ckt   : wave vector inside plasma, 3-by-1
%
% Written by Yuan SHI, Mar 9, 2022
function [w, ckt] = BoostRefract(w0, theta, phi, bt, beta, para, geometry)
    %%%%%%%%%%%%%%%
    flag=-1; % use cold magnetized dispersion
    %flag=0; % use warm magnetized dispersion
    %%%%%%%%%%%%%%%
    if nargin<7
        geometry='full';
    end

    % transform into plasma frame, in vacuum
    [w,cki] = zBoost(w0, theta, phi, beta);
    % convert to spherical coordinate 
    [azi, ele, ~]=cart2sph(cki(1),cki(2),cki(3));

    if strcmp(geometry, 'full') % no refraction needed
        % angle with B0 in plasma
        thetat = 90*(1 - 2*ele/pi);
        % compute wave vector inside plasma for given frequency
        ckr=wb_2_ck_warm(w, bt, thetat, para, 0);
        % conver to cartesian coordinate
        [ckx, cky, ckz]=sph2cart(azi, ele, ckr);
        ckt = [ckx; cky; ckz];
    else
        % rotations if in cylinder geometry
        if strcmp(geometry, 'cylinder')
            % rotation matrix for coordinate transform
            % x = R*x'
            c = cos(azi);
            s = sin(azi);
            R = [[c,-s,0];[s,c,0];[0,0,1]];
        else
            R = eye(3);
        end
     
        % transform incident vector to slab frame
        ckir = R.'*cki; % .' is transpose
        % transmit into plasma
        cktr = refraction(ckir, bt, para, flag);
        % transform back to original frame
        ckt = R*cktr;
    end
