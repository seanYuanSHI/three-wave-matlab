% This program read data written by write_data.m
%
% Input:
%    fstem : string, file path
%    flag  : flag=0 read data in matlab format
%            flag=1 read data in netcdf format
% Outputs:
%    map   : three-wave scattering data structure 
%    setup : data structure containing experimental setup info
%
% Written by Yuan SHI, Feb 5, 2019

function [map,setup]=read_data(fstem,flag)

if flag==0
    fname=strcat(fstem,'data.mat');
    
    d=load(fname,'map');
    map=d.map;
    
    d=load(fname,'setup');
    setup=d.setup;
    
else
    fname=strcat(fstem,'data.nc');
    
    setup.w1=ncread(fname,'w1_Trads');
    setup.b1=ncread(fname,'b1');
    setup.b2=ncread(fname,'b2');
    setup.t1=ncread(fname,'t1_deg');
    
    setup.t2=ncread(fname,'t2_deg');
    setup.p2=ncread(fname,'p2_deg');
    
    map.w2=ncread(fname,'w2_Trads');
    map.ck2r=ncread(fname,'ck2r_Trads');
    map.ck1r=ncread(fname,'ck1r_Trads');
    map.C=ncread(fname,'C_Trads2');
    map.u123=ncread(fname,'u123');
    map.Sa=ncread(fname,'Sa_Trads2');
    map.M=ncread(fname,'M');
    map.b3=ncread(fname,'b3');
end
