% This program detect zero crossing of an array
% Input:
%    x  : an array of length n, real values
% Output:
%    ix : a list of indices where x(ix)*x(ix+1)<=0
%    dx : a list of gaps where zero crossing happens
%         dx=x(ix)-x(ix+1)
%    sx : a list of boundary conditions
%         sx= 0, x is not zero on either boundary
%         sx=-1, x is zero on left boundary only
%         sx= 1, x is zero on right boundary only
%         sx= 2, x is zero on both boundaries
%
% Written by Yuan SHI, Nov 17, 2017

function [ix,dx,sx]=zero_cross_array(x)

n=length(x);
itmp=zeros(n,1);
stmp=zeros(n,1);
dtmp=zeros(n,1);

jj=0;
for j=1:n-1
    l=x(j);
    r=x(j+1);
    % detect zero crossing
    if l*r<=0
        jj=jj+1;
        itmp(jj)=j;
        dtmp(jj)=l-r;
    end    
    % record boundary conditions
    if l==0 && r~=0
        stmp(jj)=-1;
    elseif l~=0 && r==0
        stmp(jj)=1;
    elseif l==0 && r==0
        stmp(jj)=2;
    end
end

if jj>0
    ix=itmp(1:jj);
    dx=dtmp(1:jj);
    sx=stmp(1:jj);
else % no zero crossing
    ix=[];
    dx=[];
    sx=[];
end

