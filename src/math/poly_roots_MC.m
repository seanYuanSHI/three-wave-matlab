% This function find roots a polynomial
%     f(x)=a_{n}*x^n+a_{n-1}*x^n+...+a1*x+a0
% when we are only given the function handel fun to evaluate f(x). 
%
% This program first compute coefficients of f(x) using random sampling.
% The roots of the polynomial are then found, using Monte Carlo to improve.
%
% Inputs:
%    fun   : function handel for evaluating polynomial
%    n     : order of the polynomial
%    M     : size of the Monte Carlo ensemble
% Outputs:
%    proots    : mean estimator of roots, length n
%    sigmas    : standard deviation of the mean estimator, length n
%
% Notice if n is given wrong, then large s is expected
%
% Written by Yuan SHI, April 2, 2018

function [proots,sigmas]=poly_roots_MC(fun,n,M)

assert(M>0)
assert(n>0)

% Initialize output
proots=zeros(n,1);
sigmas=zeros(n,1);

% sum over ensemble
for i=1:M    
    % find coefficients for polynomial
    [p,~]=poly_coef_MC(fun,n,1);
    % normalize by coefficient of x^n
    % p=p/p(1);
    
    % roots of polynomial
    r=roots(p); % may have large error

    % ensemble average
    proots=proots+r;
    % compute s2
    sigmas=sigmas+r.*r;
end

% mean estimator
proots=proots/M;
% biased std of mean
sigmas=sqrt(sigmas/M-proots.*proots);
sigmas=sigmas/sqrt(M);

    
