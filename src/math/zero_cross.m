% This program find roots using zero crossing detection
% for a single variable continuous function
%
% Inputs:
%    fun   : function handel, should be able to take vector x
%    a     : left interval boundary
%    b     : right interval boudary
%    dx    : minimum roots separation
%    flag  : flag=1 plot figure
% Outputs:
%    roots : list of roots, column vector
%
% Written by Yuan SHI, Feb 13, 2019

function roots=zero_cross(fun,a,b,dx,flag)

% parameters %%%%%%%%%%%%%%%%%%%%%%%%%
% maximum of bisection
iter=100;

% smallness for root convergence dx*err
err=1e-12;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check inputs
a0=min(a,b);
b0=max(a,b);
assert(length(dx)==1)
dx=abs(dx);

% generate search array
nx=ceil((b0-a0)/dx);
x=linspace(a0,b0,nx);

% function values evaluated at the search array
fx=fun(x);

% search for zero crossing
[ix,~,~]=zero_cross_array(fx);
nzero=length(ix);

% finding root for each zero crossing
rtmp=zeros(nzero,1);
ex=err*dx;
ii=0;
for j=1:nzero
    % left index of detected zero crossing
    jj=ix(j);
    
    % boundary of interval
    xl=x(jj);
    xr=x(jj+1);    

    % root by bisection
    [x0,eflag]=zero_bisec(fun,xl,xr,ex,iter);
    
    if eflag~=2 % root found
        ii=ii+1;
        rtmp(ii)=x0;
    end
end

% output
if ii>0
    roots=rtmp(1:ii);
else
    roots=[];
end

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag==1
    clf
    % function
    plot(x,fx,'.-')
    hold on
    grid on
    % resolve each root
    for i=1:ii
        x=linspace(roots(i)-dx,roots(i)+dx,100);
        fx=fun(x);
        plot(x,fx,'.-')
    end
    % mark roots
    plot(roots,zeros(ii,1),'ok','MarkerFaceColor','k')
end
