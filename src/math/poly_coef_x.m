% This function find coefficients of a polynomial
%     f(x)=a_{n}*x^n+a_{n-1}*x^n+...+a1*x+a0
% when we are only given the function handel fun to evaluate f(x). 
%
% The coefficients can be found by solving matrix equation
%          X*A=F
% where the X matrix is
%    [x1^n, x1^{n-2},...,x1,1;
%     x2^n, x2^{n-1},...,x2,1;
%     ...
%     x_{n+1}^n,        x_{n+1},1]
% and A=[a_{n};a_{n-1};...;a_1,a_0]
% and F=[f(x1);f(x2);...;f(x_{n+1})]
%
% This program find coefficients by evaluating f(x) at given x 
%
% Inputs:
%    fun   : function handel for evaluating polynomial, order n
%    x     : points to evaluate the polynomial, size (n+1)-by-1
% Outputs:
%    A     : coefficients
%
% Written by Yuan SHI, Mar 26, 2018

function A=poly_coef_x(fun,x)

% order of polynomial n + 1
n1=length(x);
assert(size(x,2)==1)
assert(n1>1)

% compute X matrix
X=ones(n1,n1);
xk=x;
for k=1:n1-1
    X(:,n1-k)=xk;
    xk=xk.*x;
end

% compute F vector
F=fun(x);

% compute A vector
A=X\F;


    
