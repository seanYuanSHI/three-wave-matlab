% This function find coefficients of a polynomial
%     f(x)=a_{n}*x^n+a_{n-1}*x^n+...+a1*x+a0
% when we are only given the function handel fun to evaluate f(x). 
%
% The polynomial is assumed to have been normalized such that
% all roots are not far out of the unit circle. 
% If there is no machine error, the coefficients can be found 
% by solving matrix equation
%          X*A=F
% where the X matrix is
%    [x1^n, x1^{n-2},...,x1,1;
%     x2^n, x2^{n-1},...,x2,1;
%     ...
%     x_{n+1}^n,        x_{n+1},1]
% and A=[a_{n};a_{n-1};...;a_1,a_0]
% and F=[f(x1);f(x2);...;f(x_{n+1})]
% However, when the polynomial roots are multiscale, choosing 
% some values of X may give large errors in A.
%
% This program use Monte Carlo to sample X, and return mean estimator 
% and its sample variance.
%
% Inputs:
%    fun   : function handel for evaluating polynomial
%    n     : order of the polynomial
%    M     : size of the Monte Carlo ensemble
% Outputs:
%    A     : mean estimator of coefficients, length n+1
%    s     : estimate of standard deviation of the mean estimator
%
% Notice if n is given wrong, then large s is expected, compared to |A|
%
% Written by Yuan SHI, Mar 24, 2018

function [A,s]=poly_coef_MC(fun,n,M)

assert(M>0)

% Initialize output
A=zeros(n+1,1);
s2=0;

% sum over ensemble
for i=1:M
    % sample in unit square
    x=2*rand(n+1,1)-1;
    y=2*rand(n+1,1)-1;
    z=x+1i*y; % complex
    
    % compute coefficients
    Ai=poly_coef_x(fun,z);

    % ensemble average
    A=A+Ai;
    % compute s2
    s2=s2+Ai'*Ai;
end

% mean estimator
A=A/M;
% biased std of mean
s=sqrt(s2/M-A'*A);
s=s/sqrt(M);

    
