% This program find zero of a continuous function by bisection
% on an invterval [a,infty), or (-infty,a]. 
% The function must have exactly one zero crossing on this interval
%
% Inputs:
%    fun  : function handle of a single variate function
%    a    : one side interval
%    s    : s=1,  search for root on [a,infty)
%           s=-1, search for root on (-infty,a]
% Outputs:
%    x0   : root of function
%    flag : flag=0, zero found
%           flag=1, jump found
% Parameter:
%    imax : maximum number of base interval duplications
%
% Written by Yuan SHI, Nov 13, 2017

function [x0,flag]=zero_infty_bisec(fun,a,s)

% search parameter
imax=1e2;
debug=0; % 1: plot figure

% check input
assert(length(a)==1);
assert(abs(s)==1);

% determine sign-change boundary
b=2*s*abs(a);
fa=fun(a);
fb=fun(b);
ii=1;
while fa*fb>0 && ii<imax
    b=2*b;
    fb=fun(b);
    ii=ii+1;
end
if ii>=imax
    warning('No root found towards infty!')
end
    
% use bisection to find root
if s>0
    [x0,flag]=zero_bisec(fun,a,b);
else
    [x0,flag]=zero_bisec(fun,b,a);
end

% plot figure
if debug==1
    clf
    x=linspace(a,b,1000);
    plot(x,fun(x),'.-')
    grid on
    hold on
    x=linspace(a,2*s*a,1000);
    plot(x,fun(x),'.-')
end
    
