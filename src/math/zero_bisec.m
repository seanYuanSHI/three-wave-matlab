% This program find zero of a continuous function by bisection
% on an invterval [a,b]. The function must have exactly one 
% zero crossing on this interval
%
% Inputs:
%    fun  : function handle of a single variate continuous function
%    a    : left interval
%    b    : right interval
%    ex   : convergence criteria for x, absolute
%    imax : maximum number of iteration
% Outputs:
%    x0   : root of function
%    flag : flag=0, zero found
%           flag=1, jump found
%           flag=2, same sign at a,b
% Parameters:
%    ef   : continuous criteria for fun, relative to initial average
%
% Written by Yuan SHI, Nov 13, 2017
% Modified by Yuan SHI, April 2, 2018

function [x0,flag]=zero_bisec(fun,a,b,ex,imax)

% convergence parameters
if nargin<4 % internally defined ex
    ex=1e-12;
end
if nargin<5
    imax=1e2; 
end
ef=1e-6; % jump criteria

% check inputs
% assert(b>=a);
a0=min(a,b);
b0=max(a,b);
a=a0;
b=b0;

% Check function changes sign on [a,b]
fa=fun(a);
fb=fun(b);
if fa*fb>0
    x0=(a+b)/2;
    flag=2;
else    
    % relative precesion
    %ex=ex*(b-a);
    ef=ef*abs(fb-fa);
    
    % Serch for root
    iter=0;
    if abs(fa)<=ef && abs(fb)>ef
        % root on left boundary
        x0=a;
        f0=fa;
    elseif abs(fa)>ef && abs(fb)<=ef
        % root on right boundary
        x0=b;
        f0=fb;
    elseif abs(fa)<=ef && abs(fb)<=ef
        % root on both boundary
        x0=(a+b)/2;
        f0=fun(x0);
    else
        % root not no boundary, bisection
        xl=a;
        xr=b;
        x0=(xr+xl)/2;
        f0=fun(x0);
        %while xr-xl>ex && abs(f0)>ef && iter<imax
        while xr-xl>ex && iter<imax
            if f0*fa>0
                xl=x0;
            else
                xr=x0;
            end
            x0=(xr+xl)/2;
            f0=fun(x0);
            iter=iter+1;
        end
    end
    
    % check function convergence
    if abs(f0)>ef
        flag=1;
        %     disp(['zero_bisec not converged, final f0=',num2str(f0)]);
    else
        flag=0;
    end
end
