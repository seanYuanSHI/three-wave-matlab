% This function determine xa beyond which f(x) is asymptotic to a*x^N,
% assuming this is indeed the asymptotic behavior of fun.
%
% The search is conducted using the following algorithm:
% Suppose x1,x2>xa, then f1~a*x1^N and f2~a*x2^N.
% The exponent is then N~log(f2/f1)/log(x2/x1), 
% which should be independent of the choice of x1,x2.
% Therefore, given initial guess x0, we can take xn=n*x0.
% The series Nn=log(f_{n+1}/f_{n})/log(x_{n+1}/x_{n}) should 
% converge for large n. The convergence is determined by Cauchy condition.
%
% Inputs:
%    fun  : function handel for evaluating y=fun(x)
%           f(x)~a*x^N when x->infty
%    x0   : initial guess of xa
%    eN   : convergence criteria for N
%           eg. if N is expected to be an interger, 
%           then eN=1 may be sufficient
%    flag : flag=1 plot figure
% Outouts:
%    xa   : asymptotic boundary
%    fa   : asymptotic value fa=fun(xa)
%    Na   : asymptotic exponent N
%
% Written by Yuan SHI, Feb 4, 2019


function [xa,fa,Na]=asympt_interval_refine(fun,x0,eN,flag)

% Maximum number of iteration
iter=100;

% default no plot
if nargin<4
    flag=0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initial values
if x0==0
    x00=1; % step away from zero in positive direction
else
    x00=x0;
end
x1=x00;
f1=fun(x1);
N1=log(abs(f1)+1)/log(abs(x1)+1); % avoid negative and zero
dN=10*eN;

% iterative search
ii=1;
while ii<=iter && dN>eN
    % compute next sample
    x2=(ii+1)*x00;
    f2=fun(x2);
    
    % compute exponent
    N2=log(f2/f1)/log(x2/x1);
    dN=abs(N2-N1);
    
    % update loop
    ii=ii+1;
    x1=x2;
    f1=f2;
    N1=N2;
end

xa=x1;
fa=f1;
Na=N1;
if ii>iter
    warning('Expected asymptotic behavior not attained!')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot figure
if flag==1 
    clf
    x=linspace(x0,xa,100);
    f=fun(x);
    p=x.^Na;
    semilogy(x,abs(f))
    hold on
    semilogy(x,abs(p),'.-')
end
    