% This is am implimentation of Newton's method for root finding
% Assume univariate f(x) and x are properly normalized s.t. both ~O(1)
%
% Suppose z_0=x0 is an initial guess, then 
%            z_{n+1}=z_{n}-f(z_{n})/f'(z_{n})
% is a better approximation of the root. Assuming the function f(x)
% is well behaved near x0, which is sufficiently close to the w, 
% at which f(w)=0. Then if f'(w) is nonzero, the z_n will
% converge to w when n->infty.
%
% Inputs:
%    fun   : function handel to evaluate univariate function f(x)=fun(x)
%    x0    : start point for root finding, vector of length nx
%    erf   : absolute convergence criteria for f(x): |f(z)|<erf
%    erx   : absolute criteria for x: |z-w|<erx
%    iter  : maximum number of iterations
%
%            The iteration stop when either f(x) or x 
%            tolerance goal is reached, or maximum number
%            of iterations is reached.
%            
%            If erx=0, then convergence is entirely determined by f(x)
%            If erf=0, then convergence is entirely determined by x
%
% Output:
%    x     : the found root of the function, vector of length nx 
%    flag  : error flag, flag=0 no error
%
% Written by Yuan SHI, Mar 21, 2018


function [x,flag]=zero_Newton(fun,x0,erf,erx,iter)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% machine precision limit
err=1e-12;

% limitors to prevent jumping to other zeros
% relative maximum step size limitor
dx_rel=1e-1;
% absolute maximum step size limitor
%dx_abs=10*err;
dx_abs=max(2*erx,10*err);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%assert(erx>err)
%assert(dxmax>err)

% make sure erf and erx are positive
erf=abs(erf);
erx=abs(erx);
% It both zeros, then cannot converge
assert(erf+erx>0)

% loop over all search points
nx=length(x0);
x=zeros(1,nx);
for i=1:nx    
    % initial guess
    xi=x0(i);
    
    % initial function value
    f=fun(xi);
    if isnan(f)
        % 0/0, set f to large value
        f=1/err;
    end
    
    % initial function error
    fe=abs(f);
    dx=2*erx+1;
    
    ii=0;
    while fe>erf && abs(dx)>erx && ii<iter
        % derivative
        %df=df_center(fun,xi,err);
        df=df_center(fun,xi,dx_abs);
        %fun(xi)
        %fun(xi+dx_abs)
        
        if df~=0 % nonzero
            % error correction
            dx=-f/df;
            
            % apply limitor
            sx=sign(dx);
            dxmax=max(dx_rel*abs(xi),dx_abs);
            dx=sx*min(max(abs(dx),erx/2),dxmax);
            
            % update guess
            xi=xi+dx;
            
            % new function value
            f=fun(xi);
            fe=abs(f);
            
            % update counter
            ii=ii+1;
        else
            ii=iter;
            warning('Zero derivatve df!')
        end
    end
    
    % load value
    x(i)=xi;
    
    % report potential problems
    if ii==iter
        flag=1;
        warning('Newton iteration fails to converge!')
        disp(['df=',num2str(df)])
        disp(['f(x) error: ',num2str(f),', target f(x)<=',num2str(erf)])
        disp(['x error: ',num2str(dx),', target dx<=',num2str(erx)])
        disp(['Limitor dx=',num2str(dxmax)])
    else
        flag=0;
    end
end
