% This function refine polynomial roots using Weierstrass iteration
%
% Convergence can be slow when roots are separated by orders 
% of magnetude or when roots are degenerate.
%
% Inputs:
%    fun  : function handel for evaluating the polynomial of degree n>1
%           The coefficient of highest power of x should be one
%    x0   : initial guess of all roots, length n
%
%    imax : maximum number of iterations
%    eabs : absolute smallness parameter
%    erel : relative error in unit of min|x|
%           convergent if max|dx|<max(erel*min|x|,eabs) 
%
% Output:
%    x    : refined roots, length n
%
% Written by Yuan SHI, Jan 2, 2019
% Last modified by Yuan SHI, Feb 20, 2019

function x=poly_roots_Weierstrass(fun,x0,imax,eabs,erel)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% default parameters 
if nargin<3
    imax=1e3;       
end
if nargin<4
    eabs=1e-9;     
end
eabs=abs(eabs);
if nargin<5
    erel=1e-6;    
end
erel=abs(erel);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% degree of polynomial
n=length(x0);
assert(n>1,'Should use Aberth-Ehrlich method instead!')

% initialize output
x=x0;
% initialize corrections
%dx=zeros(size(x0));
% initialize convergence criteria
%c=zeros(size(x0));

% refine roots iteratively
iter=0; % number of iterations
%sc=0;   % summed convergence criteria
c=0; % global convergence
%while sc<n && iter<imax 
%clf;hold on
while c<1 && iter<imax 
    % initial error small to triger update
    ce=0;
    % Weierstrass iteration
    for i=1:n        
        xi=x(i);
        fi=fun(xi);
        di=1;
        for j=1:n
            p=abs(sign(i-j));
            di=di*(xi-x(j))^p;
            %if j~=i
            %    di=di*(xi-x(j));                
            %end
        end
        % corrections for each root
        dxi=-fi/di;
        %dx(i)=dxi;
        % serial update
        x(i)=xi+dxi;
        % global convergence error
        ce=max(ce,abs(dxi));
        % check x convergence, at least 9 iterations
        %c(i)=sign(iter-8)*sign(max(erel*abs(xi),eabs)-abs(dxi));
    end
    % parallel update 
    %x=x+dx; 
    %semilogy(iter,abs(dx),'b.')
    
    % global convergence criteria
    cc=max(erel*min(abs(x)),eabs);
    % converge of c==1 <=> cc>ce
    c=sign(cc-ce);
    
    % update iteration
    %sc=sum(c);
    iter=iter+1;
end

if iter==imax && c<1
    warning(['Weierstrass has not converged!',char(10),...
        'criteria=',num2str(cc),', error=',num2str(ce),char(10),...
        'The coefficient of highest power x^n should be 1',char(10),...
        'Check deg of polynomial, increase iteration/relax convergence']) 
end
% if iter==imax && sc~=n
%     warning(['Summed convergence is only ',num2str(sc),...
%         ' for ',num2str(n),' roots!',newline,...
%         'Check deg of polynomial, increase iteration/relax convergence']) 
% end


