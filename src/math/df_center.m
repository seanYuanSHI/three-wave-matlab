% This function evaluate centered derivetive f'(x0) 
%
% Inputs:
%    fun   : function handel to evaluate f(x)=fun(x)
%    x0    : point to evaluate derivative
%    h     : step size, machine precision limit h>~1e-12
%
% Output:
%    df    : centered derivative
%    h     : step size for computing derivative
%
% Written by Yuan SHI, Mar 19, 2018

function df=df_center(fun,x0,h)
%function df=df_center(fun,x0)

%h=1e-11; % As precise as practical with machine precision

df=(fun(x0+h/2)-fun(x0-h/2))/h;