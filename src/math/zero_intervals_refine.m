% This program refine intervals where zeros of a function can exist.
%
% y=f(x) may have zero on x\in(ai,bi)
% This program further select x\in(ao,bo)\subset(ai,bi)  
% There may be more subsets when there are multiple zeros
%
% Assuming the function is continuously differentiable,
% then when the interval is sufficiently refined, the 
% function is approximately linear. 
%
% To determine linearity, fun can be compared with
%   p(x)=yb*(x-a)/(b-a)+ya*(x-b)/(a-b);
% When std(f-p)<ee*std(f), where ee is some parameter, 
% the function is considered sufficiently linear.
% 
% Assume no root is not on the boundary.
% Notice that if f0 is too large or sample is
% too sparse then zeros may be skiped
%
% Inputs:
%    fun  : function handel for evaluating y=fun(x)
%           fun is assumed to be a continuous function
%    f0   : reject x if fun(x)>f0
%    ai   : initial left boundaries, length ni
%    bi   : initial right boundaries, length ni
%    ci   : linear criteria, length ni
%           c=1 if fun is approximately linear on the interval
%           c=0 if fun is not sufficiently linear on the inteval
%    nx   : number of sampling points in each interval
%    flag : flag=1 plot figure
% Outouts:
%    ao   : refined left boundaries, length no>=ni
%    bo   : refined right boundaries, length no>=ni
%    co   : linear criteria, length ni
%           c=1 if fun is approximately linear on the interval
%           c=0 if fun is not sufficiently linear on the inteval
%
% Written by Yuan SHI, Feb 3, 2019

function [ao,bo,co]=zero_intervals_refine(fun,nx,ai,bi,ci,f0,flag)

% smallness parameter for linear criteria
ee=0.1;
% sample size for determining linearity
nl=10;

% ensure f0>0
f0=abs(f0);

% Check input
ni=length(ai);
assert(length(bi)==ni,'a,b,c should be same length!')
assert(length(ci)==ni,'a,b,c should be same length!')

% initialize outputs
% at most of length nx*ni
% atmp=zeros(nx*ni);
% btmp=zeros(nx*ni);
% ctmp=zeros(nx*ni);
atmp=zeros(nx,1);
btmp=zeros(nx,1);
ctmp=zeros(nx,1);

% refine each interval
ii=0;
for i=1:ni
    % from right to left, because more nosy on left
    ind=ni-i+1;
    a=ai(ind);
    b=bi(ind);
    assert(b>=a,['right=',num2str(b),' must larger than left=',num2str(a)])
    if ci(ind)==1 % already linear
        %disp(['No need to refine interval ',num2str(i)])
        ii=ii+1;
        atmp(ii)=a;
        btmp(ii)=b;
        ctmp(ii)=1;
    else % need further refinement       
        %disp(['Refine interval ',num2str(i)])
        % generate samples
        x=linspace(a,b,nx);
        y=fun(x);
        
        % search for intervals
        l=1; % left index
        r=1; % right index
        s=0; % s=0 outside intervals
             % s=1 have entered an interval
             % s=2 have exited an interval
        for j=1:nx
            % search from right to left
            jj=nx-j+1; 
            
            yj=y(jj);
            ayj=abs(yj);
            
            % update right boundary
            if ayj<f0 && s==0
                r=jj;
                s=1;
            end
            
            % update left boundary
            if (ayj>=f0 || jj==1) && s==1
                l=jj;
                s=2;
            end            
            
            % identify an interval
            if s==2 
                % left boundaries values
                xl=x(l);
                yl=y(l);
                
                % right boundaries values
                xr=x(r);
                yr=y(r);
                
                % register interval
                ii=ii+1;
                atmp(ii)=xl;
                btmp(ii)=xr;           
                                
                % criteria to stop refine 
                % single root if linear && cross zero  
                if yl*yr<0
                    % linear approximation
                    z=linspace(x(l),x(r),nl);
                    p=((yr-yl)*z+yl*xr-yr*xl)/(xr-xl);
                    % standard deviations
                    w=fun(z);
                    s0=std(w);
                    s1=std(w-p);   
                    
                    ctmp(ii)=sign(ee*s0-s1);
                end                
                s=0;
            end
        end
    end
end

% output
if ii==0 % no interval remains
    ao=[];
    bo=[];
    co=[];
else
    if ii>nx
        warning('Interval refine may be too noisy!')
    end
    ii=min(ii,nx);
    % ascend order
    ao=flipud(atmp(1:ii));
    bo=flipud(btmp(1:ii));
    co=flipud(ctmp(1:ii));
end

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag==1
    clf
    % plot fun on sparse grid
    x=linspace(ai(1),bi(ni),nx);
    y=fun(x);
    plot(x,y,'.-')
    hold on
    grid on
    
    % plot on each initial interval
    for i=1:ni
        x=linspace(ai(i),bi(i),nx);
        y=fun(x);
        plot(x,y,'.-')
    end
    
    % plot on each final interval
    if ii>0
        for i=1:ii
            x=linspace(ao(i),bo(i),nx);
            y=fun(x);
            plot(x,y,'.-')
        end
    end
end
    
            
    