% This function finds all roots of a continuous function on 
% given interval using Lebesgue interval refine. 
%
% Inputs:
%    fun   : function handel of the continous function
%    a     : left boundary of the interval, scalar
%    b     : right boundary of the interval, scalar
%    ex    : absolute error tolerance for roots
%    flag  : flag=1 plot dianostic figure
%            flag=2 plot without erasing previous figure
%
%    rejec : rejec=1 reject root on rightest boundary
%            rejec=-1 reject root on leftest boundary 
%            reject=2 or -2, reject both boundaries
%    rate  : exclude f1>f0/rate for next level of interval refinement
%            slow rate may result in slow convergence
%            fast rate may miss roots for low resolution nx
%    nx    : number of sampling points for interval refine
%    iter  : maximum interactions for refinement
%
% Outputs:
%    roots: all roots of fun on [a,b]
%    t    : t=1 if rejection actually occured
%           t=0 if no rejection occured
%
% Written by Yuan SHI, Feb 6, 2019

function [roots,t]=zeros_continue(fun,a,b,ex,flag,rejec,rate,nx,iter)

%disp('enter zero_continue')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% default parameters
if nargin<6
    rejec=0;
end
if nargin<7
    rate=3; 
end
if nargin<8
    nx=100;
end
if nargin<9
    iter=100; 
end

debug=0; % debug=1: showing debug message

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check input
assert(rate>1,'y interval must be shrinking!')
%assert(b>a,'right should be larger than left!')
ex=abs(ex);

ni=length(a);
assert(ni==length(b),'left and right should be same length!');

% initial sampling of global structure of fun
al0=min(a);  
br0=max(b);
x=linspace(al0,br0,nx);
f=fun(x);
fmax=max(abs(f));

% iteratively identify intervals each contain only one root
%a0=min(a,b); % left boundary
%b0=max(a,b); % right boundary
a0=a;
b0=b;
c0=zeros(size(a0)); % linear criteria satisfied?
nc=0; % number of intervals already satisfy linear criteria
f0=fmax/rate; % exclusion fun>f0
t=0; % default no rejection
nw=1; % initially one interval
ii=1;
%disp('refining intervals...')
while ii<=iter && nc<nw
    [a0,b0,c0]=zero_intervals_refine(fun,nx,a0,b0,c0,f0,0);
    nw=length(c0); % maximum number of possible roots
    nc=sum(c0); % number of intervals that are already linear
    f0=f0/rate; % next y interval
    ii=ii+1;
    
    %disp('condier boundaries...')
    % reject right boundary
    % possibly single root near right boundary
    if (rejec==1 || abs(rejec)==2) && nw>1 && b0(nw)>br0-ex && c0(nw)==1 
        % right boundary values
        ar=a0(nw);
        br=b0(nw);
        far=fun(ar);
        fbr=fun(br);        
        
        % linear approximation of root
        xr=(ar*fbr-br*far)/(fbr-far);
        
        % reject interval if no root or root at boundary
        if xr<ar || xr>br0-ex       
            a0=a0(1:nw-1);
            b0=b0(1:nw-1);
            c0=c0(1:nw-1);
            nw=nw-1;
            nc=nc-1;
        end
        
        % count root on boundary
        if abs(xr-br0)<ex
            t=1;
        end
    end
        
    % reject left boundary
    % possibly single root near left boundary
    if (rejec==-1 || abs(rejec)==2) && nw>1 && a0(1)<al0+ex && c0(1)==1 
        % left boundary values
        al=a0(1);
        bl=b0(1);
        fal=fun(al);
        fbl=fun(bl);        
        
        % linear approximation of root
        xl=(al*fbl-bl*fal)/(fbl-fal);
        
        % reject interval if no root or root at boundary
        if xl>bl || xl<al0+ex       
            a0=a0(2:nw);
            b0=b0(2:nw);
            c0=c0(2:nw);
            nw=nw-1;
            nc=nc-1;
        end
        
        % count root on boundary
        if abs(xl-al0)<ex
            t=1;
        end
    end
end
if ii>iter
    warning('fun not linear on all intervals! Roots may be missing!')
end

% debug message
if debug==1
    disp(['a=',num2str(a),', b=',num2str(b),', ex=',num2str(ex),...
        ', rejec=',num2str(rejec),', ii=',num2str(ii),', t=',num2str(t)])
    disp(['a0=',num2str(a0')])
    disp(['b0=',num2str(b0')])
end


% find root on each interval
rtmp=zeros(1,nw); % temporary storage of roots
ii=0;
%disp('finding roots...')
for i=1:nw    
    [x0,s]=zero_bisec(fun,a0(i),b0(i),ex,iter);
    %[k0,s]=zero_Newton(fun,(a0(i)+b0(i))/2,0,ek,iter);
    
    if s~=2 % root found
        ii=ii+1;
        rtmp(ii)=x0;
    else
        warning(['root error! s=',num2str(s)])
    end
end

% load output
%disp('loading outputs...')
roots=rtmp(1:ii);

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag>0
    if flag==1
        clf
    end
    % plot big scale
    plot(x,f,'.-')
    hold on
    % resolve each root
    for i=1:nw
        ck=linspace(a0(i),b0(i),100);
        plot(ck,fun(ck),'.-')
    end
    grid on
    plot(roots,zeros(ii,1),'ok','MarkerFaceColor','k')
end
%disp('leave zero_continue')
