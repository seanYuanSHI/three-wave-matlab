% This program write netcdf data for main_map.m
%
% Inputs:
%    map   : three-wave scattering data structure 
%    setup : data structure containing experimental setup info
%    para  : structure containing plasma parameters
%    fstem : string, file path 
%    flag  : flag=0 save data in matlab format
%            flag=1 write netcdf format
%
% The default file name is data.nc
% Revised by Yuan SHI, Feb 3, 2018


function write_data(map,setup,para,fstem,flag)

if flag==0
    % save data in matlab format
    % open file
    fname=strcat(fstem,'data.mat');
    save(fname,'map','setup','para')
else
    % open file
    fname=strcat(fstem,'data.nc');
    ncid=netcdf.create(fname,'CLOBBER'); % overwrite
    
    % unload data structures
    w2=map.w2;
    Zs=para.Zs;
    
    % dimension of variables
    nx=size(w2,1); % number of x grid points
    ny=size(w2,2); % number of y grid points
    nw=size(w2,3); % number of wave branches
    ns=length(Zs); % number of plasma species
    
    % define dimensions
    n1id=netcdf.defDim(ncid,'dim_1',1);
    nxid=netcdf.defDim(ncid,'nx_dim',nx);
    nyid=netcdf.defDim(ncid,'ny_dim',ny);
    nwid=netcdf.defDim(ncid,'nw_dim',nw);
    nsid=netcdf.defDim(ncid,'ns_dim',ns);
    
    % define variables
    B0id=netcdf.defVar(ncid,'B0_MG','double',n1id);
    w1id=netcdf.defVar(ncid,'w1_Trads','double',n1id);
    b1id=netcdf.defVar(ncid,'b1','double',n1id);
    b2id=netcdf.defVar(ncid,'b2','double',n1id);
    t1id=netcdf.defVar(ncid,'t1_deg','double',n1id);
    
    Zsid=netcdf.defVar(ncid,'Zs_e','double',nsid);
    Msid=netcdf.defVar(ncid,'Ms_me','double',nsid);
    Nsid=netcdf.defVar(ncid,'Ns_20cc','double',nsid);
    psid=netcdf.defVar(ncid,'ps','double',nsid);
    Tsid=netcdf.defVar(ncid,'Ts_keV','double',nsid);
    
    t2id=netcdf.defVar(ncid,'t2_deg','double',[nxid nyid]);
    p2id=netcdf.defVar(ncid,'p2_deg','double',[nxid nyid]);
    
    w2id=netcdf.defVar(ncid,'w2_Trads','double',[nxid nyid nwid]);
    ck2rid=netcdf.defVar(ncid,'ck2r_Trads','double',[nxid nyid nwid]);
    ck1rid=netcdf.defVar(ncid,'ck1r_Trads','double',n1id);
    Cid=netcdf.defVar(ncid,'C_Trads2','double',[nxid nyid nwid]);
    u123id=netcdf.defVar(ncid,'u123','double',[nxid nyid nwid]);
    Said=netcdf.defVar(ncid,'Sa_Trads2','double',[nxid nyid nwid]);
    Mid=netcdf.defVar(ncid,'M','double',[nxid nyid nwid]);
    b3id=netcdf.defVar(ncid,'b3','double',[nxid nyid nwid]);
    
    % Finish definition
    netcdf.endDef(ncid);
    
    % write data
    netcdf.putVar(ncid,B0id,para.B0);
    netcdf.putVar(ncid,w1id,setup.w1);
    netcdf.putVar(ncid,b1id,setup.b1);
    netcdf.putVar(ncid,b2id,setup.b2);
    netcdf.putVar(ncid,t1id,setup.t1);
    
    netcdf.putVar(ncid,Zsid,Zs);
    netcdf.putVar(ncid,Msid,para.Ms);
    netcdf.putVar(ncid,Nsid,para.Ns);
    netcdf.putVar(ncid,psid,para.ps);
    netcdf.putVar(ncid,Tsid,para.Ts);
    
    netcdf.putVar(ncid,t2id,setup.t2);
    netcdf.putVar(ncid,p2id,setup.p2);
    
    netcdf.putVar(ncid,w2id,w2);
    netcdf.putVar(ncid,ck2rid,map.ck2r);
    netcdf.putVar(ncid,ck1rid,map.ck1r);
    netcdf.putVar(ncid,Cid,map.C);
    netcdf.putVar(ncid,u123id,map.u123);
    netcdf.putVar(ncid,Said,map.Sa);
    netcdf.putVar(ncid,Mid,map.M);
    netcdf.putVar(ncid,b3id,map.b3);
    
    netcdf.close(ncid);
end
