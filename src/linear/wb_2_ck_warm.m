% This function solve for ckr such that the dispersion
% relation w=wb(ckr)is satisfied.
%
% This function utilize ck_2_w_warm.m
% There is either one root or no root
%
% This program may miss extremely large ck for plasma waves
% These solutions, even found, would invalidate the fluid model
%
% Inputs:
%    w     : wave frequency in unit of Trad/s, scalar
%    b     : branch index, b=1 highest frequency branch
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%    flag  : plot figure if flag=1
%
% Output:
%    ckr  : length of wave vector in Trad/s, scalar
%
% Written by Yuan SHI, Feb 4, 2019

function ckr=wb_2_ck_warm(w,b,theta,para,flag)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters
eN=1; % convergence criteria for asymptotic exponent
iter=100; % maximum interactions for refinement
err=1e-12; % convergence criteria for ck2r in unit w

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check input
assert(length(w)==1)
assert(length(b)==1)
ek=err*w;

% construct search function
function fd=fwb2ck_warm(ckr,w,b,theta,para)
    nk=length(ckr);
    omega=ck_2_w_warm(ckr,theta,para,0);
    fd=omega(b,1:nk)-w*ones(1,nk);
end
% function handel for root finding
fun=@(ckr)fwb2ck_warm(ckr,w,b,theta,para);
    

% decide method based on w
nn=para.nn; % nn=1 if non neutral
wa=para.wa;
% characteristics of gapless modes
[~,vp2,ngl,~]=ck_2_w_small(wa,theta,para,0);
% index of first gapless mode
nw=length(vp2);
ib=nw-ngl+1; % if no such mode, ngl=0 and ib>nw
    

if w<wa && nn==0 && b>=ib % use asymptotic dispersion
    % phase speed
    vp=sqrt(vp2(b));
    % wave vector
    ckr=w/vp;
    % for plotting
    kmax=2*ckr;
else  % use root finding
    % identify upper bound where roots can exist
    w0=max(w,para.wp);
    [kmax,fmax,Na]=asympt_interval_refine(fun,w0,eN,0);
    
    % search for root if exist
    f0=fun(0);
    df=df_center(fun,kmax,ek);
    % default values
    s=2;
    if f0*fmax<0 || (df*fmax<0 && abs(Na)> eN) % exist single root
        [ck0,s]=zero_bisec(fun,0,kmax,ek,iter);
        %[ck0,s]=zero_Newton(fun,kmax/2,0,ek,iter);
    end
    
    if s~=2
        ckr=abs(ck0);
        kmax=max(kmax,2*ck0);
    else
        ckr=[];
    end
end

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%
if flag==1
    clf
    ck=linspace(-kmax,kmax,1000);
    plot(ck,fun(ck),'.-')
    hold on
    ck=linspace(ckr-1e2*ek,ckr+1e2*ek,100);
    plot(ck,fun(ck),'.-')
    grid on
    plot(ckr,zeros(size(ckr)),'ok','MarkerFaceColor','k')
end

end