% This function compute refractive index n^2 from frequency w
% The magnetic field is in the z-direction
% The wave vector is in the xz-plane
% See AppendixB in PRE 96.023204 for details
%  
% Inputs:
%    w     : frequency of the wave in 2*pi*THz
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%    flag  : plot figure if flag=1
% Outputs:
%    n2p : positive branch of the quadratic equation
%    n2n : negative branch of the quadratic equation
%    n2=-999 if exact on resonance, in which case n2 goes to infinity
%
% Written by Yuan SHI, Nov 10, 2017

function [n2p,n2n]=w_2_n2(w,theta,para,flag)

% Initialie default outputs
n2p=-999;
n2n=-999;

% convert angle from degree to rad
t=theta/180*pi;
s2=sin(t)^2;
c2=cos(t)^2;

% Stix's dielectric functions
[S,D,P]=Stix(w,para);

% Coefficients of the qurdratic equation for n2
% A*n2^2-B*n2+C=0
A=S*s2+P*c2;

R=S+D;
L=S-D;
B=R.*L*s2+P.*S*(1+c2);

C=P.*R.*L;

% determinant of quadratic equation for n2
F2=B.^2-4*A.*C;
%assert(F2>=0);
F=sqrt(F2);

% solve quadratic equation
if A~=0 % not on resonance
    n2p=(B+F)/2./A;
    n2n=(B-F)/2./A;
else
    disp(['Exactly on resonance! w=',...
        num2str(w),' theta=',num2str(theta)])
end

% plot figure
if flag==1
    clf
    plot(w,n2n,'.')
    hold on
    plot(w,n2p,'.r')
    plot(w,ones(size(w)),'.','color',[0.5,0.5,0.5])
    plot(w,zeros(size(w)),'.','color',[0.5,0.5,0.5])
    
    nw=length(w);
    axis([0 w(nw) -1e2 1e2])
    
    % mark cutoffs
    wc=cutoff(para);
    plot(wc,zeros(size(wc)),'ok','MarkerFaceColor','k')
    
    % mark resonance
    yLp=max(n2p);
    yLn=max(n2n);
    yL=max(yLp,yLn);
    wr=resonance(theta,para);
    nr=length(wr);
    for j=1:nr
        line([wr(j),wr(j)],[-yL,yL],'Color','k')
    end
end
