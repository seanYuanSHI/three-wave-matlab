% This function compute coefficients of the 
% implicit dispersion relation A*n^4-B*n^2+C=0
% where A,B,C are functions of w and ck
%  
% Inputs:
%    w     : frequency of the wave in Trad/s, vector of length nw
%    ck    : norm of wave vector in unit of w, vector of length nk
%    theta : angle between wave vector and B-field in degree, scalar
%    para  : structure containing input parameters
%    flag  : flag=1,2,3,4 plot A,B,C,F respectively
% Outputs:
%    A, B, C : coefficients of size nw*nk
%    F       : determinant F2=B^2-4*A*C
%
% Written by Yuan SHI, Mar 13, 2018

function [A,B,C,F]=wck_2_ABCF_warm(w,ck,theta,para,flag)

% convert angle from degree to rad
t=theta/180*pi;
s2=sin(t)^2;
c2=1-s2;

% Warm Stix's dielectric functions
[Sw,Dw,Pw,Tw,Ew]=Stix_warm_wck(w,ck,theta,para,0);

% Coefficients of the qurdratic equation for n2
% A*n2^2-B*n2+C=0
A=Sw*s2+Pw*c2;

Rw=Sw+Dw;
Lw=Sw-Dw;
B=Rw.*Lw*s2+Pw.*Sw*(1+c2)+2*Tw.*A;

C=Pw.*Rw.*Lw+Tw.*(B-Tw.*A)+Ew.*(2*Pw.*Dw-Ew.*A)*c2;

% determinant of quadratic equation for n2
F2=B.^2-4*A.*C;
%assert(F2>=0);
F=sqrt(F2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot figure
if flag==1
    plot_nwnk(w,ck,A,'k.-')
elseif flag==2
    plot_nwnk(w,ck,B,'b.-')
elseif flag==3
    plot_nwnk(w,ck,C,'r.-')
elseif flag==4
    plot_nwnk(w,ck,F,'g.-')
end
