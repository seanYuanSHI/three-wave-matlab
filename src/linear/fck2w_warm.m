% This function is used in ck_2_w_warm.m for root finding
%     fd=tau*(A*ck2^2-B*ck2*w2+C*w2^2)
% where A,B,F are fucntions of w for given ck
% tau is pole removing polynomial
%
% Input:
%    w     : frequency of the wave in 2*pi*THz
%    ck    : norm of wave vector in unit of w, scalar
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%    flag  : flag=1, plot figure
%
% Output:
%    fd    : value of difference
%
% Written by Yuan SHI, April 1, 2018

function fd=fck2w_warm(w,ck,theta,para,flag)

% Check ck is scalar, namely, given ck, find w
assert(length(ck)==1)
ck2=ck^2;
ck4=ck2^2;

% Compute dispersion coefficients
[A,B,C,~]=wck_2_ABCF_warm(w,ck,theta,para,0);

% Polynomial to remove thermal poles
tau=pole_w_remove_warm(w,ck,theta,para,0);

% compute fd function
nw=length(w);
fd=zeros(nw,1);
w2=w.^2;
for i=1:nw
    f0=A(i)*ck4-B(i)*ck2*w2(i)+C(i)*w2(i)^2;
    fd(i)=f0*tau(i);
end
% ensure real-valued
fd=real(fd);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot figures
if flag>0
    plot(w,fd,'.-')
    hold on
    grid on 
    
    % thermal and magnetic poles    
    wt=pole_thermal_w(ck,theta,para,0); % size (ns,3,nk)
    
    % mark poles
    ymax=max(fd);
    ymin=min(fd);
    ns=size(wt,1); % number of species
    for j=1:ns
        line([wt(j,1,1),wt(j,1,1)],[ymin,ymax],'Color','b')       % wn
        line([wt(j,2,1),wt(j,2,1)],[ymin,ymax],'Color',[1 1 1]/2) % Ws
        line([wt(j,3,1),wt(j,3,1)],[ymin,ymax],'Color','r')       % wp
    end
end