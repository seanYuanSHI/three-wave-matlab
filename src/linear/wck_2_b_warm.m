% This function determine the branch index given w and ck
% This function also check if dispersion relation is indeed 
% satisfied upto some error tolerance.
%
% The error tolerance is ee*dw
%   ee: parameter set at the begining of the program
%   dw: next-to-minimum branch separation
%
% Inputs:
%    w     : frequency of the wave in Trad/s, scalar
%    ck    : wave vector in unit of Trad/s, 3-by-1
%    para  : structure containing input parameters
% Outputs:
%    b     : branch index ordered from high to low frequency
%            b=1 for highest frequency branch
%            b=0 error when identifying dispersion branch
%
% Written by Yuan SHI, Feb 4, 2019

function b=wck_2_b_warm(w,ck,para)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% relative smallness parameter for checking dispersion
ee=1e-3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check input
assert(length(w)==1)
assert(length(ck)==3)

% khat1
[~,lat,ckr]=cart2sph(ck(1),ck(2),ck(3));
theta=90-lat/pi*180; % polar angle

% w from dispersion
omega=ck_2_w_warm(ckr,theta,para,0);
dw=abs(omega-w);

% absolute smallness parameter
% ew=ee*min(w,para.wp);
% branch separation
nw=length(omega);
dw0=omega(1:nw-1)-omega(2:nw); % omega descend 
dw0=sort(dw0,'ascend');
ew=ee*dw0(2); % next to smallest to aviod degeneracy

% search for branch
ii=0;
for i=1:nw
    if dw(i)<ew
        ii=ii+1;
        btmp=i;
    end
end

if ii==1 % unique branch identified
    b=btmp;
else % not uniquely identified
    % closest branch
    [~,iw]=min(dw);
    b=iw;    
    
    % special treatment to unmagnetized EM wave
    B0=para.B0;    
    if B0==0 && (b==1 || b==2)
        b=1; % always set to higher branch when degenerate
    else           
        warning(['Dispersion branch not uniquely identified! ii=',...
            num2str(ii),', w=',num2str(w),', ck=',num2str(ck'),...
            ', omega=',num2str(omega'),', ew=',num2str(ew),...
            ', set b=',num2str(iw)])
    end
end