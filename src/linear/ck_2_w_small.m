% This function compute asymptotic dispersion in warm fluid plasma
% when ck is small, assuming the plasma is quasineutral
%
% For small ck and w, the dispersions gapless modes are the following
%    fast wave   : w=Cf*k
%    Alfven wave : w=Ca*k*cos(theta)
%    slow waves  : w=Cs*k
% where Ca^2=Va^2/(1+Va^2/c^2) is Alfven speed, Cs is the sound speed
% In multispecies plasma, there can be multiple sound waves
%
% In addition, there are gapped hybrid waves and EM waves 
%  
% Inputs:
%    ck    : wave vector in unit of w (Trad/s), length 1-by-nk
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%    flag  : plot figure if flag=1
%
% Outputs:
%    wc2    : cutoff frequency wc^2, size nw-by-1
%    vp2    : dw^2/ dck^2, size nw-by-1
%
%             The asymptotic dispersion is of the form
%             w^2=wc2+vp2*ck^2
%    ngl    : nunmber of nontrivial gapless modes
%    wsmall : frequency of the wave in Trad/s, size nw-by-nk
%             For each ck, listed from high to low frequencies.

%
% Written by Yuan SHI, Feb 11, 2019

function [wc2,vp2,ngl,wsmall]=ck_2_w_small(ck,theta,para,flag)

% check inputs
assert(size(ck,1)==1)
assert(length(theta)==1)

% check quasi neutrality
nn=para.nn;
if nn==1
    warning('wsmall is not designed for non-neutral plasma!')
end

% initialize outputs
nw=para.nw;
wc2=zeros(nw,1);
vp2=zeros(nw,1);

% gapless modes refractive index
in2=in2_small(theta,para,0);
if sum(in2)~=0 % not cold unmagnetized
    % number of nontrivial gapless modes
    ngl=length(in2); % np>=1    
    wc2(nw-ngl+1:nw)=0;
    vp2(nw-ngl+1:nw)=in2;
end

% gapped modes lowest order correction
[din2,nwc]=din2_gapped(theta,para); % column vector
nc=length(din2);
wc2(1:nc)=nwc.^2;
vp2(1:nc)=din2;
    
% quadratic dispersion relations
nk=size(ck,2);
wsmall=sqrt(wc2*ones(1,nk)+vp2*ck.^2); % vector outer product

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%
if flag==1
    plot(ck,wsmall)
end
        