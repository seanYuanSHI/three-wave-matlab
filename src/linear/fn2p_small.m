% This function is used in n2p_small.m for root finding
%     fd=[I2*(n2p-iCa2+I0*st2)-I1^2*st2]*tau
% where I0, I1, I2 are characteristic sums, 
% and tau is the pole removing polynomial
%
% Input:
%    n2p   : parallel refractive index n2*cos(theta)^2, length nn
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%    flag  : flag=1, plot figure
%
% Output:
%    fd    : value of difference, length nn
%
% Written by Yuan SHI, Feb 10, 2019

function fd=fn2p_small(n2p,theta,para,flag)

% calculate angles
t=theta/180*pi;
st=sin(t);
st2=st^2;

% unload para
Ws=para.Ws;
wps2=para.wps2;
us2=para.us2;
ns=length(Ws);

% compute un-normalized fd %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute characteristic sums
I0=ones(size(n2p));
I1=zeros(size(n2p));
I2=zeros(size(n2p));
tau=ones(size(n2p));

% sum over species
for i=1:ns
  pole=1-us2(i)*n2p;
  tau=tau.*pole;
  
  ker=wps2(i)./pole;
  I0=I0+ker/Ws(i)^2;
  I1=I1+ker/Ws(i);
  I2=I2+ker;
end

% Alfven speed
Ca=para.Ca;
iCa2=1/Ca^2;

% output
fd=I2.*(n2p-iCa2+I0*st2)-I1.^2*st2;
fd=fd.*tau;

% normalize fd %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% s.t. the polynomial leading coefficient = 1
% thermal species
uindex=para.uindex;
nt=length(uindex);

% compute leading coefficient
if nt>=ns-1
    a=0;
    % sum over species
    for i=1:ns
        % product of thermal speeds
        p=1;
        for j=1:ns
            % j~=i
            s=abs(sign(i-j));
            p=p*(-us2(j))^s;
        end
        a=a+wps2(i)*p;
    end
else
    a=0; 
    p=1; 
    for i=1:ns
        s=sign(us2(i)); % s=1 if warm
        a=a+(1-s)*wps2(i); % sum only cold species
        p=p*(-us2(i))^s; % product only warm species
    end
    a=a*p;
end
assert(a~=0,'leading coefficient should be nonzero!')
fd=fd/a;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot figures
if flag>0
    clf
    plot(n2p,fd,'.-')
    hold on
    grid on 

    % mark poles
    ymax=max(fd);
    ymin=min(fd);
    for i=1:ns
        line([1/us2(i),1/us2(i)],[ymin,ymax],'Color','k')
    end
end