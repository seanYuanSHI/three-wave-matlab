% This function search frequency w from vector ck in warm fluid plasma
%  
% Inputs:
%    ck    : wave vector in unit of w (Trad/s), length nk
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%    flag  : flag=1 plot figure 
%            flag=2 plot in log scale
%
% Outputs:
%    omega : frequency of the wave in Trad/s, size nw-by-nk
%            For each ck, listed from high to low frequencies.
%            omega=sqrt(w^2) is the positive square root
%
% Notice theta=0 and 90 deg are special angles
% May not convergence when frequency scales are too separated
%
% Written by Yuan SHI, April 7, 2018
% Last modified by Yuan Shi, Feb 14, 2019

function omega=ck_2_w_warm(ck,theta,para,flag)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% maximum number of Weierstrass iterations
iter=1e3;

% Convergence criteria of Weierstrass iterations.
% ebas=1e-6 means maximum root error is 1e-6*min(w).
% Usually error for smallest root << error for largest root
%eabs=1e-6;  
eabs=1e-3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize output
nk=length(ck);
nb=para.nw;
nb2=2*nb;
% allocate array
omega=zeros(nb,nk);

% frequency scales for normalization
% total plasma frequency
wp=para.wp;
% asymptotics upper bound 
wa=para.wa;
wa2=wa^2;

% smallness parameters for root finding
%eabs2=eabs^2;
% smallness parameter for asymptotics
ea=para.ea;
ea2=ea^2;
%waea=wa/ea;

% asymptotic correction if non-neutral
nn=para.nn;
% precompute small ck aymptotics
[wc2,vp2,~,~]=ck_2_w_small(wa,theta,para,0);
% smallest nonzero phase velocity
%vp2sort=sort(unique(vp2),'ascend');
%vpmin=sqrt(vp2sort(2));
%assert(vpmin~=0,'Need nonzero phase speed!')

% Special treatment for cutoff
ck1=abs(ck(1));
if ck1==0 
    % load values of fixed length, truncate and pad tail
    %omega(1:nw,1)=pad_trunc(sort(wc,'descend'),nw,0,2);
    omega(:,1)=sqrt(wc2);
    % start loop from ck(2)
    iki=2;
else
    iki=1;
end

% loop through k
na=0; % number of modes corrected asymptotically
for ik=iki:nk   
    cki=abs(ck(ik));
    cki2=cki^2;
    
    % normalize to center roots on O(1) scale    
    wn2=max(cki,wa)*wp; % geometric average    
    %wn2=sqrt(wp*waea*vpmin)*cki;
    wn=sqrt(wn2);
    w2n=wn2^nb;
    
    % function handel for root finding
    fun=@(w)fck2w_warm(w*wn,cki,theta,para,0)/w2n;
    % polynomial of w2
    %fw2=@(w2)fun(sqrt(w2));
    
    % use Monte Carlo to estimate roots
    %[w2,~]=poly_roots_MC(fw2,nb,1);
    %w2=sort(abs(w2),'descend');    
    [w,~]=poly_roots_MC(fun,nb2,1);
    w=sort(real(w),'descend');    
    
    % correction in quasi-neutral plasma
    for iw=1:nb % always compute because cheap
        dw2tmp=vp2(iw)*cki2;
        wc2tmp=wc2(iw);
        % low w or ck asymptotics
        if nn==0 && dw2tmp<max(wa2,ea2*wc2tmp) 
            % quadratic dispersion
            %w2tmp=wc2tmp+dw2tmp;    
            wtmp=sqrt(wc2tmp+dw2tmp)/wn; 
            % update normalized guess 
            %w2(iw)=w2tmp/wn2;
            w(iw)=wtmp;
            w(2*nb-iw+1)=-wtmp;
            na=na+1;
        end
    end
    
    % refine roots 
    if na<nb % not all roots are asymptotic
        % normalized error scale
        %we2=max(min(w2),eabs2*wa2/wn2)
        we=eabs*max(min(w),wa/wn);
        % dw2=2*w*dw, now dw=ee*w => dw2=2*ee*w^2
        %we2=2*eabs*we2
        
        % use noise to remove degeneracy
        % noise << error tolerance
        % Weierstrass convergence determined by absolute error
        %w2=w2+1e-6*we2*rand(nb,1);
        w=w+eabs*we*rand(nb2,1);
    
        %x=poly_roots_Weierstrass(fun,x0,imax,eabs,erel)
        %w2=poly_roots_Weierstrass(fw2,w2,iter,we2);
        w=poly_roots_Weierstrass(fun,w,iter,we);
    end
        
    % load data
    %w=sort(sqrt(abs(w2)),'descend');
    w=sort(w,'descend');
    omega(1:nb,ik)=wn*w(1:nb);     
    na=0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot figure
if flag>0 && ~isempty(ik)
    %figure
    clf
    if nk>1 % plot dispersion
        if flag==1
            plot(ck,real(omega),'.-')
        else
            semilogy(ck,real(omega),'Linewidth',2)
        end
        hold on
        % mark light cone and w=0
        line([0,cki],[0,cki],'Color',[1 1 1]/2)
        line([0,cki],[0,0],'Color',[1 1 1]/2)
        ylim([0 cki])

        % axis labels
        xlabel('ck (Trad/s)')
        ylabel('w (Trad/s)')
        set(gcf,'color','w')
    else % plot diagnostics
        w0=linspace(-1e-2*w(1),2*w(1),1000);
        plot(w0,fun(w0),'.-')
        hold on
        grid on
        % resolve each root
        for j=1:nb            
            % mark polynomial roots
            wpj=w(j);
            plot(wpj,fun(wpj),'or')
            wj=linspace(0.99*wpj,1.01*wpj,100);
            plot(wj,fun(wj),'.-')
        end
    end
end
    
    
    
