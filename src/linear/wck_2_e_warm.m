% This function compute polaization vector e from w and ck
% The magnetic field is in the z-direction
% The outputs are vector in (x,y,z) coordinate
%
% See paper for detailed description of the algorithm
% Essentially, we need to solve linear equations
%    d1.ehat=d2.ehat=d3.ehat=0
% The solutions are given by
%    ehat=a3*(d1 x d2)+a1*(d2 x d3)+a2*(d3 x d1)
% where ai are scaling parameters such that ehat is nonzero.
%
% The dispersion matrix in wave coordinate is of the form
%      (  D11   i*D12   D13  )   
%  D = (-i*D12   D22  -i*D23 )
%      (  D13   i*D23   D33  )
% The polarization vector is of the form ehat=(ek,-i*ey,e*)
% So evec=(ek,ey,e*) satisfies the matrix equation
%
%      (  D11    D12   D13  ) (ek)  
%      (  D12    D22   D23  ) (ey) = 0
%      (  D13    D23   D33  ) (e*)
%
% Special treatment is needed for degenerate EM waves 
% in unmagnetized plasmas. Assume always linearly
% polarized in y direction in wave frame.
% 
%  
% Inputs:
%    w     : frequency of the wave in Trad/s, length nw
%    ck    : wave vector in unit of Trad/s, 3-by-nw
%    para  : structure containing input parameters
%    flag  : plot figure if flag=1
%    unit  : unit=0: polarization angles in degree
%            unit=1: polarization angles in rad
% Outputs:
%    exyz   : normalized polarization in (x,y,z) coordinate b=(0,0,1)
%             complex vector, 3-by-nw
%    phi    : polarization angle <e,k>, length nw
%    psi    : polarization angle <e_perp,k*y>, length nw
%
%             ek=cos(phi)
%             ey=sin(phi)*sin(psi)
%             e*=sin(phi)*cos(psi)
%
% Last modified by Yuan SHI on Oct 29, 2021

function [exyz,phi,psi]=wck_2_e_warm(w,ck,para,flag,unit)

if nargin<5
    unit=0;
end

% smallness parameter to ignore relatively small vector
% should not be too small otherwise numerical error may be large
ee=0.1; 

% special treatment for unmagnetized
% smallness parameter for detecting zero
e0=1e-12;
% special treatment when unmagnetized
B0=para.B0;

% check dimension
nw=length(w);
assert(size(w,1)==1,'w must be a row vector')
assert(size(ck,1)==3,'vector ck should be 3-by-nw')
assert(size(ck,2)==nw,'vector ck should be 3-by-nw')

% initialize arrays
exyz=zeros(3,nw); 
phi=zeros(1,nw);
psi=zeros(1,nw);

% compute for each (w,ck)
for i=1:nw
    % unload vector
    wi=w(i);
    cki=ck(1:3,i);

    % Geometry %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % convert ck from (x,y,z) to sperical coordinate
    [azi,ele,ckr]=cart2sph(cki(1),cki(2),cki(3));
    
    % polar angle theta=pi/2-ele;
    theta=90-ele/pi*180;
    st=cos(ele);
    ct=sin(ele);
    st2=st^2;
    ct2=ct^2;
    
    % azimuthal angle
    sp=sin(azi);
    cp=cos(azi);
    
    % rotation (x',y',z')=(x,y,z)*Rp
    Rp=[cp,-sp,0;sp,cp,0;0,0,1];
    
    % rotation (k,y',k*y')=(x',y',z')*Rt
    Rt=[st,0,-ct;0,1,0;ct,0,st];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % compute polariztion vector in wave frame (k,y',k*y')
    
    % refractive index n2=ckr^2/w^2
    n2=ckr^2/wi^2;
    
    % Stix's dielectric functions
    [Sw,Dw,Pw,Tw,Ew]=Stix_warm_wck(wi,ckr,theta,para,0);
    
    % components of the dielectric tensor in the wave frame
    D11=Sw*st2+Pw*ct2;
    D12=-Dw*st;
    D13=(Pw-Sw)*st*ct;
    
    D21=D12;
    D22=Sw+Tw-n2;
    D23=(Dw-Ew)*ct;
    
    D31=D13;
    D32=D23;
    D33=Sw*ct2+Pw*st2+Tw-n2;
    
    % compute 3 versions of ehat
    d1d2=[D12*D23-D22*D13;
          D13*D21-D11*D23;
          D11*D22-D12*D21];
      
    d2d3=[D22*D33-D23*D32;
          D31*D23-D21*D33;
          D21*D32-D22*D31];
      
    d3d1=[D13*D32-D33*D12;
          D11*D33-D31*D13;
          D31*D12-D32*D11];
      
    % compute norm of didj
    n12=sqrt(d1d2'*d1d2);
    n23=sqrt(d2d3'*d2d3);
    n31=sqrt(d3d1'*d3d1);
    
    % total norm
    n123=n12+n23+n31;
    if abs(B0)<e0 && n123<ee % unmagnetized EM wave
        %disp('Unmagnetized EM wave')
        evec=[0;1;0];
    else       
        % criteria for rejecting small vector
        en=n123*ee;
        
        % weighting factors to avoid zero ehat
        % (+-)1.0(+-)0.8(+-)1.2~=0 is nonzero for all sign combinations
        % the scaling parameters are otherwise arbitrary
        if n12<en % d1d2 effectively zero
            a3=0;     % weight =0
        else
            a3=1/n12; % wight =1
        end
        
        if n23<en % d2d3 effectively zero
            a1=0;     % weight =0
        else
            a1=0.8/n23; % wight =0.8
        end
        
        if n31<en % d3d1 effectively zero
            a2=0;     % weight =0
        else
            a2=1.2/n31; % wight =1.2
        end
        
        % unnormalized evec=(ek,ey',e*)
        evec=a3*d1d2+a1*d2d3+a2*d3d1;
    end
    
    % convert to sperical coordinate 
    % e*=\sin\phi*\cos\psi, ey'=\sin\phi*\sin\psi, ek=\cos\phi
    % The coordinate k*y', y', -k is right-handed
    [az,el,r]=cart2sph(evec(3),evec(2),-evec(1));
    assert(r~=0,'polarization vector should be nonzero!')
   
    if unit==0 % angles in degree 
        % polarization polar angle phi=pi/2+ele;
        phi(i)=90+el/pi*180;
        % polarization azimuthal angle psi=azi
        psi(i)=az/pi*180;
    else
        % polarization polar angle phi=pi/2+ele;
        phi(i)=pi/2+el;
        % polarization azimuthal angle psi=azi
        psi(i)=az;
    end 
 
    % unit vector in wave frame (k,y',k*y')
    ehat=[evec(1);-1i*evec(2);evec(3)]/r;    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  Rotate from wave frame (k,y,k*y) to (x,y,z)
    exyz(1:3,i)=Rp*Rt*ehat;
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot angles in wave coordinate
% Notice e->-e is an identity transformation
% which allows (phi,psi)->(phi \pm 180,psi)
% as well as   (phi,psi)->(-phi,psi \pm 180)
if flag==1    
    % unit of angles
    if unit==0 % in degree
      ulabel = ' (deg)';
    else
      ulabel = ' (rad)';
    end

    clf
    subplot(2,1,1)
    plot(w,phi,'.')
    hold on
    grid on
    ylabel(strcat('phi', ulabel))
    
    subplot(2,1,2)
    plot(w,psi,'.')
    hold on
    grid on
    xlabel('w (Trad/s)')
    ylabel(strcat('psi', ulabel))
    set(gcf,'Color','w')
end



