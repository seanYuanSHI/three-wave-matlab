% This program computes the warm dielectric functions S',D',P',E,T
% The 'ed warm operators are related to Stix dielectric function by
%    S'=S-T*cos(t)^2
%    D'=D+E*cos(t)^2
%    P'=P-T*sin(t)^2
%
% Input: 
%    w     : frequency in unit of 2*pi*THz, vector of length nw
%    ck    : norm of wave vector in unit of w, vector of length nk
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%    flag  : flag=1,2,3,4,5 plot Sw,Dw,Pw,Tw,Ew respectively
% Outputs:
%    warm dielectric functions are matrix of size nw*nk
%
% Written by Yuan SHI, April 2, 2018

function [Sw,Dw,Pw,Tw,Ew]=Stix_warm_wck(w,ck,theta,para,flag)

% Length
nw=length(w);
nk=length(ck);

% convert angle
t=theta/180*pi;
ct2=cos(t)^2;

% set default output
Sw=ones(nw,nk);
Dw=zeros(nw,nk);
Pw=ones(nw,nk);
Tw=zeros(nw,nk);
Ew=zeros(nw,nk);

% list of species plasma frequency^2, gyro frequency, thermal speed
wps2=para.wps2;
Ws=para.Ws;
us2=para.us2;
ns=length(wps2);

% loop through w,ck
% Matlab is column major
for j=1:nk
    % square of wave vector
    ck2=ck(j)^2;
    for i=1:nw
        % square of frequency
        w2=w(i)^2;
        % sum over species        
        for k=1:ns
            % magnetization ratio/factor
            bs=Ws(k)/w(i);
            bs2=bs^2;
            
            % thermal pole polynomial
            us2ck2=us2(k)*ck2;
            Ws2=Ws(k)^2;
            tau=w2^2-(Ws2+us2ck2)*w2+Ws2*us2ck2*ct2;
            
            % magnetic dielectric kernal
            chi0=wps2(k)/tau;
            
            % dielectric functions
            Sw(i,j)=Sw(i,j)-chi0*w2;
            Dw(i,j)=Dw(i,j)+chi0*w2*bs;
            Pw(i,j)=Pw(i,j)-chi0*w2*(1-bs2);
            Tw(i,j)=Tw(i,j)+chi0*us2ck2;
            Ew(i,j)=Ew(i,j)+chi0*us2ck2*bs;
        end
    end
end       

% plot figure
if flag==1
    plot_nwnk(w,ck,Sw,'.')
elseif flag==2
    plot_nwnk(w,ck,Dw,'.')
elseif flag==3
    plot_nwnk(w,ck,Pw,'.')
elseif flag==4
    plot_nwnk(w,ck,Tw,'.')
elseif flag==5
    plot_nwnk(w,ck,Ew,'.')
end

