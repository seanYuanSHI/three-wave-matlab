% This program computes the Stix dielectric functions S,D,P
% in cold magnetized plasma
%
% Input: 
%    w    : frequency in unit of 2*pi*THz
%    para  : structure containing input parameters
% Written by Yuan SHI, Nov 10, 2017

function [S,D,P]=Stix(w,para)

% set default output
S=1;
D=0;
P=1;

% list of species plasma frequency^2, gyro frequency
wps2=para.wps2;
Ws=para.Ws;

% square of frequencies
Ws2=Ws.^2;
w2=w.^2;

% sum over species
ns=length(wps2);
for j=1:ns
    S=S-wps2(j)./(w2-Ws2(j)); % devide by zero possible
    D=D+Ws(j)*wps2(j)./w./(w2-Ws2(j)); % devide by zero possible
    P=P-wps2(j)./w2;
end