% This script calculate 1D group velocity along the k_vec direction
% using numerical derivative of the dispersion relation.
%
% Notice that in magnetized plasmas, the vector group velocity
%    vg_vec = \partial\omega/\partial k_vec
% is in general in a different direction as k_vec.
%
% Inputs:
%    b     : branch index, integer
%    ck    : wave vector in unit of w (Trad/s), scalar 
%    theta : angle between wave vector and B-field, in degree
%    para  : structure containing input parameters
%    flag  : flag=1 plot diagnostic figure and report error
% Outputs:
%    vg : 1D group velocity along k_vec, in units of c
%
% Last modified by Yuan Shi, Oct 28, 2021

function vg = Vgroup1D(b, ck, theta, para, flag)
    if nargin<5
        flag=0; % default no plot
    end
    % ensure b is a positive integer
    b = ceil(abs(b));
    assert(b>0,'branch index should be positive integer!')
    % check length
    assert(length(ck)==1, 'ck should be scalar!')

    % function for wave frequency on branch b
    % nested function knows about parent variables
    function w = ckb2w(x)
        omega = ck_2_w_warm(x,theta,para,0);
        w = omega(b,:);
    end 

    % function handel for numerical differentiation
    fun = @(x)ckb2w(x);
    % numerical derivative
    [vg, err]=derivest(fun,ck);

    % plot diagnostic figure
    if flag>0
        figure
        disp(['Vgroup1D: accuracy of numerical differentiation is ',num2str(err)])
        % plot local dispersion 
        nk = 50;
        ckall = linspace(0,2*ck,2*nk+1);
        omega = ck_2_w_warm(ckall,theta,para,1);
        w = omega(b,nk+1);
        % mark local slope     
        cklocal = ckall(ceil(0.5*nk):floor(1.5*nk)); 
        plot(cklocal,w+vg*(cklocal-ck),'k--','LineWidth',2)
        line(xlim,[w,w],'Color',[0.5,0.5,0.5]);
        line([ck,ck],ylim,'Color',[0.5,0.5,0.5]);
        % add title
        title('Group Velocity 1D')
    end   
end
