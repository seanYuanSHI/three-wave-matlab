% This function compute the refractive index for gapless modes
% assuming the plasma is qusi neutral
%
% Inputs:
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%    flag  : flag=1, plot figure
%
% Output:
%    in2   : inverse of values of refractive index, 1/n^2
%            The dispersion is then of the form w^2=in2*(ck)^2
%            in2 is descend ordered
%
% Written by Yuan SHI, Feb 10, 2019

function in2=in2_small(theta,para,flag)

% smallness parameter for perp angle
er=1e-6;

% check inputs
assert(length(theta)==1)
% unload para
%nB=para.nB;
B0=para.B0;

% unmagnetized sound waves
Cs=para.Cs;

%if nB==1 % weak magnetic field
if B0==0 % zero magnetic field
    % load sound waves
    in2=Cs.^2;

    % trivial case, no plot
    flag=0;
    
else % magnetized
    % initialize output
    % sounds + Alfven + fast
    nc=length(Cs);
    in2=zeros(nc+2,1); 
    
    % unload para
    % Alfven speed
    Ca=para.Ca;
    Ca2=Ca^2;    
    % Themal speed
    us2=para.us2;
    ns=length(us2);    
   % plasma frequencies
    Ws=para.Ws;
    wps2=para.wps2;        
    
    % compute cos(theta)
    ct=cos(theta/180*pi);
    ct2=ct^2;
    
    if ct2<er % perpendicular propagation
        % all waves becomes zero-frequency modes
        % except for the fast wave
        
        % compute fast wave speed
        fast=1;
        for i=1:ns
            fast=fast+us2(i)*wps2(i)/Ws(i)^2;
        end
        in2(1)=Ca2*fast;
        
        % trivial case, no plot
        flag=0;
        
    else % other angles of propagation
        % load Alfven wave
        in2(1)=Ca2*ct2;
        
        if nc==0 % load fast wave, no sound wave
            in2(2)=Ca2;
            % trivial case, no plot
            flag=0;
        else % sounds + fast    
            % function handel for root finding fast + sounds
            % fun is a polynomial of order nc+1
            % fun is normalized leading coefficient = 1
            fun=@(n2p)fn2p_small(n2p,theta,para,0);
            
            % initial guess of polynomial root
            [n2,~]=poly_roots_MC(fun,nc+1,1);
            
            % refine roots
            n2=poly_roots_Weierstrass(fun,real(n2));
            
            % load roots
            % inverse 1/n^2=cos(theta)^2/n2;
            in2(2:2+nc)=ct2./n2;
        end     
    end
end
in2=sort(in2,'descend');

% plot figure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag==1
    clf
    % plot global structure
    n2max=max(n2);
    x=linspace(0,2*n2max,1000);
    plot(x,fun(x),'.-')
    hold on
    grid on
    % resolve each root
    for i=1:nc+1
        x=linspace(0.8*n2(i),1.2*n2(i),100);
        plot(x,fun(x),'.-')
    end
    % mark roots
    plot(n2,zeros(nc+1,1),'ok','MarkerFaceColor','k')
end
    
    
        
