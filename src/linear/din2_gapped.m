% This function compute correction to refractive index for gapped modes
% near the cutoff: din2=dw^2/ck^2
%
% At the cutoff, the Stix symbol C=0
% Near the cutoff, Taylor expansion: 0~-B*n^2+(dC/dw2)* dw2
% Then dw2~ (2*B*ck^2)/(w*dC/dw), where all terms are evaluated at cutoff
%
% Inputs:
%    theta : angle between wave vector and B-field in degree
%    para  : structure containing input parameters
%
% Output:
%    din2   : din2=dw^2/ck^2, size nc-by-1
%    nwc    : corresponding nontrivial cutoff frequencies, size nc-by-1 
%
% Modes are ordered from high to low frequency
%
% Written by Yuan SHI, Feb 12, 2019

function [din2,nwc]=din2_gapped(theta,para)

% unload para
wp=para.wp;
wp2=wp^2;

% B field
B0=para.B0;
%nB=para.nB;
if B0==0 % zero magnetic field
    err=1e-9; % smallness parameter to break degeneracy
    dw=wp*err;
    
    din2=[1;1;0]; % 2EM + Langmuir
    nwc=[wp+dw;wp;wp]; % degenerate
else
    % cutoff frequencies
    wc=para.wc; % row vector
    nc=length(wc);
    nn=para.nn;
    
    % non-trivial cutoffs
    %nwc=flipud(wc(2-nn:nc)'); % column vector, descend
    nnc=nc-1+nn;   
    nwc=wc(1:nnc)'; % column vector, descend
 
    
    % initialize output
    din2=zeros(nnc,1);
    
    % convert angle
    ct=cos(theta/180*pi);
    ct2=ct^2;
    st2=1-ct2;
    
    % unload para
    Ws=para.Ws;
    wps2=para.wps2;
    ns=length(Ws);    
    
    % compute for each cutoff
    for i=1:nnc
        % cutoff frequency
        w=nwc(i);
        w2=w^2;
        
        % Stix symbols at cutoff
        [S,D,P]=Stix(w,para);
        R=S+D;
        L=S-D;
        
        % dispersion coefficient B
        B=R*L*st2+S*P*(1+ct2);
        
        % derivatives of Stixs L and R
        dL=0;
        dR=0;
        for j=1:ns
            dL=dL+wps2(j)*(2*w-Ws(j))/w2/(w-Ws(j))^2;
            dR=dR+wps2(j)*(2*w+Ws(j))/w2/(w+Ws(j))^2;
        end
        
        % Derivative of C
        dC=dL*R*P+dR*L*P+L*R*2*wp2/w^3;
        
        % output
        din2(i)=2*B/w/dC;
    end
end
    
    
    