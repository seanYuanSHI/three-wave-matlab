% This function compute the polynomial tau(w) that is used 
% to remove thermal and magnetic poles from the dispersion coefficients
%
% Inputs:
%    w     : frequency of the wave in Trad/s, row vector length nw
%    ck    : norm of wave vector, in unit of w, row vector length nk
%    theta : angle between wave vector and B-field in degree, scalar
%    para  : structure containing input parameters
%    flag  : plot figure
% Output:
%    tau   : polynomial , nk-by-nw
%                tau_t=\Pi_s(w^2-ws+^2)(w^2-ws-^2)
%            where ws+ and ws- are thermal poles.
%
%            When there are non-thermal species
%                tau=tau_t*w^2\Pi_s(w^2-Ws^2)
%    dw2   : tau*fd is a polynomial of w^2 of degree dw2
%
% Written by Yuan SHI, April 2, 2018

%function [tau,dw2]=pole_w_remove_warm(w,ck,theta,para,flag)
function tau=pole_w_remove_warm(w,ck,theta,para,flag)

% convert angle from degree to rad
t=theta/180*pi;
ct2=cos(t)^2;

% initialize output
nw=length(w);
nk=length(ck);
tau=ones(nk,nw);

% thermal speeds
us2=para.us2;
ns=length(us2); % total number of species

index=para.uindex; % index of species with us>0
nt=length(index);  % number of thermal species    
assert(ns>=nt);

% gyro frequencies
B0=para.B0;
Ws=para.Ws;
Ws2=Ws.^2;

% pre-calculate squares
w2=w.^2;
w4=w2.^2;
ck2=ck.^2;

% compute polynomial
% Matlab is column major
if nt==0 % cold
    if B0==0 % unmagnetized
        %dw2=3; % (2 EM)+(1 Langmuir)
        for iw=1:nw
            for ik=1:nk
                tau(ik,iw)=w2(iw);
            end
        end
    else % magnetized
        %dw2=ns+3; % (2 EM)+(1 Langmuir)+(ns Magnetic)
        for iw=1:nw
            for ik=1:nk
                tmp=w2(iw);
                for is=1:ns
                    tmp=tmp*(w2(iw)-Ws2(is));
                end
                tau(ik,iw)=tmp;
            end
        end
    end
else % not all cold
    % exponent for w^2=0 pole        
    ew=sign(ns-nt);   
    if B0==0 % unmagnetized
        %dw2=nt+ew+2; % (2 EM)+(1 Langmuir)+(nt+ew-1 sound)
        for iw=1:nw
            ww=w2(iw)^ew;
            for ik=1:nk
                tmp=ww;
                for it=1:nt
                    ind=index(it);
                    us2ck2=us2(ind)*ck2(ik);
                    tmp=tmp*(w2(iw)-us2ck2);
                end
                tau(ik,iw)=tmp;
            end
        end
    else % magnetized
        % exponnet for removing degenerate w^2 poles
        dew=ns-nt-ew;
        %dw2=ns+nt+ew+2; % (2 EM)+(1 Langmuir)+(ns Magnetic)+(nt+ew-1 sound)
        % loop over w and ck
        for iw=1:nw           
            a=w4(iw);
            ww=1/w2(iw)^dew;
            for ik=1:nk
                tmp=ww;
                for is=1:ns
                    us2ck2=us2(is)*ck2(ik);
                    b=(Ws2(is)+us2ck2)*w2(iw);
                    c=Ws2(is)*us2ck2*ct2;
                    tmp=tmp*(a-b+c);
                end                
                tau(ik,iw)=tmp;
            end
        end
    end    
end

% plot figure
if flag==1
    plot_nwnk(w,ck,tau,'.-')
end

