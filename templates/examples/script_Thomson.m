% This script to be run in batch
%close all
%clear
cd ./src % enter /src directory
addpath(genpath('./'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Bs = [0.15, 0.2, 0.25];
Bs = 0.18;

% branch indices
%b1b2 = [[1,2]];
b1b2 = [[1,1];[1,2]];

fpath = '../data/OMEGA/CBET/TIM2/TIM4Seed/psi95/';
fname_in = [fpath,'theta86.97phi-104.02_angles.txt'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nBs = length(Bs);
sbb = size(b1b2);
nbb = sbb(1);
for i=1:nBs
    B0 = Bs(i)
    for j=1:nbb
        b1 = b1b2(j,1);
        b2 = b1b2(j,2);
        fstem = [fpath,'p',num2str(b1),'s',num2str(b2),'/'];
        fstem_out = [fstem,'N0.5B',num2str(B0),'_helium_B'];
        main_Thomson_driver(B0, fname_in, fstem_out, b1, b2)
    end
end
