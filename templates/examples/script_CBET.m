% This script compute CBET for OMEGA setups.
% This script to be run in batch
%close all
%clear
cd ./src % enter /src directory
addpath(genpath('./'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Bs = [0.3, 0.6, 1, 2];
%Bs = [0.2, 0.3, 0.4, 0.5, 0.6];
Bs = 2;

% branch indices
%b1b2 = [[1,2]];
b1b2 = [[1,1];[1,2]];
%b1b2 = [[1,2];[2,1];[2,2]];
%b1b2 = [[1,1];[1,2];[2,1];[2,2]];

%fstem = '../data/OMEGA/CBET/TIM2/TOP9Seed/B12Pump/';
%fstem = '../data/OMEGA/CBET/TIM2/B25Seed/';
fstem = '../data/OMEGA/CBET/backscatter/';

%fname_in = [fstem,'B12Pump_TOP9Seed_TIM2Radiograph_angles_halved.txt'];
%fname_in = [fstem,'B30Pump_B25Seed_TIM2Radiograph_angles_halved.txt'];
fname_in = [fstem,'backscatter_angles.txt'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nBs = length(Bs);
sbb = size(b1b2);
nbb = sbb(1);
for i=1:nBs
    B0 = Bs(i)
    for j=1:nbb
        b1 = b1b2(j,1);
        b2 = b1b2(j,2);
        fpath = [fstem,'p',num2str(b1),'s',num2str(b2),'/'];
        fname_out = [fpath,'N1T1B',num2str(B0),'_carbon.txt'];
        main_CBET_driver(B0, fname_in, fname_out, b1, b2)
    end
end
