[[_TOC_]]

# Overview
This _MATLAB_ program suite contains functions for linear waves in magnetized warm-fluid plasmas, as well as their nonlinear three-wave couplings. The underlying methodology is described in details in the paper: Yuan Shi, "_Three-wave interactions in magnetized warm-fluid plasmas: General theory with evaluable coupling coefficient_", [Physical Review E **99**, 063212 (2019)](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.99.063212). The code is reviewed and released under LLNL-MI-837379.

After adding [src/](src/) to _MATLAB_ seach path, a plasma object is initialized using [species.txt](species.txt) by  [Parameter.m](src/Parameter.m)
```
>> B0 = 30; % in MG
>> para = Parameters(B0);
```
In [species.txt](species.txt), species with zero or negative densities will be ignored. The p-index is the polytropic index. The standard units of the program for magnetic field is mega-gauss, for density is $`10^{20}`$ cc, for temperature is keV, and for frequency is Trad/s. These units are tailored for typical parameters of laser-plasma interactions. The background magnetic field $`\mathbf{B}_0`$ is along the z direction. Notice that propagations exactly parallel or perpendicular to 
$`\mathbf{B}_0`$ are degenerate cases for the analytical theory, so are three-wave interactions in exact forward or backward scattering geometry. These special cases are handled separately. All final results are nevertheless continuous and have no singularity.


# Linear waves
Functions contained in [src/linear/](src/linear/) compute wave dispersion relation and polarization. Polynomial roots are handeled using a combination of Monte Carlo method and Weierstrass iteration. This scheme does not require knowing the coefficients of the polynomials, but is slow and may have errors. In most cases, functions should work satisfactorily. In known cases where they do not work well, warning messages will be displayed. 

- [ck_2_w_warm.m](src/linear/ck_2_w_warm.m): Use the magnetized warm-fluid dispersion relation to calculate wave frequencies as functions of the wavevector norm and porpagation angle. At each `ck`, there are multiple branches for `omega`. The branch with index `b=1` has the heighest frequency.
```
>> ck = linspace(0,2e3,100); % array of wavevectors in Trad/s
>> theta = 30;               % angle between k and B0 in degree
>> omega = ck_2_w_warm(ck, theta, para, 1); % 1: plot figure
```
![Dispersion.png](src/plots/examples/Dispersion.png "Example dispersion relation")


- [wck_2_e_warm.m](src/linear/wck_2_e_warm.m): Use the magnetized warm-fluid dispersion tensor to calculate the unit polarization vector `exyz` in the lab frame, and the polarization angles `phi` and `psi` in the wave frame. Transverse waves have `phi=90 deg`, while longitudinal waves have `phi=0 deg`. Right-handed circularly polarized waves have `psi=45 deg`, while left-handed circularly polarized waves have `psi=-45 deg`.
```
>> khat = [sin(pi/6);0;cos(pi/6)]; % unit wave vector for 30 deg propagation
>> b = 1;                          % 1: heightest frequency branch
>> [exyz, phi, psi] = wck_2_e_warm(omega(b,:), khat*ck, para, 1); % 1: plot figure
```
![Polarization.png](src/plots/examples/Polarization.png "Polarization angles")


The following wrapper function for linear waves is contained in [src/utilities/](src/utilities/) and returns all properties of linear waves.
- [linear_single_w.m](src/utilities/linear_single_w.m): For given wave frequency `w`, propagation angle `theta`, and branch index `b`, computes wavevector, polarization, energy coefficient, group velocity, normalization factor, and store them in `wave`.
```
w = 1.8e3; % in Trad/s
wave = linear_single_w(para, w, theta, b);
```

# Three-wave coupling
Functions involved in evaluating the magnetized three-wave coupling coefficient is contained in [src/nonlinear/](src/nonlinear/). Once parameters for the three resonant waves are known, the evaluation is straightforward. However, it is difficult to solve the resonance conditions
```math
\omega_1 = \omega_2 + \omega_3,\\
\mathbf{k}_1 = \mathbf{k}_2 + \mathbf{k}_3,
```
where $`\omega_j=\omega(\mathbf{k}_j)`$ satisfies the dispersion relation. The above resonance conditions are solved by (1) specifying $`\mathbf{k}_1`$ and b1 to compute $`\omega_1`$, (2) specifying the unit vector $`\mathbf{\hat{k}}_2`$ and b2, and (3) numerically find the root for $`k_2`$, such that 
```math
\omega_1 = \omega(k_2 \mathbf{\hat{k}}_2) + \omega(\mathbf{k}_1 - k_2 \mathbf{\hat{k}}_2).
```
The above equation has multiple roots, and the program attempts to find all roots using [search_resonance_warm.m](src/nonlinear/search_resonance_warm.m), which may not be successful. The root finding procedure can be inspected using `flag=1`, which plot the objective function on search grids.

- [main.m](src/main.m): Driver for computing three-wave coupling. Use [search_resonance_warm.m](src/nonlinear/search_resonance_warm.m) to find resonances, and evaluate the coupling coefficients.

- [main_map.m](src/main_map.m): Wrapper for [main.m](src/main.m). Computes coupling for multiple $`\mathbf{\hat{k}}_2`$, and returns 1D or 2D scattering maps. 

- [main_CBET.m](src/main_CBET.m): Similar to [main.m](src/main.m), for cross-beam energy transfer (CBET), handles laser refractions when entering plasmas from the vacuum.

The above functions can be called using scripts like [main_driver.m](src/utilities/main_driver.m), [map_table_driver.m](src/utilities/map_table_driver.m), and [main_CBET_driver.m](src/utilities/main_CBET_driver.m). These driver scripts facilitate the process of setting up input variables. 

For example, running [map_table_driver.m](src/utilities/map_table_driver.m) _as is_ will generate data files like _map_N1.00B100.mat_, which can be printed using [map_table_print.m](src/utilities/map_table_print.m).
![map_table_print.png](src/plots/examples/map_table_print.png "Print map_main.m results.")
Each table contains a list of resonances with branch index `b3` and frequency `w3`, which are results of root finding with frequency `w2` and wavevector norm `ck2`. The normalized three-wave coupling coefficient is `M`, where `M=1` means the coupling equals to unmagnetized Raman backscattering in a cold plasma of the same density.

The folder [src/auxiliary/](src/auxiliary/) contains plasma-specific functions, whereas the folder [src/math/](src/math/)contains general-purpose functions. See documentation and comments in each function for details.
Using data generated by main drivers, various plots can be made using functions in [src/plots/](src/plots/). See papers [Physical Review E **99**, 063212 (2019)](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.99.063212) and [Physics of Plasmas **26**, 072114 (2019)](https://aip.scitation.org/doi/10.1063/1.5099513) for some examples. 


